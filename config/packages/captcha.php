<?php if (!class_exists('CaptchaConfiguration')) { return; }

// BotDetect PHP Captcha configuration options

return [
  // Captcha configuration for example page
  'RegisterCaptcha' => [
    'UserInputID' => 'captchaCode',
    'ImageWidth' => 200,
    'ImageHeight' => 50,
  ],

];