# News scopes specifications
## How to manage who can see my message
Comment sélectionner les destinataires de mon message

---
Au moment de publier un message, je peux choisir les destinataires de ce message.
J'ai le choix entre :

- Tous mes amis
- Choisir parmi mes groupes d'amis
- Mes amis et abonnés
- Autour de moi

Les amis reçoivent tous les messages, sauf s'il ne font pas parti du/des groupe(s) sélectionné(s).

Les abonnés (followers) ne reçoivent que les messages pour "Mes amis et abonnés".

Les messages "Autour de moi" sont reçus par mes amis et les personnes localisées à l'intérieure de la zone sélectionnée.

CF : condifentiality
Le droit de publier sur les pages est limité par un paramètre de condidentialité

#### Autorisation du partage
Au moment de publier un message, je peux autoriser mes lecteurs à partager mon message ou non.

news:
	text
	author 		: id de la Page de app.user (utilisateur connecté) qui publie le message
	//signed 		: id Page "en tant que" : soit la Page de app.user, soit l'une des pages dont il est admin
	//target 		: id Page sur laquelle est publié le message
	//scopeType 	: friends - groups[ids] - followers - localised[polygon]
	sharable 	: true / false : si false > personne ne peut partager la news
	object 		: Page / POI / Annonce / Ressources / Proposal / etc
	images		: pictures[ids]
	likes		: liste d'id PageUser add onclick btn-like this post
	created		: date de la publication
	sharedBy	:
		text 
		author 		: id de la Page de app.user (utilisateur connecté) qui partage le message
		signed		: id Page "en tant que" : soit la Page de app.user, soit l'une des pages dont il est admin
		target 		: id Page sur laquelle est partagé le message
		scopeType 	: friends - groups - followers - localised
		scopeGeo 	: polygon
		scopeGroup	: groups[id]
		isOrigin	: true
		created		: date du partage

----------------------------------------------------------
les partages sont des news avec un objShared de type News :

	text

	author 		: id de la Page de app.user (utilisateur connecté) qui publie le message
	signed 		: id Page "en tant que" : soit la Page de app.user, soit l'une des pages dont il est admin
	target 		: id Page sur laquelle est publié le message
	
	scopeType 	: friends - groups[ids] - followers - localised[polygon]
	scopeGeo 	: polygon
	scopeGroup	: groups[id]
	
	isSharable 	: true / false : si false > personne ne peut partager la news
	objShared	: Page / POI / Annonce / Ressources / Proposal / etc
	images		: pictures[ids]
	likes		: liste d'id PageUser add onclick btn-like this post
	created		: date de la publication
	