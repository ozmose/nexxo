- Symfony4 official Documentation
https://symfony.com/doc/current/index.html#gsc.tab=0

- Symfony4 tuto GetStarted 
http://symfony.com/doc/current/page_creation.html

- Symfony4 / Doctrine / Mongo / Mapping Annotations 
https://www.doctrine-project.org/projects/doctrine-mongodb-odm/en/1.2/reference/annotations-reference.html
https://medium.com/@ahmetmertsevinc/symfony-4-and-doctrine-mongo-db-c9ac0f02f742

- Symfony4 DoctrineMongoDBBundle Configuration
https://symfony.com/doc/master/bundles/DoctrineMongoDBBundle/config.html

- Symfony4 Documents Repositories (mongo)
https://www.doctrine-project.org/projects/doctrine-mongodb-odm/en/1.2/reference/document-repositories.html

- Symfony4 Form
http://symfony.com/doc/current/reference/forms/types.html

- Symfony4 Encore : optimizing assets
https://symfony.com/doc/current/frontend.html

- Twig : template engine
https://twig.symfony.com/doc/2.x/
https://twig.symfony.com/doc/2.x/templates.html

- Dom Crawler 
https://symfony.com/doc/current/components/dom_crawler.html