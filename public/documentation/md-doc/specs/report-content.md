# Report content
## Signalement de contenu

### contraintes :
- On peut signaler : des Pages, des News, des Commenaires.
- +de 3 signalements : 
    + redflag 
    + ouverture de la procédure de vote 
    + affichage progressbar resultat
    + bouton de vote rapide
    + bouton info "comment ça marche ?"

- Toutes les Pages signalées sont affichées dans une page unique où on peut voter pour/contre le bloquage de la Page.
- Pour les News et les Commentaires signalés, les boutons de vote sont affichés dans le fil d'actu directement (on ne peut pas voter sur des News ou Commentaire auquels on n'a pas accès)


Report
-----
- id
- aboutType : Page - News - Comment
- aboutId
- commentId : (if aboutType == Comment : aboutId = newsId)
    - pour les signalements de commentaires :
        -  aboutId correspond à l'id de la News
        -  aboutType est "Comment"
        -  commentId correspond à l'id du Comment
- authors[] : liste des auteurs de signalement de cet item
- voteUp[] :liste des votants
- voteDown[] : liste des votants
- status : 
    - open : vote autorisé 
    - disabled : vote clos -> l'élément a été désactivé
    - enabled : vote clos -> l'élément a été réactivé
- created

### Etapes de développement : 
- pouvoir signaler du contenu
    - créer ReportEntity
    - créer ReportController
        - saveReport(aboutType, aboutId, commentId=null)
        - getAllReport() //for superAdmin
        - getReport($aboutType) // used to show Pages reported
        - closeReports() //procedure de vérification des résultats des votes
    - 
- afficher la liste de tous les éléments signalés
- pouvoir voter pour/contre
- procédure de vérification (durée 72h de vote)
