# Comments specifications

---

## How to create the Comments feature :

 ### - 1 | Create the model src/Entity/Comment.php

Define all attributes in private variable
Define getters / setters

```
class Comment
{

/**
 * @MongoDB\Id(strategy="auto")
 */
private $id;
/**
 * @MongoDB\Field(type="string")
 */
private $name;

/**
 * @return mixed
 */
public function getId()
{
    return $this->id;
}

/**
 * @return mixed
 * my description of getName
 */
public function getName()
{
    return $this->name;
}

/**
 * @param mixed $name
 * @return self
 */
public function setName($name)
{
    $this->name = strip_tags($name);
    return $this;
}


etc...
```

---

### - 2 | Create the controller in src/Controller/CommentController.php

```
use App\Entity\Comments;

class CommentsController extends Controller
{
```

- Then create the first @route : getCommentsNews() 
url : /comments/get-comments-news/{newsId}

```
/**
 * Route : /comments/get-comments-news/{newsId}", name="get-comments-news"
 * @Route("/comments/get-comments-news/{newsId}", name="get-comments-news")
 */
public function getCommentsNews($newsId="", 
                                Request $request, 
                                AuthorizationCheckerInterface $authChecker)
                                {}
```

- Add process to get all comments on news (query db + render)

```
/**
 * Route : /comments/get-comments-news/{newsId}", name="get-comments-news"
 * @Route("/comments/get-comments-news/{newsId}", name="get-comments-news")
 */
public function getCommentsNews($newsId="", 
                                Request $request, 
                                AuthorizationCheckerInterface $authChecker){}
```

### - 3 | Create public/js/comment.js

> All javascript code related to Comments features must be in this file, included jquery events.

First just add : 
```
var comment = {
    initUi : function(){},
    getCommentsNews : function(idNews){
        $.ajax({
          type: "POST",
          url: "/comments/get-comments-news/"+newsId,
          success: function(res){
            if(res.error == false){
                toastr.success(res.errorMsg);
            }else{
                toastr.error(res.errorMsg);
            }
          },
          //dataType: dataType
        });
    }    
};
```


### - 4 | Create the template for comments block : /templates/comments/comment-block.html.twig

```
<ul class="comments-list">
	<li>
		<p></p>
		<a href="#" class="post-add-icon inline-items"></a>
		<a href="#" class="reply post__date"></a>
		<a href="#" class="more float-right">
		<a href="#" class="reply float-right margin-right-15">Reply</a>
	</li>
</ul>
<a href="#" class="more-comments">View more comments <span>+</span></a>
<form class="comment-form inline-items">
	<div class="post__author author vcard inline-items">
		<img src="img/author-page.jpg" alt="author">
		<div class="form-group with-icon-right ">
			<textarea class="form-control" placeholder=""  ></textarea>
			<div class="add-options-message">
				<a href="#" class="options-message" data-toggle="modal" data-target="#update-header-photo">
					<svg class="olymp-camera-icon"><use xlink:href="icons/icons.svg#olymp-camera-icon"></use></svg>
				</a>
			</div>
		</div>
	</div>
</form>
```


### - 5 | init the button to send request 'get-comments-news' : 
add css class : "btn-open-comments"
add data-news-id="{{ post.id }}" to be able to load comment for the good newsId
```
<a href="javascript:" class="post-add-icon inline-items btn-open-comments"
   data-news-id="{{ post.id }}">
	<svg class="olymp-speech-balloon-icon">
	    <use xlink:href="{{ asset('icons/icons.svg') }}#olymp-speech-balloon-icon"></use>
    </svg>
	<span>0</span>
</a>
```


### - 6 | implement comment.initUi()
```
var comment = {
initUi : function(){
    $(".btn-open-comments").click(function(){
        var newsId = $(this).data("news-id");
        comment.getCommentsNews(idNews);
    });
}
...
```

### - 7 | Initialise jquery event with comment.initUi() 
in template/news/news-stream.php : 
```
<script>
$( document ).ready(function() {
    ...
    comment.initUi();
});
</script>
```