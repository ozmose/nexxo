var reportObj = {
	initUi : function(){ console.log("reportObj.initUi()");
		$(".btn-want-send-report").off().click(function(){
			var aboutType = $(this).data("about-type");
			var aboutId = $(this).data("about-id");
			var whatType = $(this).data("what-type");
			var whatId = $(this).data("what-id");

			
	        $("#modal-quickview #modal-content").html(
	        		"<div class='ui-block-title'>" +
	        			"<h6 class='title'>" +
	        				"<i class='fa fa-spin fa-refresh'></i>" +
	        			"</h6>" +
	        		"</div>");

		    $("#modal-quickview").modal("show");
			$.ajax({
		        type: "POST",
		        url: "/report/modal-confirm",
		        // data: data,
		        success: function(res){

		          console.log("response of /report/modal-howitwork"); 
	              $("#modal-quickview #modal-content").html(res);
	              console.log("whatId", whatId);

			 	  var url = "/save-report/"+aboutType+"/"+aboutId;
			 	  if(typeof whatId != "undefined" && whatId != "" && whatId != null) 
			 	    if(typeof whatType != "undefined" && whatType != "" && whatType != null) 
			 	  	  url += "/" + whatType + "/" + whatId;

				 	$(".btn-send-report").off().click(function(){
				 		var reason = $("#report-reason").val();
		                $.ajax({
				          type: "POST",
				          url: url,
				          data: { reason: reason },
				          success: function(res){
				          	console.log(res);
				          	toastr.success(t.t("Report sent"));
					        
				          },
				          error: function(res){
				          	console.log(res);
				          	toastr.error("Une erreur s'est produite. aboutType:"+res.aboutType);
				          }
				          //dataType: dataType
				        });
		            });

		        },
		        error: function(error){
		          console.log("sharePost error", error);
		        }
		      });

			
		});

		$(".btn-report-vote").off().click(function(){
			var voteVal = $(this).data("vote-val");
			var aboutType = $(this).data("about-type");
			var aboutId = $(this).data("about-id");
			var commentId = $(this).data("comment-id");
			var reportId = $(this).data("report-id");

			var url = "/save-vote-report/"+voteVal+"/"+aboutType+"/"+aboutId;
			if(commentId != "") url += "/" + commentId;

			$.ajax({
	          type: "POST",
	          url: url,
	          //data: { membersSlug: membersSlug},
	          success: function(res){
	          	console.log("success", url);
	          	$(".report-container[data-report-id='"+reportId+"']").html(res);
				nexxo.progresBars();
				reportObj.initUi();
				comment.initUi();
	          	toastr.success(t.t("Vote sent"));
		        
	          },
	          error: function(res){
	          	console.log("error", url);
	          	toastr.error("Une erreur s'est produite. "+res.msg);
	          }
	          //dataType: dataType
	        });
		});

		$(".btn-report-vote-admin").off().click(function(){
			var reportId = $(this).data("report-id");
			var voteVal = $(this).data("vote-val");

			var url = "/close-reports-admin/"+reportId+"/"+voteVal;

			$.ajax({
	          type: "POST",
	          url: url,
	          success: function(res){
	          	console.log("success", url);
	          	$(".report-container[data-report-id='"+reportId+"']").html(res);
				nexxo.progresBars();
				reportObj.initUi();
	          	toastr.success(t.t("Vote sent"));
		        
	          },
	          error: function(res){
	          	console.log("error", url);
	          	toastr.error(t.t("Sorry, an error has occured ")+res.msg);
	          }
	          //dataType: dataType
	        });
		});

		
		$(".btn-show-vote-report-comment").off().click(function(){
			var reportId = $(this).data("report-id");
			if(  $(".report-container[data-report-id='"+reportId+"']").hasClass("d-none"))
				 $(".report-container[data-report-id='"+reportId+"']").removeClass("d-none");
			else $(".report-container[data-report-id='"+reportId+"']").addClass("d-none");
		});

		$(".btn-open-header-details").off().click(function(){
			var id = $(this).data("report-id");
			if($(".report-header[data-report-id='"+id+"'] .report-detail").hasClass("d-none")){
				$(".report-header[data-report-id='"+id+"'] .report-detail").removeClass("d-none");
			}else{
				$(".report-header[data-report-id='"+id+"'] .report-detail").addClass("d-none");
			}
		});

		nexxo.progresBars();

		reportObj.initBtnModalHowitwork();

		//  Activate the Tooltips
        //$('[data-toggle="tooltip"], [rel="tooltip"]').tooltip();
	},

	initBtnModalHowitwork : function(){
		console.log("initBtnModalHowitwork");
		$(".btn-modal-report-howitworks").off().click(function(){
	        $("#modal-quickview #modal-content").html(
	        		"<div class='ui-block-title'>" +
	        			"<h6 class='title'>" +
	        				"<i class='fa fa-spin fa-refresh'></i>" +
	        			"</h6>" +
	        		"</div>");

		    //$("#modal-quickview").modal("show");
			$.ajax({
		        type: "POST",
		        url: "/report/modal-howitwork",
		        // data: data,
		        success: function(res){
		          console.log("response of /report/modal-howitwork"); 
	              $("#modal-quickview #modal-content").html(res);
		        },
		        error: function(error){
		          console.log("sharePost error", error);
		        }
		      });
		});
	}
};