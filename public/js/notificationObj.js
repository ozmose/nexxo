var notificationObj = {

    timerNotif : false,

    initNotifUI : function(){
      $(".open-notification").off().click(function(){
        var aboutType = $(this).data("about-type");
        var aboutId = $(this).data("about-id");
        var aboutSlug = $(this).data("about-slug");
        var whatType = $(this).data("what-type");
        var whatId = $(this).data("what-id");
        var lastAuthorSlug = $(this).data("last-author-slug");
        var verb = $(this).data("verb");

        if(aboutType == "news"){
          if(location.pathname == "/my-dashboard"){
            //close notif window responsive
            $(".tab-content-responsive #notification, .tab-content-responsive #request").removeClass("active");
            notificationObj.openQuickviewNews(aboutId, aboutType, whatId, whatType);
          }else{
            location.href = BASE_URL + "/news/single-post/"+aboutId;
          }
        }
        if(aboutType == "page"){
          if(verb == "LIKE"             || verb == "DISLIKE" || 
             verb == "BECOME_FOLLOWER"  ||  verb == "BECOME_PARTICIPANT"){
            location.href = BASE_URL + "/page/"+lastAuthorSlug;
          }
          else{
            location.href = BASE_URL + "/page/"+aboutSlug;
          }
        }
      });

      $(".mCustomScrollbar.popup-notification, .mCustomScrollbar .notification-listriend-requestsenu-top")
      .css('maxHeight', '600px');

      $(".btn-show-dropdown-notif").off().click(function(){
         var countNotif = parseInt($(".label-avatar.notif").html());
         $(".label-avatar.notif").html("0").addClass("d-none");
         notificationObj.saveDateCheckNotif();
      });

      $(".btn-respond-friend-request").off().click(function(){
          var button = $(this);
          var slugPageReq = $(this).data("slug-pagereq");
          var slugPageRef = $(this).data("slug-pageref");
          var response = $(this).data("response");
          $.ajax({
            type: "POST",
            url: "/relation/respond-friend-request/"+slugPageReq+"/"+response+"/"+slugPageRef,
            // data: data,
            success: function(res){
              console.log("response of /respond-request-friend/"+slugPageReq+"/"+response+"/"+slugPageRef, res);
              if(response == true){
                if(res.pageType == "user")
                toastr.success(t.t("Request accepted : you are friends !"));
                else
                  toastr.success(t.t("Request accepted : here is a new member in your page !"));

                if(typeof res.notif != "undefined" && res.notif != "")
                  Chat.broadcastNotification(res.notif);
              }
              else
                toastr.error(t.t("Request refused"));

              button.closest("li").remove();
              var i = $(".label-avatar.bg-blue").html();
              i = parseInt(i); i--;
              if(i == 0) $(".label-avatar.bg-blue").html(0).addClass("d-none");
              else $(".label-avatar.bg-blue").html(i);
            },
          });
      }); 

      $(".btn-respond-admin-request").off().click(function(){
          var button = $(this);
          var slugPageReq = $(this).data("slug-pagereq");
          var slugPageRef = $(this).data("slug-pageref");
          var response = $(this).data("response");
          $.ajax({
            type: "POST",
            url: "/relation/respond-admin-request/"+slugPageReq+"/"+response+"/"+slugPageRef,
            // data: data,
            success: function(res){
              console.log("response of /respond-admin-request/"+slugPageReq+"/"+response+"/"+slugPageRef, res);
              if(response == true)
                toastr.success(t.t("Request accepted : you added a new admin !"));
              else
                toastr.error(t.t("Request refused"));

              if(typeof res.notif != "undefined" && res.notif != "")
                Chat.broadcastNotification(res.notif);

              button.closest("li").remove();
              var i = $(".label-avatar.bg-blue").html();
              i = parseInt(i); i--;
              if(i == 0) $(".label-avatar.bg-blue").html(0).addClass("d-none");
              else $(".label-avatar.bg-blue").html(i);
            },
          });
      });

      $(".btn-refresh-notif").off().click(function(){
        notificationObj.updateActivityNotif();
        notificationObj.activateTimerNotif(false);
        notificationObj.activateTimerNotif(true);
      }); 


      //change chat status
      console.log("init change status chat.js", $("ul.chat-settings li.btn-change-chat-status").html());
      $("ul.chat-settings li.btn-change-chat-status").click(function(){
          var status = $(this).data("status");
          //change color status in top bar (near my username)
          $(".header-content-wrapper .author-page .author-thumb .icon-status.menu-top")
            .removeClass("online offline away").addClass(status);

          console.log("change status", status);
          if(SOCKET_ENABLED == "true"){
            console.log("change status send socket", status);
            Chat.conn.send(JSON.stringify({ "action" : "changeStatus",
                                            "slug" : Chat.clientInformation.senderSlug,
                                            "status" : status
                                        }));
          }else{
            Chat.changeChatStatus(status);
          }
      });

      if(SOCKET_ENABLED == "false"){
          $("ul.chat-settings li.btn-change-chat-status").click(function(){
            var status = $(this).data("status");
            //change color status in top bar (near my username)
            $(".header-content-wrapper .author-page .author-thumb .icon-status.menu-top")
                .removeClass("online offline away").addClass(status);

            console.log("change status", status);
            Chat.changeChatStatus(status);
        });
      }
    },

    saveDateCheckNotif : function(){
      $.ajax({
          type: "POST",
          url: "/save-date-check-notif",
          success: function(res){
            console.log("saveDateCheckNotif success", res);
          },
          error: function(error){
            console.log("saveDateCheckNotif error", error);
          }
        });
    },

    openQuickviewNews : function(aboutId, aboutType, whatId, whatType){
      nexxo.scrollTo("#newsfeed-items-grid");
      $("#msg-loading-preview").removeClass("d-none");
      $("#msg-loading-preview i.fa").addClass("fa-spin");
      $("#newsfeed-items-grid").html("");
      $.ajax({
        type: "POST",
        url: "/news/quickview-post/"+aboutId,
        // data: data,
        success: function(res){
          console.log("response of /news/quickview-post/"+aboutId, whatType);
          $("#newsfeed-items-grid").html(res);
          $("#msg-loading-preview").addClass("d-none");
          $("#msg-loading-preview i.fa").removeClass("fa-spin");
          
          newsObj.initSharePost();
          newsObj.initLikePost();
          comment.initUi();
          comment.initFormAddComment();
                    
          if(whatType != ""){
            var ide = ".comment-block[data-parent-id='"+aboutId+"'][data-parent-type='"+aboutType+"'] "+
                      "ul.comments-list li[data-"+whatType+"-id='"+whatId+"'] p";

            $(ide).addClass("notified");

            $(".comment-block[data-parent-id='"+aboutId+"'][data-parent-type='"+aboutType+"'] "+
                ".container-answers[data-comment-id='"+whatId+"']")
                  .parents(".container-answers")
                  .removeClass("d-none");
          }
          nexxo.scrollTo("#newsfeed-items-grid");
        },
      });
    },

    updateActivityNotif : function(){
      console.log("updateActivityNotif");
        //si le chargement est déjà en cours, on quite
        if($(".btn-refresh-notif .fa").hasClass("fa-spin")) return;
            
        $(".btn-refresh-notif .fa").addClass("fa-spin");
        $.ajax({
          type: "POST",
          url: "/get-all-activity",
          success: function(res){
            console.log("updateActivityNotif success", res);

            /* USED WHEN NO SOCKET ENABLED */
            var countRequestOld = $("#container-activity .label-avatar.friend-requests").html();
            var countNotifOld = $("#container-activity .label-avatar.activity-feed").html();
            /* USED WHEN NO SOCKET ENABLED */

            $("#container-activity").html(res.htmlNotif);

            //dispatch notif in UI
            var notifs = $("#container-activity .notification-list.activity-feed").html();
                        
            //dispatch request in UI (responsive)
            var request = $("#container-activity .notification-list.friend-requests").html();
            $(".tab-content-responsive .notification-list.friend-requests.menu-top").html(request);

            var countRequest = $("#container-activity .label-avatar.friend-requests").html();
            $(".mobile-app-tabs .label-avatar.friend-requests").html(countRequest);
            var showRequest = !$("#container-activity .label-avatar.friend-requests").hasClass('d-none');
            if(showRequest) $(".mobile-app-tabs .label-avatar.friend-requests").removeClass('d-none');

            var countNotif = $("#container-activity .label-avatar.activity-feed").html();
            $(".mobile-app-tabs .label-avatar.activity-feed").html(countNotif);
            var showNotif = !$("#container-activity .label-avatar.activity-feed").hasClass('d-none');
            if(showNotif) $(".mobile-app-tabs .label-avatar.activity-feed").removeClass('d-none');

            $(".btn-refresh-notif .fa").removeClass("fa-spin");

            /* show notif toastr + alert sonore  */
            //console.log("count notif :", parseInt(countRequestOld), parseInt(countRequest), parseInt(countNotifOld), parseInt(countNotif));
            if(parseInt(countRequestOld) < parseInt(countRequest) || parseInt(countNotifOld) < parseInt(countNotif)){
              Chat.upTitlePage();
             
              //met à jour la liste des notif dashboard
              $("ul.notification-list.activity-feed").html(notifs);
              //disable links in notification (img illustration media)
              $("ul.notification-list.activity-feed .open-notification a")
                .attr("href", "javascript:").attr("target", "");

              nexxo.playSound("notification");
              notificationObj.toastrNotifNotRead();
            }else{
              if(countNotifOld == ""){
                //met à jour la liste des notif dashboard (au premier chargement, si la liste est vide)
                $("ul.notification-list.activity-feed").html(notifs);
                //disable links in notification (img illustration media)
                $("ul.notification-list.activity-feed .open-notification a")
                  .attr("href", "javascript:").attr("target", "");
              }
            }
            
 
            //prepend online (all)
            $.each($("ul.chat-users .js-chat-open"), function(li, data){ 
              //console.log("foreach message", li, data);
              var slug = $(this).data("slug");
              
              //console.log("hasClass online", slug, $(".js-chat-open[data-slug='"+slug+"'] .icon-status").hasClass("online"));
              if($(".js-chat-open[data-slug='"+slug+"'] .icon-status").hasClass("online")){
                //console.log(slug, "hasClass online");
                notificationObj.prependChatContact(slug, data.count);
              }
            });

            //prepend online (only users)
            $.each($("ul.chat-users .js-chat-open"), function(li, data){ 
              var slug = $(this).data("slug");
              if($(".js-chat-open[data-slug='"+slug+"'] .icon-status").hasClass("online")){
                var type = $("li.js-chat-open[data-slug='"+slug+"']").data("type");
                //console.log("hasClass online type", "type");
                
                if(type == "user")
                  notificationObj.prependChatContact(slug, data.count);
              }
            });

            //prepend new messages
            var newMsg = false; var c = 0;
            $.each(res.messagesToRead, function(senderSlug, data){ 
              //move li to the top of the list, if count (new msg) is >= 1
              var currentCount = 
              $(".js-chat-open[data-slug='"+senderSlug+"'] .count-toread-msg").html();
              //console.log("parseInt(currentCount)", parseInt(currentCount), data.count, senderSlug);
              if(parseInt(currentCount) >= 1){
                newMsg = true;
                $(".js-chat-open[data-slug='"+senderSlug+"'] .count-toread-msg")
                            .html(currentCount).data("count", currentCount)
                            .removeClass("d-none");
                notificationObj.prependChatContact(senderSlug, currentCount);

              } c+=data.count;
              //Chat.initNewSender(data);
            });


            /* USED WHEN SOCKET NOT ENABLED */
            if(SOCKET_ENABLED == "false") {
                //display if new ChatMessage
                //console.log("display if new ChatMessage", res.messagesToRead);
                $.each(res.status, function(senderSlug, status){ 
                    console.log("foreach status", senderSlug, status);
                    $(".js-chat-open[data-slug='"+senderSlug+"'] .icon-status").
                      removeClass("online offline away").addClass(status).data("status", status);
                });
            }
            /* USED WHEN NO SOCKET ENABLED */


            if(c > 0) $(".mobile-app-tabs .label-avatar.message").html(c).removeClass("d-none");
            if(newMsg == true) nexxo.playSound("message");


            setTimeout(function(){
              notificationObj.initNotifUI(); 
            }, 500);
            
          },
          error: function(error){
            console.log("updateActivityNotif error", error);
            $(".btn-refresh-notif .fa").removeClass("fa-spin");
          }
        });
    },

    prependChatContact : function(senderSlug, count){
      //console.log("prependChatContact", senderSlug, count);
      $(".js-chat-open[data-slug='"+senderSlug+"'] .count-toread-msg").data("count", count);

      var status = $(".js-chat-open[data-slug='"+senderSlug+"'] .icon-status").data("status");
      //alert(status);
      //move li to the top of the list
      var liHtml = $(".sidebar--large li.js-chat-open[data-slug='"+senderSlug+"']").clone();
      $(".sidebar--large li.js-chat-open[data-slug='"+senderSlug+"']").remove();
      $(".sidebar--large ul.chat-users").prepend(liHtml);
      var liHtml = $(".sidebar--small li.js-chat-open[data-slug='"+senderSlug+"']").clone();
      $(".sidebar--small li.js-chat-open[data-slug='"+senderSlug+"']").remove();
      $(".sidebar--small ul.chat-users").prepend(liHtml);
      $(".js-chat-open[data-slug='"+senderSlug+"'] .icon-status").data("status", status);
      Chat.initBtnStartChat();
      //reactivate tooltip after remove/prepend
      $(".author-thumb[data-toggle='tooltip']").tooltip();
    },

    activateTimerNotif : function(bool){
      if(bool == true){
        if(notificationObj.timerNotif == false){
          console.log("start timer notif", INTERVAL_TIMER_NOTIF);
          notificationObj.timerNotif = setInterval(function(){
            notificationObj.updateActivityNotif();
          }, INTERVAL_TIMER_NOTIF);
          console.log("activateTimerNotif", bool);
        }
      }else{
        if(notificationObj.timerNotif != false)
          clearInterval(notificationObj.timerNotif);
        notificationObj.timerNotif = false;
        console.log("activateTimerNotif", bool);
      }
    },


    toastrNotifNotRead : function(){
      var i = 0;
      var max = 3;
      var nbUnread = $("#site-header ul.notification-list.activity-feed li.unread").length;
      $.each($("#site-header ul.notification-list.activity-feed li.unread"), function(){
        if(i < max){
          var html = $(this)[0].outerHTML;
          console.log("html notif", html);
          toastr.info("<ul class='notification-list'>"+html+"</ul>");
        }else{
          if(i == max)
          toastr.info("<ul class='notification-list'>"+
                        "<li class='open-notification d-inline-block full-width unread'>"+
                          "<b>+ " + (nbUnread-max) + " " + t.t("other notification(s)") + "</b>" +
                        "</li>"+
                      "</ul>");
        } i++;
      });
    },

    getVerbStr : function(notif){
      console.log("getVerbStr", notif);

      if(notif.verb == "COMMENT"){
      	return "commented on";
      }
      else if(notif.verb == "SHARED"){
      	return "shared";
      }
    },
    getAboutStr : function(notif){
      console.log("getVerbStr", notif);

      if(notif.aboutType == "news"){
      	return "a publication";
      }
      else if(notif.aboutType == "Comment"){
      	return "a comment";
      }
      else if(notif.aboutType == "Page"){
      	return "the page";
      }
    }

}