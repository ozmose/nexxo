var newsObj = {

	myPosition : new Array(0, 0),
	myMarker : false,
	scopeMarker : false,

	currentRadius : 10000,
	currentKm : 10,
	currentCircleMarker : false,

	scopeCircleMarker : false,

	mainCanvasId : "map-container",
	shareCanvasId : "map-container-share",

    mediaLoading : false,
    mediasLoaded : new Array(),
	mediasMetadata : new Array(),

	loadingMore : false,

	sendPost : function(){
		var text = $("#news_text").val();
		
		if(text == "" && $("#news_image").val() == "") {
			toastr.error(t.t("Your text message is empty. Please write something before to send your post !"));
			return;
		}
		//console.log("newsObj.currentCircleMarker", newsObj.currentCircleMarker);
		//var token = $("#news__token").val();
		var data = {
				news : {
					//_token : token,
					text : text,
					scopeType : $("#news_sharedBy_scopeType").val(),
					isSharable : $("#news_check_sharable").prop('checked'),
					//original : true,
					//sharable : true
				}
		};
		//console.log("first data post", data); return;
		if(data.news.scopeType == "localised" && newsObj.currentCircleMarker != false){
			var circlePolygon = L.Circle.toPolygon(newsObj.currentCircleMarker, 50, Sig.map);
			//console.log("circlePolygon", circlePolygon);

			var coords = [];
			$.each(circlePolygon, function(key, coord){
				coords.push([coord.lng, coord.lat]);
			});
			coords.push(coords[0]);

			var origin = newsObj.myMarker.getLatLng();
			origin = [origin.lng, origin.lat];

			data.news.scopeGeo = {  'type' : "Polygon", 'coordinates' : coords, 'origin' : origin };
			data.news.radiusKm = parseInt(newsObj.currentKm);
			data.news.scopeType = "localised";
		}else{
		
			if(data.news.scopeType == "groups"){
				if($("#news_scopeGroupsHidden").val() != ""){
					var groupStr = $("#news_scopeGroupsHidden").val().replace(/\s/g, '');
					var allGroups = groupStr.split(",");
					data.news.scopeGroups = allGroups;
					data.news.scopeType = "groups";
				}
			}

		}
		if(typeof data.news.scopeType == "undefined" || data.news.scopeType == "") 
			data.news.scopeType = "friends";

		//var cnt=0; $.each(news.mediasLoaded, function(key, media){ cnt++; }); ctn;
		console.log("news.mediasLoaded.length", newsObj.mediasMetadata.length);
		if(newsObj.mediasMetadata.length > 0){
			//var medias = new Array();
			// $.each(newsObj.mediasMetadata, function(key, media){
			// 	medias.push(media);
			// });
			var metadatas = new Array();
			$.each(newsObj.mediasMetadata, function(key, meta){
				if(meta.preview == true) metadatas.push(meta);
			});
			
			if(metadatas.length > 0)
				data.news.medias = metadatas;
		}

		console.log("has image ?", $("#news_image").val());
		if($("#news_image").val() != ""){
			data.news.image = $("#news_image").val();
		}

		//data.news = new Array(data.news);
		console.log("new post", data);
		//return;

		
		$("#block-sending-msg article .alert-sending").removeClass("d-none");
		$("#block-sending-msg article .alert-sending .fa-refresh").addClass("fa-spin");
		$("#block-sending-msg article .alert-success, #block-sending-msg article .alert-danger").addClass("d-none");
		$("#block-sending-msg").removeClass("d-none");
		
		//return;
		$.ajax({
          type: "POST",
          url: $("form[name='news']").attr("action"),
          //async:true,
		  data: data,
          success: function(res){
          	//console.log("post send", res);
          	if(res.error == false){
				$("#block-sending-msg article .alert-sending, #block-sending-msg article .alert-danger").addClass("d-none");
				$("#block-sending-msg article .alert-sending .fa-refresh").removeClass("fa-spin");
				$("#block-sending-msg article .alert-success").removeClass("d-none");
          		
          		$("#news_text").val("");
				$("#news_image").val("");
				$("#preview-upload-image .cr-image").attr("src", "");
				$('#preview-upload-image').addClass("d-none");

				$("#newsfeed-items-grid").prepend(res.html);
				
				newsObj.mediasMetadata = new Array();
				newsObj.mediasLoaded = new Array();
				$("#media-shared-container").html("");

				newsObj.closeForm();
	          	newsObj.initSharePost();
	          	newsObj.closeMapScope();
		        newsObj.initLikePost();
		        newsObj.scalePostSize();
		        newsObj.initShowScopePost();
		        comment.initUi();
	          	comment.initFormAddComment();

	          	if(typeof res.notif != "undefined" && res.notif != "")
            		Chat.broadcastNotification(res.notif);

	          	setTimeout(function(){
					$("#block-sending-msg").addClass("d-none");
          		}, 2000);
				
          	}else{
          		$("#block-sending-msg article .alert-sending, #block-sending-msg article .alert-success").addClass("d-none");
				$("#block-sending-msg article .alert-sending .fa-refresh").removeClass("fa-spin");
				$("#block-sending-msg article .alert-danger").removeClass("d-none");
          		$("#block-sending-msg article .alert-danger").html("<b>"+res.errorMsg+"</b>");
          	}
          }
        });
	},

	initInputScopeGroup : function(){
		$("#display-scope-groups").html("");
		$("#news_scopeGroupsHidden").val("");
		//cancel selection of all group selected
		$.each($("#modal-select-group-scope ul.dropdown-menu li.selected a"), function(key, val){
			val.click();
		});
	},

	initSharePost : function(){
		$(".btn-share-post").off().click(function(){
	      console.log("test");
	      var postId = $(this).data("post-id");
	      var signedId = $(".form-share-post #signed-id").val();
	      var text = $(".form-share-post textarea#txt-share-post").val();
	      var confirm = (typeof $(this).data("share-confirm") != "undefined") ? $(this).data("share-confirm") : 'false';
	      
	      var data = {
				news : {
					isSharable : $(".form-share-post #news_check_sharable").prop('checked'),
					scopeType : $(".form-share-post #news_sharedBy_scopeType").val(),
					signedId : signedId,
					text : text
				}
		  };
		
		  //console.log("first data post", data); return;
		  if(data.news.scopeType == "localised" && newsObj.currentCircleMarker != false){
			var circlePolygon = L.Circle.toPolygon(newsObj.currentCircleMarker, 50, Sig.map);
			//console.log("circlePolygon", circlePolygon);

			var coords = [];
			$.each(circlePolygon, function(key, coord){
				coords.push([coord.lng, coord.lat]);
			});
			coords.push(coords[0]);

			var origin = newsObj.myMarker.getLatLng();
			origin = [origin.lng, origin.lat];

			data.news.scopeGeo = {  'type' : "Polygon", 'coordinates' : coords, 'origin' : origin };
			data.news.radiusKm = parseInt(newsObj.currentKm);
			data.news.scopeType = "localised";
		  }

	      //console.log("confirm", typeof confirm);
	      console.log("data before share news", data);
	      
	      $.ajax({
	        type: "POST",
	        url: "/news/share-post/"+postId+"/"+confirm,
	        data: data,
	        success: function(res){
	          console.log("response of xxx /news/share-post/"+postId+"/"+confirm); 
	          console.log("res", res);
	          
	          if(res.error == "true"){
	          	toastr.error(res.errorMsg);
		        setTimeout(function(){
	              $("article.post[data-news-id='"+postId+"'], "+
	              	".comment-block[data-parent-id='"+postId+"'], "+
	              	"li.open-notification[data-about-id='"+postId+"'][data-about-type='news']").remove();
	            }, 4000);
		        return;
	          }

	          if(confirm == "false"){
		          $("#modal-quickview #modal-content").html(res.html);
		          $("#modal-quickview").modal("show");
	          	  newsObj.initFormSharePost();

		      }else{
		      	if(typeof res.notif != "undefined" && res.notif != "")
	        		Chat.broadcastNotification(res.notif);
	        		
		        toastr.success(t.t("Post shared !"));
		        newsObj.closeForm();
	          	newsObj.closeMapScope();
		      }
	        },
	        error: function(error){
	          console.log("sharePost error", error);
		      toastr.error(t.t("Error sharing post"));
	        }
	      });
	    });

	    $("#btn-cancel-share-post").off().click(function(){
	    	// $("#modal-quickview #modal-content").html("");
		    // $("#modal-quickview").modal("hide");
	          	  
	    	newsObj.closeForm();
	        newsObj.closeMapScope();
	    });
	},

	initFormSharePost : function(){
		nexxo.Materialize();

        newsObj.closeMapScope();
        $(".form-share-post #check-use-aroundme").prop('checked', false);
        $(".form-share-post #check-use-aroundme").change(function(){
          console.log("isChecked ?", $(".form-share-post #check-use-aroundme").is(':checked'));
          if($(".form-share-post #check-use-aroundme").is(':checked')){
            newsObj.showMapScope();
          }else{
          	//$("#map-container").addClass("d-none");
          	newsObj.closeMapScope();
          }
        });

        $(".form-share-post #news_check_sharable").prop('checked', false);
        $(".form-share-post #news_check_sharable").change(function(){
          console.log( $(".form-share-post #news_check_sharable").is(':checked'));
          if($(".form-share-post #news_check_sharable").is(':checked')){
            //newsObj.showMapScope();
          }else{
          	//$("#map-container").addClass("d-none");
          	//newsObj.closeMapScope();
          }
        });

        $(".form-share-post #formControlRange").val(10);
        $(".form-share-post #formControlRange").change(function(){
			console.log('range change', $(this).val(), typeof $(this).val());
			var dist = parseInt($(this).val());
			if(dist < 5) dist = 2*dist;
			if(dist == 0) dist = 2;
			/*if(dist >= 5 && dist < 10) dist = 0.5*dist;
			if(dist > 50 && dist < 60) dist = dist*2;
			if(dist >= 60 && dist < 70) dist = dist*3;
			if(dist >=70 && dist < 80) dist = dist*4;
			if(dist >=80 && dist <= 90) dist = dist*5;
			if(dist >=90 && dist <= 100) dist = dist*6;
			if(dist == 0) dist = 0.1;*/

			/*if(dist < 5) dist = 0.2*dist;
			if(dist >= 5 && dist < 10) dist = 0.5*dist;
			if(dist > 50 && dist < 60) dist = dist*2;
			if(dist >= 60 && dist < 70) dist = dist*3;
			if(dist >=70 && dist < 80) dist = dist*4;
			if(dist >=80 && dist <= 90) dist = dist*5;
			if(dist >=90 && dist <= 100) dist = dist*6;
			if(dist == 0) dist = 0.1;*/
			
			var km = parseFloat(dist).toFixed(1);
			newsObj.currentRadius = km*1000;
			newsObj.currentKm = km;
			$(".form-share-post .around-me-distance").html(km + " km");
			newsObj.showMapScope();
		});

		$(".form-share-post button#dropdown-scope-news").focusout(function(){ console.log("what !!!");
			setTimeout(function(){
				$(".form-share-post #dropdown-menu-scope-news").removeClass("show");
			}, 200);
		});
		$(".form-share-post button#dropdown-target-news").focusout(function(){ console.log("what2 !!!");
			setTimeout(function(){
				$(".form-share-post #dropdown-menu-target-news").removeClass("show");
			}, 200);
		});

		$(".form-share-post .btn-select-scope-type").click(function(){
			var scopeType = $(this).data("scope-type");
			newsObj.initInputScopeGroup();
			
			if(scopeType != "localised")
				newsObj.closeMapScope();
			else {
				$(".form-share-post #check-use-aroundme").prop('checked', true);
				newsObj.showMapScope();
			}

			$(".form-share-post #news_sharedBy_scopeType").val(scopeType);

			$(".form-share-post #scope-lbl").html("<i class='fa fa-"+ $(this).data("icon-lbl") +"'></i> " + $(this).data("scope-lbl"));
		});

		$(".form-share-post #news_sharedBy_scopeType").val("friends");

		$(".form-share-post .btn-select-signed").click(function(){
			var signedPageId = $(this).data("signed-page-id");
			var signedPageImgSrc = $(this).data("signed-page-imgprofil");

			//change image in form and in button select signed
			$(".form-share-post #dropdown-target-news").find("img").attr("src", '/uploads/imgProfil/'+signedPageImgSrc);
			$(".form-share-post .news-feed-form form .author-thumb img").attr("src", '/uploads/imgProfil/'+signedPageImgSrc);
			
			console.log("signedPageId", signedPageId, signedPageImgSrc);
			$("#modal-select-group-scope ul.dropdown-menu a").hide();
			$("#modal-select-group-scope ul.dropdown-menu a.group-in-page-"+signedPageId).show();

			//change url form to signe to the new page selected
			/*var currentUrl = $("form[name='news']").attr("action");
			var split = currentUrl.split("/");
			console.log("split", split);
			var newUrl = "/" + split[1] + "/" + split[2] + "/" + signedPageId + "/" + split[4];
			$(".form-share-post form[name='news']").attr("action", newUrl);
			*/
			$(".form-share-post #signed-id").val(signedPageId);
			

			if($(this).data("lat") != false && $(this).data("lng") != false)
			newsObj.myPosition = new Array($(this).data("lat"), $(this).data("lng"))

			//clear old group selected
			newsObj.initInputScopeGroup();
			newsObj.closeMapScope();
		});

		$(".form-share-post #signed-id").val(APP_USER_ID);

		$(".form-share-post #btn-add-group-to-scope").click(function(){ console.log("onclick #btn-add-friend-to-group");
			var groupId = $(this).data("group-id");
			var membersId = $("#modal-select-group-scope .show-tick .filter-option").html();
			newsObj.initInputScopeGroup();
			
			if(membersId != "Nothing selected"){
				$(".form-share-post #news_scopeGroupsHidden").val(membersId);
				var groupStr = membersId.replace(/\s/g, '');
				var allGroups = groupStr.split(",");
				//console.log("membersId", membersId);
				$.each(allGroups, function(key, id){
					var name = $("#option-select-group-"+id).data("name");
					$("#display-scope-groups").append(
						"<span class='badge badge-secondary float-left padding-5 letter-white margin-right-5 margin-bottom-5'>"+
							"<i class='fa fa-check'></i> "+name+
						"</span> ");
				});
			}
			//console.log("value scopeGroups", $("#news_scopeGroupsHidden").val());
		});

	    newsObj.initSharePost();
	},

	initShowScopePost : function(){
		$(".btn-show-scope-news").off().click(function(){
			var lat = $(this).data("lat");
			var lng = $(this).data("lng");
			var radius = $(this).data("radius");
			var newsId = $(this).data("news-id");

			var author = {
				name : $(this).data("signed-name"),
				thumb : $(this).data("signed-thumb"),
				type : $(this).data("signed-type"),
				slug : $(this).data("signed-slug"),
				address : $(this).data("signed-address"),
			};

			var radius = $(this).data("radius");
			var newsId = $(this).data("news-id");
			var radius = $(this).data("radius");

			var coordinates = [lat, lng];
			
			newsObj.closeMapScope();
			
    		console.log("html ?", $("#map-container-scope-"+newsId).html());
			if($("#map-container-scope-"+newsId).html() != ""){
				if(Sig.map != false){
					Sig.map.remove(); Sig.map = false;
	    			Sig.mapIsLoaded = false;
	    		}
	    		$("article .map-container-scope").html("").removeClass().addClass("map-container-scope d-none");
	    	}else{
	    		if(Sig.map != false){
					Sig.map.remove(); Sig.map = false;
	    			Sig.mapIsLoaded = false;
	    		}
	    		$("article .map-container-scope").html("").removeClass().addClass("map-container-scope d-none");
	    		newsObj.showMapScopeNews(newsId, coordinates, radius, author);
	    	}
		});
	},

    closeMapScope : function (){ console.log("closeMapScope");
    	if(newsObj.currentCircleMarker != false)
    		Sig.map.removeLayer(newsObj.currentCircleMarker);
    	
    	newsObj.currentCircleMarker = false;

		$("#news_sharedBy_scopeType").val("friends");
		var lbl = $(".btn-select-scope-type[data-scope-type='friends']").data("scope-lbl");
		var icon = $(".btn-select-scope-type[data-scope-type='friends']").data("icon-lbl");
		$("#scope-lbl").html("<i class='fa fa-"+icon+"'></i> "+lbl+"");

		$(".check-use-aroundme").prop('checked', false);
	    $(".container-scoping").addClass("d-none").removeClass("d-flex");

	    var canvas = newsObj.mainCanvasId;
		if($(".form-share-post").is(":visible"))
			canvas = newsObj.shareCanvasId;

		$("#"+canvas).hide(200);
		setTimeout(function(){ $("#"+canvas).show().addClass("d-none"); }, 500);
    },

    scalePostSize : function(){
    	$.each($(".post .textHtml") ,function(key, el){
    		//console.log("post size height", $(el).height());
    		if($(el).height() > 300){
    		 	$(el).addClass("limited");
    		 	var id = $(el).data("post-id");
    		 	//console.log("post size limited", id);
    			$(".btn-read-more-post[data-post-id='"+id+"']").removeClass("d-none");
    		}
    	});

    	$(".btn-read-more-post").off().click(function(){
		 	var id = $(this).data("post-id");
		 	console.log(".btn-read-more-post click", id);
    		$(".post .textHtml[data-post-id='"+id+"']").toggleClass("limited");
    	});
    },

    initLikePost : function(){
		$(".btn-post-like").off().click(function(){
			newsObj.sendLikePost($(this).data("post-id"), $(this).data("like-type"), $(this));
		});
		$(".btn-post-dislike").off().click(function(){
			newsObj.sendLikePost($(this).data("post-id"), $(this).data("like-type"), $(this));
		});
		reportObj.initUi();
		newsObj.initEditPost();
    },

	initEditPost : function(){
	    $(".btn-edit-post").off().click(function(){
	      var newsId = $(this).data("news-id");
	      var newsText = $("article.post[data-news-id='"+newsId+"'] p.textHtml.main-text").text();
	      newsText = newsText.trim();

	      $("article.post[data-news-id='"+newsId+"'] p.textHtml.main-text").addClass("d-none");
	      $("article.post[data-news-id='"+newsId+"'] .input-edit-news").removeClass("d-none").val(newsText);
	      $("article.post[data-news-id='"+newsId+"'] .btn-update-news").removeClass("d-none");
	      $("article.post[data-news-id='"+newsId+"'] .btn-cancel-update-news").removeClass("d-none");


	      $("article.post[data-news-id='"+newsId+"'] .btn-update-news").off().click(function(){
	          var newsId = $(this).data("news-id");
	          var text = $("article.post[data-news-id='"+newsId+"'] .input-edit-news").val();
	          newsObj.updatePost(newsId, text);
	      });
	      $("article.post[data-news-id='"+newsId+"'] .btn-cancel-update-news").off().click(function(){
	      	  $("article.post[data-news-id='"+newsId+"'] p.textHtml.main-text").removeClass("d-none");
	          $("article.post[data-news-id='"+newsId+"'] .input-edit-news").addClass("d-none").val(newsText);
		      $("article.post[data-news-id='"+newsId+"'] .btn-update-news").addClass("d-none");
		      $("article.post[data-news-id='"+newsId+"'] .btn-cancel-update-news").addClass("d-none");
	      });

	    });

	    $(".btn-delete-post").off().click(function(){
	      var newsId = $(this).data("news-id");
	      $("article.post[data-news-id='"+newsId+"'] .conf-delete-news").removeClass("d-none");

	      $("article.post[data-news-id='"+newsId+"'] .btn-conf-delete-news").off().click(function(){
	          //var newsId = $(this).data("news-id");
	          newsObj.deletePost(newsId);
	      });

	      $("article.post[data-news-id='"+newsId+"'] .btn-cancel-delete-news").off().click(function(){
	        //var newsId = $(this).data("news-id");
	        $("article.post[data-news-id='"+newsId+"'] .conf-delete-news").addClass("d-none");
	      });

	    });
	},

	updatePost : function(newsId, text){
	    $.ajax({
	        type: "POST",
	        url: "/news/edit-text/"+newsId,
	        data: { "news" : { "text" : text, "_token" : $("#news__token").val() }},
	        success: function(res){
	          console.log("response of /news/edit-text/"+newsId, res);
	          if(res.error == false){ 
	            toastr.success(t.t("Your post has been edited"));
	            $("article.post[data-news-id='"+newsId+"'] p.textHtml.main-text").removeClass("d-none").html(res.newText);
			    $("article.post[data-news-id='"+newsId+"'] .input-edit-news").addClass("d-none");
			    $("article.post[data-news-id='"+newsId+"'] .btn-update-news").addClass("d-none");
				$("article.post[data-news-id='"+newsId+"'] .btn-cancel-update-news").addClass("d-none");
	          }else{
	            toastr.error("Your post has not been edited");
	          }
	          
	          if(typeof res.notif != "undefined" && res.notif != "")
	            Chat.broadcastNotification(res.notif);
	        },
	        error: function(error){
	          console.log("send post like error", error);
	        }
	    }); 
	},

	deletePost : function(newsId, text){
	    $.ajax({
	        type: "POST",
	        url: "/news/delete/"+newsId,
	        data: { "news" : { "text" : text, "_token" : $("#news__token").val() }},
	        success: function(res){
	          console.log("response of /news/delete/"+newsId, res);
	          if(res.error == false){ 
	            toastr.success(t.t("Your news has been deleted"));
	            $("article.post[data-news-id='"+newsId+"']").remove();
	            $(".comment-block[data-parent-id='"+newsId+"']").remove();
	          }else{
	            toastr.error("Your news has not been deleted");
	          }
	          
	          if(typeof res.notif != "undefined" && res.notif != "")
	            Chat.broadcastNotification(res.notif);
	        },
	        error: function(error){
	          console.log("delete news like error", error);
	        }
	    }); 
	},

	getAntiLikeType : function(likeType){
    	if(likeType == "likes") return "dislikes";
    	if(likeType == "dislikes") return "likes";
  	},

    sendLikePost : function(postId, likeType, button){
    	$.ajax({
	        type: "POST",
	        url: "/news/send-like/"+postId+"/"+likeType,
	        // data: data,
	        success: function(res){
	          console.log("response of /news/send-like/"+postId+"/"+likeType, res);

	          if(res.error == "true"){
	          	toastr.error(res.errorMsg);
		        setTimeout(function(){
	              $("article.post[data-news-id='"+postId+"'], "+
	              	".comment-block[data-parent-id='"+postId+"'], "+
	              	"li.open-notification[data-about-id='"+postId+"'][data-about-type='news']").remove();
	            }, 4000);
		        return;
	          }

	          // if(res.addError == false){ 
	          //     if(likeType=="likes") toastr.success("You like it !");
	          //     if(likeType=="dislikes") toastr.error("You dislike it !");
	          // }else{
	          //     if(likeType=="likes") toastr.success("You do not like it anymore");
	          //     if(likeType=="dislikes") toastr.error("You do not dislike it anymore");
	          // }

	          var antiLikeType = newsObj.getAntiLikeType(likeType);
              $("a[data-like-type='"+likeType+"'][data-post-id='"+postId+"'] span b").html(res.newTotal[likeType]);
              $("a[data-like-type='"+antiLikeType+"'][data-post-id='"+postId+"'] span b").html(res.newTotal[antiLikeType]);

              res.notif["newTotal"] = res.newTotal;
              
	          if(typeof res.notif != "undefined" && res.notif != "")
            	Chat.broadcastNotification(res.notif);
	        },
	        error: function(error){
	          console.log("send post like error", error);
	        }
	    });
    },

    showMapScope : function(){
    	if(newsObj.myPosition != false && newsObj.myPosition[0] != "false"){
    		console.log("showMapScope");
    		var canvas = newsObj.mainCanvasId;
    		if($(".form-share-post").is(":visible")){
    			//$("#"+newsObj.mainCanvasId).html("").removeClass().addClass("form-group d-none");
    			if(Sig.map != false) Sig.map.remove();
    			canvas = newsObj.shareCanvasId;
    			$(".news-feed-form .check-use-aroundme").prop('checked', false);
    		}
    		else if($("#"+canvas).html() == "" && Sig.map != false){
    			Sig.map.remove(); Sig.map = false;
    			Sig.mapIsLoaded = false;
    			$("article .map-container-scope").html("").removeClass().addClass("map-container-scope d-none");
    		}

			console.log("myPos", newsObj.myPosition, canvas);
			Sig.init(canvas, function(){
	        	$("#"+canvas).removeClass("d-none").show(200);
	        	$(".container-scoping").removeClass("d-none").addClass("d-flex");

	        	var scrollWheelZoom = ($("body").width() < 767) ? false : true;
	            Sig.loadMap(canvas, {
	                center : new L.LatLng(newsObj.myPosition[1], newsObj.myPosition[0]),
	                zoom: 10,
	                maxZoom: 14,
                	scrollWheelZoom: scrollWheelZoom
	            });

	            if(newsObj.myMarker != false)
	            	Sig.map.removeLayer(newsObj.myMarker);

	            newsObj.myMarker = Sig.showSingleMarker(newsObj.myPosition, "default");

	            newsObj.myMarker.dragging.enable();
	            newsObj.myMarker.on('dragend', function(e){
	                //this.openPopup();  
	                newsObj.myPosition = [this.getLatLng().lng, this.getLatLng().lat];
	                newsObj.showMapScope();
	            });

	            if(newsObj.currentCircleMarker != false){ console.log("removeLayer");
	                Sig.map.removeLayer(newsObj.currentCircleMarker);
	            }

	            //if(newsObj.currentCircleMarker == false){
	            	newsObj.currentCircleMarker = Sig.showCircle(newsObj.myPosition, newsObj.currentRadius);
	            	console.log("newsObj.currentCircleMarker", newsObj.currentCircleMarker);
	            //}else{
	            //	newsObj.currentCircleMarker.setLatLng([newsObj.myPosition[1], newsObj.myPosition[0]]).setRadius(newsObj.currentRadius);
	            //}
	            newsObj.loadPageOnMap();
	            Sig.map.fitBounds(newsObj.currentCircleMarker.getBounds(), { 'maxZoom' : 13, 'animate':false });
	            
	        });

			var scopeType = "localised";
			$("#news_sharedBy_scopeType, .form-share-post #news_sharedBy_scopeType").val(scopeType);
			newsObj.initInputScopeGroup();
			console.log(scopeType);

			var lbl = $(".btn-select-scope-type[data-scope-type='localised']").data("scope-lbl");
			var icon = $(".btn-select-scope-type[data-scope-type='localised']").data("icon-lbl");
			$("#scope-lbl, .form-share-post #scope-lbl").html("<i class='fa fa-"+icon+"'></i> "+lbl+"");
		}else{
			toastr.error(t.t("This page is not localised"));
		}

    },

    showMapScopeNews : function(newsId, coordinates, radius, author){
    	if(coordinates != null && coordinates[0] != "false"){
    		newsObj.closeForm();
    		var canvas = "map-container-scope-"+newsId;


    		$("article .map-container-scope").html("").removeClass().addClass("map-container-scope d-none");
    			
			console.log("showMapScopeNews", coordinates, canvas);
			Sig.init(canvas, function(){
	        	$("#"+canvas).removeClass("d-none");
	        	//$(".container-scoping").removeClass("d-none").addClass("d-flex");

	        	var scrollWheelZoom = ($("body").width() < 767) ? false : true;
	            Sig.loadMap(canvas, {
	                center : new L.LatLng(coordinates[1], coordinates[0]),
	                zoom: 10,
	                maxZoom: 14,
                	scrollWheelZoom: scrollWheelZoom,
                	zoomControl : false
	            });

	            if(newsObj.scopeMarker != false)
	            	Sig.map.removeLayer(newsObj.scopeMarker);

	            newsObj.scopeMarker = Sig.showSingleMarker(coordinates, author.type);
	            var html = '<div style="width:300px;" class="d-flex">'+
								'<div class="no-padding">'+
									'<img src="/uploads/imgProfil/'+author.thumb+'" height="30">'+
								 '</div>'+
								 '<div class="col-10">'+
									'<div class="bold no-margin author-name">'+author.name+'</div>';

							if(author.address != "")
							html+=	'<div class="author-name"><small><i class="fa fa-map-marker"></i> '+author.address+'</small></div>';

							html+=	'<br><button class="btn btn-blue full-width open-quickview" '+
										 'data-type="page" data-id="'+author.slug+'" '+
										 'data-toggle="modal" data-target="#modal-quickview"'+'>'+
										t.t('Quick view')+
									'</button>'+
								'</div>'+
							'</div>';

				newsObj.scopeMarker.bindPopup(html);
				newsObj.scopeMarker.on("popupopen", function(){
					$(".open-quickview").click(function(){
						nexxo.openQuickView($(this).data("type"), $(this).data("id"));
					});
				});

	            if(newsObj.scopeCircleMarker != false){ console.log("removeLayer");
	                Sig.map.removeLayer(newsObj.scopeCircleMarker);
	            }

	            //if(newsObj.scopeCircleMarker == false){
	            	var km = parseFloat(radius).toFixed(1); radius = km*1000;
	            	newsObj.scopeCircleMarker = Sig.showCircle(coordinates, radius);
	            	console.log("newsObj.scopeCircleMarker", newsObj.scopeCircleMarker);
	            //}else{
	            //	newsObj.scopeCircleMarker.setLatLng([newsObj.myPosition[1], newsObj.myPosition[0]]).setRadius(newsObj.currentRadius);
	            //}
	            //newsObj.loadPageOnMap();
	            Sig.map.fitBounds(newsObj.scopeCircleMarker.getBounds(), { 'maxZoom' : 13, 'animate':false });
	            
	        });

		}else{
			toastr.error(t.t("This page is not localised"));
		}

    },

    loadPageOnMap : function(){
    	var geo = "lat="+newsObj.myPosition[1]+"&lon="+newsObj.myPosition[0]+"&radius="+newsObj.currentKm;

    	//var getParams = (geo+types != "") ? "?" + geo+types : "";

		var url = "/search/-/json?"+geo+"&types=user";
		console.log("url complet", url);
		
		$.ajax({
          type: "POST",
          url: url,
          // data: data,
          success: function(res){
          	//if(Sig.mapIsLoaded == true)
				//Sig.showDataOnMap(res.jsonRes, fitBounds=false);
			var s = res.jsonRes.length > 1 ? "s" : "";
			newsObj.myMarker.bindPopup( '<div class="text-center bold">'+
											res.jsonRes.length + 
											//" <i class='fa fa-user'></i>"+
											" personne"+s+" touchée"+s +
										'</div>').openPopup();

            console.log("global search ok", url);
          }
        });
    },

    closeForm : function(){
    	$("#news_text").removeClass('open');
    	$(".btn-check-sharable").addClass("d-none");
		$(".news-feed-form .btn-open-tags, .news-feed-form .btn-open-image").addClass("d-none");
		$(".news-feed-form #tag-list").addClass("d-none");
    	$(".extanded-form-news").addClass("d-none");
    },

    initFormUi : function(myPosition){ console.log("initFormUi");
    	newsObj.myPosition = myPosition;

    	$("#btn-send-post").click(function(){
    		newsObj.sendPost();
    	});
    	$("#btn-show-map").click(function(){
            newsObj.showMapScope();
        });
        //$("#check-use-aroundme").prop('checked', false);
        newsObj.closeMapScope();
        $("#check-use-aroundme").change(function(){
          console.log( $("#check-use-aroundme").is(':checked'));
          if($("#check-use-aroundme").is(':checked')){
            newsObj.showMapScope();
          }else{
          	//$("#map-container").addClass("d-none");
          	newsObj.closeMapScope();
          }
        });

        $("#news_check_sharable").prop('checked', false);
        $("#news_check_sharable").change(function(){
          console.log( $("#news_check_sharable").is(':checked'));
          if($("#news_check_sharable").is(':checked')){
            //newsObj.showMapScope();
          }else{
          	//$("#map-container").addClass("d-none");
          	//newsObj.closeMapScope();
          }
        });

        $("#news_text").focus(function(){
        	//if($(this).val() != "") {
        		$(".news-feed-form .btn-open-tags, .news-feed-form .btn-open-image").removeClass("d-none");
        		$(this).addClass('open');
        	//}
        	$(".extanded-form-news").removeClass("d-none");
        	/*$(this).addClass('open');
        	$(".btn-check-sharable").removeClass("d-none");*/
        });

        $("#news_text").focusout(function(){
        	if($(this).val() == "" && $(".news-feed-form #tag-list").hasClass("d-none")) {
        		setTimeout(function(){
        			if($(".news-feed-form #tag-list").hasClass("d-none")){
        				$("#news_text").removeClass('open');
				    	$(".btn-check-sharable").addClass("d-none");
						$(".news-feed-form .btn-open-tags, .news-feed-form .btn-open-image").addClass("d-none");
						$(".news-feed-form #tag-list").addClass("d-none");
        			}
	        	}, 300);
	        }
        });

        $("#news_text").keyup(function(){
        	var text = $(this).val();
        	newsObj.checkPreviewMediaShared(text);
        	$(this).addClass('open');
        	$(".btn-check-sharable").removeClass("d-none");
        	$(".news-feed-form .btn-open-tags, .news-feed-form .btn-open-image").removeClass("d-none");
        	$(".extanded-form-news").removeClass("d-none");
        });
        $("#news_text").change(function(){
        	var text = $(this).val();
        	newsObj.checkPreviewMediaShared(text);
        });
        $("#news_text").mouseup(function(){ 
        	setTimeout(function(){
        		console.log("mouseup", $("#news_text").val());
        		var text = $("#news_text").val();
        		newsObj.checkPreviewMediaShared(text);
        	}, 100);
        });

        $("#formControlRange").val(10);
        $("#formControlRange").change(function(){
			console.log('range change', $(this).val(), typeof $(this).val());
			var dist = parseInt($(this).val());
			
			//if(dist < 5) 
				dist = 2*dist;
			if(dist == 0) dist = 2;
			
			var km = parseFloat(dist).toFixed(1);
			newsObj.currentRadius = km*1000;
			newsObj.currentKm = km;
			$(".around-me-distance").html(km + " km");
			newsObj.showMapScope();
		});

		$("button#dropdown-scope-news").focusout(function(){ console.log("what !!!");
			setTimeout(function(){
				$("#dropdown-menu-scope-news").removeClass("show");
			}, 200);
		});
		$("button#dropdown-target-news").focusout(function(){ console.log("what2 !!!");
			setTimeout(function(){
				$("#dropdown-menu-target-news").removeClass("show");
			}, 200);
		});

		$(".btn-select-scope-type").click(function(){
			var scopeType = $(this).data("scope-type");
			newsObj.initInputScopeGroup();
			
			if(scopeType != "localised")
				newsObj.closeMapScope();
			else {
				$("#check-use-aroundme").prop('checked', true);
				newsObj.showMapScope();
			}

			$("#news_sharedBy_scopeType").val(scopeType);

			$("#scope-lbl").html("<i class='fa fa-"+ $(this).data("icon-lbl") +"'></i> " + $(this).data("scope-lbl"));
		});

		$(".btn-select-signed").click(function(){
			var dashboard = $(this).data("dashboard");
			var signedPageId = $(this).data("signed-page-id");
			var signedPageImgSrc = $(this).data("signed-page-imgprofil");

			//change image in form and in button select signed
			$("#dropdown-target-news").find("img").attr("src", '/uploads/imgProfil/'+signedPageImgSrc);
			$(".news-feed-form form .author-thumb img").attr("src", '/uploads/imgProfil/'+signedPageImgSrc);
			
			console.log("signedPageId", signedPageId, signedPageImgSrc);
			$("#modal-select-group-scope ul.dropdown-menu a").hide();
			$("#modal-select-group-scope ul.dropdown-menu a.group-in-page-"+signedPageId).show();

			//change url form to signe to the new page selected
			var currentUrl = $("form[name='news']").attr("action");
			var split = currentUrl.split("/");
			console.log("split", split);
			console.log("dashboard", dashboard);
			var newUrl = "/" + split[1] + "/" + split[2] + "/" + signedPageId + "/" + split[4];
			if(dashboard == true) newUrl = "/" + split[1] + "/" + split[2] + "/" + signedPageId + "/" + signedPageId;
			console.log("newUrl", newUrl);
			
			$("form[name='news']").attr("action", newUrl);

			if($(this).data("lat") != false && $(this).data("lng") != false)
			newsObj.myPosition = new Array($(this).data("lat"), $(this).data("lng"))

			//clear old group selected
			newsObj.initInputScopeGroup();
			newsObj.closeMapScope();
		});

		$("#btn-add-group-to-scope").click(function(){ console.log("onclick #btn-add-friend-to-group");
			var groupId = $(this).data("group-id");
			var membersId = $("#modal-select-group-scope .show-tick .filter-option").html();
			newsObj.initInputScopeGroup();
			
			if(membersId != "Nothing selected"){
				$("#news_scopeGroupsHidden").val(membersId);
				var groupStr = membersId.replace(/\s/g, '');
				var allGroups = groupStr.split(",");
				//console.log("membersId", membersId);
				$.each(allGroups, function(key, id){
					var name = $("#option-select-group-"+id).data("name");
					$("#display-scope-groups").append(
						"<span class='badge badge-secondary float-left padding-5 letter-white margin-right-5 margin-bottom-5'>"+
							"<i class='fa fa-check'></i> "+name+
						"</span> ");
				});
			}
			//console.log("value scopeGroups", $("#news_scopeGroupsHidden").val());
		});

		$(".btn-filter-stream").click(function(){
			var filter = $(this).data("filter");
			if(filter == "active") filter = $(".btn-filter-stream.active").data("filter");
			else {
				$(".btn-filter-stream").removeClass("active");
				$(this).addClass("active");
			}

			var idTarget = $(this).data("id-target");
			var isDashboard = $(this).data("is-dashboard");
    	
			newsObj.updateStream(filter, idTarget, isDashboard);
		});


		$(".news-feed-form .btn-open-tags").click(function(){
			if($(".news-feed-form #tag-list").hasClass("d-none"))
				$(".news-feed-form #tag-list").removeClass("d-none");
			else
				$(".news-feed-form #tag-list").addClass("d-none");
				
			$(".news-feed-form #tag-list .btn-select-tag").off().click(function(){
				var tag = $(this).data("tag-value");
				var text = $("#news_text").val();
				text += " " + tag;
				$("#news_text").val(text);
			});
		});

		$(".news-feed-form .btn-open-image").click(function(){
			$("#image-uploader").click();
		});

		$(".btn-filter-tag").click(function(){
			var tag = $(this).data("tag-value");
			console.log("seach tag", tag)
			var allTags = $("#search-tags").val();
			if($(this).hasClass("active")) {
				allTags = allTags.replace(tag+" ", "");
				allTags = allTags.replace(tag, "");
				$(this).removeClass("active");
			}
			else{
				allTags += tag + " ";
				$(this).addClass("active");
			}
			$("#search-tags").val(allTags);
		});
		$("#search-tags").val("");

		$("#search-tags").focusin(function(){
			$(".search-tools").removeClass("d-none");
		});
		$("#search-tags").focusout(function(){
			//$("#search-tools").addClass("d-none");
			setTimeout(function(){
				if($("#search-tags").val() == "")
				$(".search-tools").addClass("d-none");
			}, 300);
		});

		
		newsObj.initUploadImage();
		newsObj.initUpdateStream();
    },

    initUploadImage : function(){
    	$('.news-feed-form #image-uploader').on('change', function () { 
	    	if (this.files && this.files[0]) {

	    		if(this.files[0].size/1024/1024 > 2){
	    			toastr.error(t.t("Sorry, this image is too big ! (> 2MB) Thank you to reduce the size of your image or to choose a smaller image."));
	    			return;
	    		}
	    		//alert('This file size is: ' + this.files[0].size/1024/1024 + "MB");

			    var reader = new FileReader();

			    reader.onload = function(e) {
			      $('#preview-upload-image img').attr('src', e.target.result);
			      $("#news_image").val(e.target.result);
			      $('#preview-upload-image').removeClass("d-none");
			    }

			    reader.readAsDataURL(this.files[0]);
			}
    	});
    	//preview-upload-image
    /*	newsObj.uploadCrop = 
    	$('#preview-upload-image').croppie({
			viewport: {
				width: "100%",
				height: "100%",f
				type: 'square'
			},
			enableExif: true,
			showZoomer: false
		});

		$('.news-feed-form #image-uploader').on('change', function () { 
			$('#preview-upload-image').removeClass("d-none");
			newsObj.readFile(this); 
			//setTimeout(function(){ resize(); }, 400);
		});
		$("#news_image").val("");

		$('.upload-result').on('click', function (ev) {
			newsObj.uploadCrop.croppie('result', {
				type: 'canvas',
				size: 'original'
			}).then(function (resp) { console.log("resp", resp);
				newsObj.popupResult({
					src: resp
				});
			});
		});
		$(".cr-slider").change(function(){
			newsObj.uploadCrop.croppie('result', {
				type: 'canvas',
				size: 'original'
			}).then(function (resp) { console.log("resp", resp);
				newsObj.popupResult({
					src: resp
				});
			});
		});

		$(".cr-boundary").mouseup(function(){
			newsObj.uploadCrop.croppie('result', {
				type: 'canvas',
				size: 'original'
			}).then(function (resp) { console.log("resp", resp);
				newsObj.popupResult({
					src: resp
				});
			});
		});

		*/
    },

	popupResult : function (result) {
		$("#news_image").val(result.src);
	},

    readFile : function(input) {
		if (input.files && input.files[0]) { console.log('input.files', input);
            var reader = new FileReader();
            
            reader.onload = function (e) {
				$('.upload-demo').addClass('ready');
            	newsObj.uploadCrop.croppie('bind', {
            		url: e.target.result
            	}).then(function(){
            		//console.log('jQuery bind complete');
            	});
            	
            }
            
            reader.readAsDataURL(input.files[0]);
            //$("#btn-submit-form-img").removeClass("d-none");
        }
        else {
	        swal("Sorry - you're browser doesn't support the FileReader API");
	    }
	},

    updateStream : function(filter="all", idTarget, isDashboard, timestamp=null){
    	if(newsObj.loadingMore == true) return;

    	newsObj.loadingMore = true;

    	if(timestamp == null)
    		$("#newsfeed-items-grid").html("<h5 class='text-center padding-50'><i class='fa fa-refresh fa-spin'></i> "+
    											t.t("Loading ...")+
    										"</h5>");
    	
    	$(".search-tools").addClass("d-none");
    	//extract tag from search input
    	var tags = $("#search-tags").val();
    	tags = tags.match(/(^|\s)(#[a-zA-Z0-9áàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ_\d-]+)/ig);
    	$.each(tags, function(key, tag){ tags[key] = tag.replace(" ", ""); })
    	
    	var url = "/news/stream/"+filter+"/"+idTarget+"/"+isDashboard;
    	if(timestamp!=null) url+="/"+timestamp;

    	$.ajax({
	        type:'POST',
	        url:url,
	        data:{ tags: tags },
	        success:function(e){ 
	        	console.log("/news/stream/ success");

	        	newsObj.loadingMore = false;

	        	if(timestamp == null) $("#newsfeed-items-grid").html(e);
	        	else{
	        		//nexxo.scrollTo("#load-more-button");
	    			$("#load-more-button").remove();
	        		$("#newsfeed-items-grid").append(e);
	        	}

	        	newsObj.initSharePost();
		        newsObj.initLikePost();
		        newsObj.initBtnReloadStream();
				newsObj.initSelectTag();
		        newsObj.initShowScopePost();
		        newsObj.scalePostSize();
		        comment.initUi();
		        comment.initFormAddComment();
		        notificationObj.initNotifUI();
			}, 
	        error: function(e){
	        	console.log("/news/stream/ error", e);
				
			}
	    });
    },

    initSelectTag : function(){
    	$(".letter-tag").click(function(){
    		$("#search-tags").val($(this).html());
    		$("#btn-search-by-tag").click();
    	});
    },

    initUpdateStream : function(){
    	console.log("init initUpdateStream");
    	$(window).off().scroll( function(e){ 
            //console.log("on scroll", $(this).scrollTop(), $("#newsfeed-items-grid").height());
            if($(this).scrollTop() >= $("#newsfeed-items-grid").height()-500){
              	if(newsObj.loadingMore == false && $("#load-more-button").length){
              		console.log("on scroll RELOAD", $(this).scrollTop(), $("#newsfeed-items-grid").height());
	              	var filter = $(".btn-filter-stream.active").data("filter");
					var idTarget = $("#load-more-button").data("id-target");
					var isDashboard = $("#load-more-button").data("is-dashboard");
					var timestamp = $("#load-more-button").data("timestamp");
    		
    				$("#load-more-button").html('<i class="fa fa-spin fa-refresh"></i>');//return;
    				newsObj.updateStream(filter, idTarget, isDashboard, timestamp);

    			}
            }
          }); 
    },

    initBtnReloadStream : function(){
    	console.log("init initBtnReloadStream");
    	$(".btn-filter-stream").off().click(function(){
    		console.log(".btn-filter-stream click");
			var filter = $(this).data("filter");
			if(filter == "active") filter = $(".btn-filter-stream.active").data("filter");
			else {
				$(".btn-filter-stream").removeClass("active");
				$(this).addClass("active");
			}

			var idTarget = $(this).data("id-target");
			var isDashboard = $(this).data("is-dashboard");
    	
			newsObj.updateStream(filter, idTarget, isDashboard);
		});

		$(".btn-filter-stream").tooltip();
    },

    checkPreviewMediaShared : function(text){

    	//extract all url from the string text
    	var urls = text.match(/\bhttps?:\/\/\S+/gi);
    	//initialize the number of response received to 0
    	var nbResponse = 0;
    	//initialize the first timeout duration before to send the first request for the first url
    	var timeout = 20;

    	//if there is at least one url in the text
    	//and we are not already waiting for a response
		if(urls != null && newsObj.mediaLoading == false){				
			$.each(urls, function(key, urlShared){
				//if this url is not allready traited
				if($.inArray(urlShared, newsObj.mediasLoaded)==-1){ 
					//show loader message
					$("#media-shared-loader").html(
						"<div class='padding-10 text-center full-width'>"+
						 	"<i class='fa fa-refresh fa-spin'></i> "+t.t('loading preview')+" (" + (nbResponse+1) + "/"+ urls.length +")" +
					    "</div>").removeClass("d-none");
					
					//if url is an image > show image preview
					var isImage = urlShared.match(/(https?:\/\/.*\.(?:png|jpg|gif|jpeg))/i);
					if(isImage != null){
						var metadata = { url: urlShared, title:"", description:"", image: urlShared, preview: true }
						newsObj.mediasLoaded.push(urlShared);	 //memorise current url in a list of urls
						newsObj.mediasMetadata.push(metadata);//memorise all metadata
						newsObj.appendMediaPreview(metadata); //show preview html in form
						$("#media-shared-loader").html("");
									
					}else{
						//load each link one after one
						//csrf bug when we load all link in same time (i don't know why) 
						var interval = 
						setInterval(function(){ console.log("interval", nbResponse, urlShared, timeout);
							//if the precedent response has been receveid (not waitng for response)
							//and not all url has been traited
							//and the next url has not been allready loaded
							if(newsObj.mediaLoading == false && nbResponse < urls.length && $.inArray(urlShared, newsObj.mediasLoaded)==-1){
								//clearInterval : we are going to send the request, so we dont wait
								clearInterval(interval);
								//set flat mediaLoading to true, 
								//for other Interval timers be able to know if they can go
								newsObj.mediaLoading = true;
								//console.log("/app/get-info-url", urlShared, "timeout", timeout);
								$.ajax({
							        type:'GET',
							        url:"/app/get-info-url", //in AppController.php
							        data:{ urlShared: urlShared },
							        success:function(e){ 
							        	//response received : we are not loading yet
							        	newsObj.mediaLoading = false;
							        	//increment the number of response, to know if we loaded all urls
							        	nbResponse++;
							        	//if all url have been traited
										if(nbResponse == urls.length){
											//clear loader message
											$("#media-shared-loader").html("");
								        }else{
								        	//update loader message
								        	$("#media-shared-loader").html(
												"<div class='padding-10 text-center full-width'>"+
												 	"<i class='fa fa-refresh fa-spin'></i> "+ t.t("loading preview")+" (" + (nbResponse+1) + "/"+ urls.length +")" +
											    "</div>").removeClass("d-none");
								        }

							        	newsObj.mediasLoaded.push(urlShared);	 //memorise current url in a list of urls
							        	newsObj.mediasMetadata.push(e.metadata);//memorise all metadata
							        	newsObj.appendMediaPreview(e.metadata); //show preview html in form
									}, 
							        error: function(e){
										console.log("error !", nbResponse, urls.length);
										//response received : we are not loading yet
							        	newsObj.mediaLoading = false;
										//increment the number of response, to know if we loaded all urls
							        	nbResponse++;
							        	//if all url have been traited
										if(nbResponse == urls.length){
								        	//clear loader message
											$("#media-shared-loader").html("");
								        }else{
								        	//update loader message
								        	$("#media-shared-loader").html(
											"<div class='padding-10 text-center full-width'>"+
											 	"<i class='fa fa-refresh fa-spin'></i> loading preview (" + (nbResponse+1) + "/"+ urls.length +")" +
										    "</div>").removeClass("d-none");
								        }
								        //set default preview for link unreachables
								        var metadata = {
								        	url: urlShared,
								        	//if the preview is not found, it is not necessary to save it in db
								        	//so set preview to false, this url will be ignored before save
								        	preview: false,
											image:"/img/broken-link.png",
											title: "Impossible to find a preview",
											description: "Preview not found : "+
															"<small>"+
																"<a href='"+urlShared+"'>"+urlShared+"</a>"+
															"</small>",
										}
										newsObj.mediasLoaded.push(urlShared);	//memorise current url in a list of urls
							        	newsObj.mediasMetadata.push(metadata); //memorise all metadata
							        	newsObj.appendMediaPreview(metadata);  //show preview html in form
										//console.log("finish ?", urls.length, e.key);
									}
							    });
							}else{
								if(nbResponse == urls.length) 
									clearInterval(interval);
							}
						}, timeout);
						timeout = 1000;
					}
					
				}else{
					nbResponse++;
				}
			});
		}
    },

    appendMediaPreview : function(metadata){
    	var html =
    	'<div class="row padding-15 no-margin block-media-preview" data-url="'+metadata.url+'">' +
			'<div class="col col-3 col-sm-2 col-lg-3 no-padding" style="max-height:100px; overflow:hidden;">' +
				'<img src="'+metadata.image+'" class="full-width">' +
			'</div>' +
			'<div class="col col-8 col-sm-9 col-lg-8">' +
				'<h5 style="font-size:1em;"><a href="'+metadata.url+'" target="_blank">'+metadata.title+'</a></h5>' +
				'<p class="d-none d-sm-inline" style="font-size:1em;">'+metadata.description+'</p>' +
			'</div>' +
			'<div class="col col-1 no-padding text-right">' +
				'<a href="javascript:" class="btn-remove-preview" data-url="'+metadata.url+'"'+
					' data-toggle="tooltips" data-original-title="remove preview">'+
					'<i class="fa fa-times"></i>'+
				'</a>' +
			'</div>' +
		'</div>';
		//add html to the view
		$("#media-shared-container").append(html).removeClass("d-none");
		//bind button to remove previews
		$(".btn-remove-preview").off().click(function(){
			var url = $(this).data("url");
			$(".block-media-preview[data-url='"+url+"']").remove();
			$.each(newsObj.mediasMetadata, function(key, meta){
				console.log("meta.url == url", meta.url, url);
				if(meta.url == url) newsObj.mediasMetadata[key].preview = false;
			});
			
		});
    }
};