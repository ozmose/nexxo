// Mini API to send a message with the socket and append a message in a UL element.
var adminObj = {

	//eventsList : new Array(),

	initUi : function(){
		console.log("adminObj.initUi");

		reportObj.initUi();
		
		$("#btn-load-media").click(function(){
			mediathequeObj.loadMedia();
		});
		
		$("#btn-check-moderation").click(function(){
			adminObj.checkModeration();
		});

		$("#btn-clean-inactive-pages").click(function(){
			adminObj.cleanInactivePage();
		});

		comment.initEditComment();
	},

	checkModeration : function(){
		$("#process-status").html("<i class='fa fa-spin fa-refresh'></i> Mise à jour de la modération, merci de patienter...")
							.removeClass("d-none");
		$.ajax({
	        type: "POST",
	        url: "/close-reports",
	        success: function(res){
	          console.log("success /check-moderation", res);
	          //if(typeof res.error == "undefined"){
	          	//$("#media-list").html(res);
	          	//comment.initFormAddComment();
				toastr.success("Mise à jour de la modération terminé. " + res.msg);
	            $("#process-status").html("<span class='letter-green'>"+
	            							"<i class='fa fa-check'></i> "+
	            							res.msg+
	            							"<br>" + res.countUp + " éléments conservés " +
	            							"<br>" + res.countDown + " éléments supprimés " +
	            						   "</span>");
	          //}else{
	          //    toastr.error(res.errorMsg);
	          //}
	        },
	        //dataType: dataType
	    });
	},

	cleanInactivePage : function(){
		$("#process-status").html("<i class='fa fa-spin fa-refresh'></i> Nettoyage des pages inactives depuis + de 7 jours, merci de patienter...")
							.removeClass("d-none");
		$.ajax({
	        type: "POST",
	        url: "/clean-inactive-pages",
	        success: function(res){
	          console.log("success /clean-inactive-pages");
	          //if(typeof res.error == "undefined"){
	          	//$("#media-list").html(res);
	          	//comment.initFormAddComment();
				toastr.success("Nettoyage terminé. " + res.msg);
	            $("#process-status").html("<span class='letter-green'><i class='fa fa-check'></i> "+res.msg+"</span>");
	          //}else{
	          //    toastr.error(res.errorMsg);
	          //}
	        },
	        //dataType: dataType
	    });
	}

};