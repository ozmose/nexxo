// Mini API to send a message with the socket and append a message in a UL element.
var agendaObj = {

	eventsList : new Array(),

	initUi : function(){

		searchObj.renderPartial = "json";
		searchObj.typesSelected = "event";

		$(".btn-create-event").click(function(){
    		agendaObj.openForm(true);
    	});

		$(".btn-show-calendar").click(function(){
    		setTimeout(function(){ searchObj.globalSearch(); }, 500);
    	});

    	searchObj.initListResUi();
    	
    	$('#calendar').fullCalendar({
			header: {
		      left: 'prev,next today',
		      center: 'title',
		      right: 'month,agendaWeek,agendaDay'
		    },
		  	viewRender: function() {
			  	console.log('viewRender');
			  	var startDate = $('#calendar').fullCalendar("getView").start.format("YYYY-MM-DD");
			  	var endDate = $('#calendar').fullCalendar("getView").end.format("YYYY-MM-DD");
			  	var search = "-";

			  	searchObj.globalSearch();
			  	//agendaObj.loadData(search, startDate, endDate);
				
			},
		  	dayClick: function(date, jsEvent, view) {
			  	console.log('clicked on ' + date.format());
				agendaObj.displayAllDayEvent(date);
	      		setTimeout(function(){ 
					nexxo.scrollTo(".today-events-thumb");
			    }, 500);
			},
			eventClick: function(calEvent, jsEvent, view) {
			    console.log('Event.id: ',calEvent, jsEvent, view);
			    agendaObj.displayDayEvent(calEvent);
			    setTimeout(function(){ 
					nexxo.scrollTo(".today-events-thumb");
			    }, 500);
			    // change the border color just for fun
			    //$(this).css('border-color', 'red');

			},
			locale: 'fr',
		
		});

    	
	},

	openForm : function(open){
		if(open){ console.log("hide calendar");
		    $("#form-event").removeClass("d-none");
		    $("#calendar").addClass("d-none");
		}else{ console.log("show calendar");
		    $("#form-event").addClass("d-none");
		    $("#calendar").removeClass("d-none");
		}
	},

	showData : function(datas){
	    console.log("agendaObj.showData()", datas);
		agendaObj.eventsList = new Array();
		$('#calendar').fullCalendar("removeEvents");
		var objEvent = null;
		$.each(datas, function(key, event){
	  		objEvent = {
	  			"id" : event["id"],
	  			"slug" : event["slug"],
	  			"title" : "· " + event["name"],
	  			"parent" : event["parent"],
	  			"description" : event["description"]==null?"":event["description"].substr(0,200),
	  			"city" : event["city"]==null?"":event["city"],
	  			"start" : event["startDate"].date,
	  			"end" : event["endDate"] != null ? event["endDate"].date : "",
	  			"imageBanner" : event["imageBanner"],
	  		};

	  		var date = event["startDate"].date;
	  		date = moment(new Date(date));
	  		date = date.format("YYYY-MM-DD");
	  		if(typeof agendaObj.eventsList[date] == "undefined") agendaObj.eventsList[date] = new Array();
	  		agendaObj.eventsList[date].push(objEvent);
	  		$('#calendar').fullCalendar("renderEvent", objEvent);
	  	});

	  	if(objEvent != null)
	  		agendaObj.displayDayEvent(objEvent);
	  	
		agendaObj.displayAllDayEvent(moment());
	},

	displayDayEvent : function(event){
	    $(".today-events.calendar").html("<h5 class='padding-15'><i class='fa fa-spin fa-refresh'></i> Chargement en cours</h5>");
		$.ajax({
	      type: "POST",
	      url: "/get-event-view/"+event.id,
	      success: function(eventView){
	      	console.log("eventView", eventView);
	      	$(".today-events.calendar").html(eventView);

	      	searchObj.initBtnOpenMarker(function(){
				setTimeout(function(){ 
					nexxo.scrollTo("#map-container");
				}, 500);
			});

	        $(".open-quickview").click(function(){
				nexxo.openQuickView($(this).data("type"), $(this).data("id"));
			});
	      }
	    });
	},


	displayAllDayEvent : function(date){ //date form = YYYY-MM-DD
	
		if(typeof agendaObj.eventsList[date.format("YYYY-MM-DD")] != "undefined"){
			var ids = new Array();
			$.each(agendaObj.eventsList[date.format("YYYY-MM-DD")], function(key, event){
				ids.push(event.id);
			});
			var searchTxt = $("#search-in-page").val() != "" ? $("#search-in-page").val() : "-";
			$(".today-events.calendar").html("<h5 class='padding-15'><i class='fa fa-spin fa-refresh'></i> Chargement en cours</h5>");
			$.ajax({
		      type: "POST",
		      url: "/get-events-view/",
		      data:{ "ids" : ids },
		      success: function(eventView){
		      	console.log("eventView", eventView);
		      	$(".today-events.calendar").html(eventView);

		      	searchObj.initBtnOpenMarker(function(){
					setTimeout(function(){ 
						nexxo.scrollTo("#map-container");
					}, 500);
				});

		        $(".open-quickview").click(function(){
					nexxo.openQuickView($(this).data("type"), $(this).data("id"));
				});
		      }
		    });
		}else{
			$("#accordion-1.day-event").html(
				"<div class='card'>"+
					'<div class="card-header" role="tab" id="headingOne-1">'+
						'<h5 class="mb-0 title">'+
							"Aucun événement prévu"+
						'</h5>'+
					'</div>'+
				'</div>');
		}
	},


	getHtmlEvent : function(event, date, endDate){
		//no longer used : template twig instead /agenda/event-view
		html =
		"<div class='card'>"+
			'<div class="card-header" role="tab" id="headingOne-1">'+
				'<div class="event-time">'+
					'<time datetime="2004-07-24T18:18"><b> Commence à : '+date.format("HH:mm")+'</b></time><br>';
		if(endDate != "")
		html +=
					'<time datetime="2004-07-24T18:18">Se termine le : '+endDate.format("DD-MM-YYYY HH:mm")+'</time>';
		html +=
						'</div>'+
				'<h5 class="mb-0 title">'+
					'<a href="/page/'+event.slug+'" target="_blank">'+
						'<i class="fa fa-angle-down" aria-hidden="true"></i> '+	
						event.title+			
					'</a>'+
				'</h5>';

		if(event.city != "")
		html +=
				'<div class="place inline-items">'+
					'<span><a href="javascript:" '+
							 'class="btn-open-marker padding-5 padding-left-10 padding-right-10 border-radius-5 margin-top-5" '+
							 'data-id="'+event.id+'">'+
						'<i class="fa fa-map-marker no-margin"></i> '+event.city+
					'</a></span>'+
				'</div>';
		html +=
				'<div class="card-body no-padding margin-top-10">'+
					event.description+
					'<br>'+
					'<button class="btn bg-breez open-quickview margin-top-10" '+
							'data-toggle="modal" data-target="#modal-quickview" ' +
							'data-type="page" data-id="'+event.slug+'">' + t.t('Quickview') + '</button>'+
				'</div>'+
			'</div>'+
		"</div>";
		return html;
	}


}