// Mini API to send a message with the socket and append a message in a UL element.
var Chat = {

    history: new Array(),
    clientInformation: {},
    conn: false,
    mousePosition: "OUT",
    timerChat: false,
    memSendJson: "",
    timerReconnect: false,
    timerCheckCon: false,

    upTitlePage : function(){
      if(Chat.mousePosition == "OUT"){
        var count = parseInt($('head title').data("count"))+1; 
        $('head title').data("count", count);
        $('head title').text("("+count+") " + $('head title').data("title")); 
      }
    },

    initChat : function(){
      if(typeof APP_USERNAME == "undefined") return;
      
      console.log("initChat");

      // This object will be sent everytime you submit a message in the sendMessage function.
      Chat.clientInformation = {
          senderUsername: APP_USERNAME,
          senderSlug: APP_USER_SLUG,
          senderToken: APP_USER_CHATTOKEN,
          receiverType: "",
          receiverSlug: "",
          senderThumb: APP_USER_THUMB,
          status: APP_USER_CHATSTATUS
          // You can add more information in a static object
      };

      //TO BE ABLE TO SHOW NOTIF IN TAB TITLE
      $(window).mouseenter(function(){
        //user mouse is in current window
        Chat.mousePosition = "IN";
        $('head title').data("count", 0);
        $('head title').text($('head title').data("title"));
      });
      $(window).mouseout(function(){
        //user mouse is out current window (other tab or other software)
        Chat.mousePosition = "OUT";
      });
      
      if(SOCKET_ENABLED == "true"){
        // START SOCKET CONFIG
        /**
         * Note that you need to change the "sandbox" for the URL of your project. 
         * According to the configuration in Sockets/Chat.php , change the port if you need to.
         * @type WebSocket
         */
        Chat.conn = new WebSocket(SOCKET_URL);

        Chat.conn.onopen = function(e) {
          Chat.connectUser();
          //Chat.connectMyGroups();
          console.info("Connection established succesfully");
          Chat.showChatServerStatus(true);

          if(Chat.memSendJson != ""){
            setTimeout(function(){ Chat.sendJson(Chat.memSendJson); }, 6000);
          }
        };

        Chat.conn.onmessage = function(e) {
            var data = JSON.parse(e.data);
            console.log("Chat.conn.onmessage", data);
            
            /*******************************
                ACTION 1 : connectUser
            ********************************/
            if(data.action == "connectUser"){
              if(typeof data.newToken != "undefined") 
                Chat.clientInformation.senderToken = data.newToken;
                Chat.memSendJson.senderToken = data.newToken;
                APP_USER_CHATTOKEN = data.newToken;
            }
            /*******************************
                ACTION 2 : sendMessage
            ********************************/
            else if(data.action == "sendMessage"){
              data.toread = true;
              console.log("exist conv ?", data, $("ul.chat-users .js-chat-open[data-slug='"+data.senderSlug+"']").length);
              if($("ul.chat-users .js-chat-open[data-slug='"+data.senderSlug+"']").length <= 0){
                Chat.initNewSender(data);
              }
              var read = Chat.appendMessage(data);
              if(read == true) Chat.setReadHistory(data.senderSlug);

              if(Chat.clientInformation.senderSlug != data.senderSlug){
                Chat.upTitlePage();
              }

            }
            /*******************************
                ACTION 3 : status (chat contact)
            ********************************/
            else if(data.action == "status"){ //console.log("update status", data);
              $.each(data.status, function(slug, status){
                //console.log("update status", slug, status);
                $("li.js-chat-open[data-slug='"+slug+"'] .icon-status").removeClass("online offline away");
                $("li.js-chat-open[data-slug='"+slug+"'] .icon-status").data("status", status.status).addClass(status.status);
                if(status.msgToRead > 0){ //console.log("update status MSG UNREAD !", "li.js-chat-open[data-slug='"+status.slug+"'] .count-toread-msg");
                  $("li.js-chat-open[data-slug='"+slug+"'] .count-toread-msg")
                  .data("count", status.msgToRead).html(status.msgToRead)
                  .removeClass("d-none");                  
                }else{
                  $("li.js-chat-open[data-slug='"+slug+"'] .count-toread-msg")
                  .data("count", status.msgToRead).html(status.msgToRead)
                  .addClass("d-none");
                }
              });
            }
            /*******************************
                ACTION 4 : friendChangeStatus
            ********************************/
            else if(data.action == "friendChangeStatus"){ console.log("friendChangeStatus");
                //console.log("update status", data.slug, data.status);
                $("li.js-chat-open[data-slug='"+data.slug+"'] .icon-status").removeClass("online offline away");
                $("li.js-chat-open[data-slug='"+data.slug+"'] .icon-status").data("status", data.status).addClass(data.status);

                if(data.status == "online")
                  notificationObj.prependChatContact(data.slug, data.count);

                //si le message correspond à la conversation en cours, ouverte dans la fenêtre de chat,
                //on affiche le message tout de suite
                if( ($("textarea.chat-msg").data('slug-receiver') == data.slug ||
                     $("textarea.chat-msg").data('slug-receiver') == data.slug) &&
                     $('.popup-chat-responsive').hasClass('open-chat') == true){
                     $(".popup-chat-responsive.open-chat .icon-status").removeClass("online offline away");
                     $(".popup-chat-responsive.open-chat .icon-status").data("status", data.status).addClass(data.status);
                }
                
            }
            /*******************************
                ACTION 5 : receiveNotification
            ********************************/
            else if(data.action == "receiveNotification"){ console.log("receiveNotification");
                //console.log("receiveNotification notif:", APP_USER_SLUG, "!=", data.notif.lastAuthor.slug);
                //console.log("receiveNotification notif:", data);
               
                var notifListClass = ".activity-feed";
                if(data.notif.verb == "REQUEST_FRIEND" || data.notif.verb == "REQUEST_ADMIN"){
                  notifListClass = ".friend-requests.menu-top";
                }

                //supprime la notification ayant le meme type/id (si elle existe)
                $("ul.notification-list"+notifListClass+
                  " .open-notification[data-about-id='"+data.notif.aboutId+"']"+
                                    "[data-about-type='"+data.notif.aboutType+"']")
                                     .remove();

                //supprime le message "aucune notif"
                $("ul.notification-list"+notifListClass+' li.no-notif').remove();
                //ajoute la nouvelle notif en haut de la liste
                $("ul.notification-list"+notifListClass).prepend(data.html);
                //disable links in notification (img illustration media)
                $("ul.notification-list"+notifListClass+" .open-notification a")
                  .attr("href", "javascript:").attr("target", "");

                var countNotif = $("#site-header ul.notification-list"+notifListClass+" li.unread").length;
                //console.log("countNotif", "#site-header ul.notification-list"+notifListClass+" li.unread", countNotif);
                $(".label-avatar"+notifListClass).html(countNotif).removeClass("d-none");
               

                Chat.upTitlePage();

                if(data.notif.verb != "COMMENT" && data.notif.verb != "LIKE" && data.notif.verb != "DISLIKE"){
                  nexxo.playSound("notification");
                }

                if(data.notif.verb != "LIKE" && data.notif.verb != "DISLIKE"){
                  var screenSM = ($("body").width() < 767) ? false : true;
                  if(screenSM == false || data.notif.verb != "COMMENT")
                    toastr.info("<ul class='notification-list'>"+data.html+"</ul>");
                }


                notificationObj.initNotifUI();

                //dans le cas d'un commentaire, on ajoute le html du commentaire dans la liste des com (si elle existe dans le dom)
                if(data.notif.verb == "COMMENT" && data.notif.whatHtml != ""){
                  if(data.whatAnswerId == null){ //si ce n'est pas une réponse sur un autre commentaire   
                    $(".comment-block[data-parent-id='"+data.notif.aboutId+"']"+
                                    "[data-parent-type='"+data.notif.aboutType+"'] ul.comments-list.root")
                                    .prepend(data.whatHtml);
                  }else{
                    $(".comment-block[data-parent-id='"+data.notif.aboutId+"']"+
                                    "[data-parent-type='"+data.notif.aboutType+"'] .container-answers[data-comment-id='"+data.whatAnswerId+"'] ul.comments-list")
                                      .append(data.whatHtml);
                    $(".comment-block[data-parent-id='"+data.notif.aboutId+"']"+
                                    "[data-parent-type='"+data.notif.aboutType+"'] .container-answers[data-comment-id='"+data.whatAnswerId+"']").removeClass("d-none");
                    
                  }
                  comment.initFormAddComment();
                }

                if(data.notif.verb == "LIKE"){
                  if(data.notif.whatType=="comment"){
                    $("a[data-like-type='likes'][data-comment-id='"+data.notif.whatId+"']"+
                      "[data-parent-id='"+data.notif.aboutId+"'][data-parent-type='"+data.notif.aboutType+"'] span b")
                       .html(data.notif.newTotal["likes"]);
                    $("a[data-like-type='dislikes'][data-comment-id='"+data.notif.whatId+"']"+
                      "[data-parent-id='"+data.notif.aboutId+"'][data-parent-type='"+data.notif.aboutType+"'] span b")
                       .html(data.notif.newTotal["dislikes"]);
                  }else{
                    if(data.notif.aboutType == "news"){
                      //console.log("LIKE NEWS ?", data.notif.aboutId);
                      $("a.btn-post-like[data-like-type='likes'][data-post-id='"+data.notif.aboutId+"'] span b")
                         .html(data.notif.newTotal["likes"]);
                      $("a.btn-post-like[data-like-type='dislikes'][data-post-id='"+data.notif.aboutId+"'] span b")
                         .html(data.notif.newTotal["dislikes"]);
                    }
                    else if(data.notif.aboutType == "page"){
                      //console.log("LIKE PAGE ?", data.notif.aboutId);
                      $("a.btn-send-request-like[data-id-page='"+data.notif.aboutId+"'] .count-item-like")
                         .html(data.notif.newTotal);
                    }
                  }
                }
                if(data.notif.verb == "DISLIKE"){
                  if(data.notif.whatType=="comment"){
                    $("a[data-like-type='likes'][data-comment-id='"+data.notif.whatId+"']"+
                      "[data-parent-id='"+data.notif.aboutId+"'][data-parent-type='"+data.notif.aboutType+"'] span b")
                       .html(data.notif.newTotal["likes"]);
                    $("a[data-like-type='dislikes'][data-comment-id='"+data.notif.whatId+"']"+
                      "[data-parent-id='"+data.notif.aboutId+"'][data-parent-type='"+data.notif.aboutType+"'] span b")
                       .html(data.notif.newTotal["dislikes"]);
                  }else{
                      $("a.btn-post-like[data-like-type='likes'][data-post-id='"+data.notif.aboutId+"'] span b")
                         .html(data.notif.newTotal["likes"]);
                      $("a.btn-post-like[data-like-type='dislikes'][data-post-id='"+data.notif.aboutId+"'] span b")
                         .html(data.notif.newTotal["dislikes"]);
                  }
                }



            }
            /*******************************
                ACTION 6 : getCountUserOnline
            ********************************/
            else if(data.action == "getCountUserOnline"){ console.log("getCountUserOnline");
              $(".count-user-online").html(data.countUserOnline);
            }
            
        };
        
        Chat.conn.onerror = function(e){
            Chat.showChatServerStatus(false);
            console.log("Error: something went wrong with the socket.");
            console.error(e);
        };

        //reset timer
        if(Chat.timerCheckCon != false) clearInterval(Chat.timerCheckCon);
        //init new timer
        Chat.timerCheckCon = setInterval(function(){ 
          var d = new Date();
          console.log("Check socket connection. readyState :", Chat.conn.readyState, d.getHours()+":"+d.getMinutes()+":"+d.getSeconds());
          Chat.clientInformation.action = "keepAlive";
          Chat.sendJson(Chat.clientInformation);
        }, 30000);

        // END SOCKET CONFIG
      } // end if(SOCKET_ENABLED == "true")


      $(".form-chat textarea.chat-msg").keydown(function(e){ 
          var code = e.which; // recommended to use e.which, it's normalized across browsers
          //console.log(code);
          if(code==13){
              e.preventDefault();
              var msg = $(".form-chat textarea.chat-msg").val();

              //Chat.clientInformation.senderThumb = $(".form-chat textarea.chat-msg").data("thumb-sender");
              Chat.clientInformation.receiverType = $(".form-chat textarea.chat-msg").data("type-receiver");
              Chat.clientInformation.receiverSlug = $(".form-chat textarea.chat-msg").data("slug-receiver");
              
              console.log("change values", Chat.clientInformation);
              if(msg != ""){
                  Chat.sendMessage(msg);
                  // Empty text area
                  $(".form-chat textarea.chat-msg").val("");
                  console.log("before new sender", Chat.clientInformation);
                  if($("ul.chat-users .js-chat-open[data-slug='"+Chat.clientInformation.receiverSlug+"']").length <= 0){
                    Chat.initNewSender({ senderSlug : Chat.clientInformation.receiverSlug,
                                         senderUsername : Chat.clientInformation.receiverUsername,
                                         senderThumb : $(".popup-chat .title img").data("thumb"),
                                         status: "online",
                                        });
                  }
              }
              
             
          }
      });

      Chat.initBtnStartChat();
      
      //$(".count-user-online").html("<i class='fa fa-refresh fa-spin'></i>");  
      //setTimeout(function(){
      //  Chat.getCountUserOnline();
      //}, 3000);

      $("#search-friend-chat").keyup(function(){
        var val = $(this).val();
        console.log("search for", val);
        $(".sidebar--large .chat-users li").each(function(index){
          var name = $(this).data("name");
          console.log("match for name", name, val, name.match("/"+val+"/i"));
          var regex = new RegExp(val, 'i');
          if(val != "" && name.match(regex) == null){
            $(this).addClass("d-none");
          }else{
            $(this).removeClass("d-none");
          }
        });
      });
      //alert("change status binded");
    },

    
    restartSocketServer : function(){
      $.ajax({
          type: "POST",
          url: "/restart-socket",
          success: function(messages){
            console.log("success response from : /restart-socket");
            
          },
          error: function(messages){
            console.log("error response from : /restart-socket");
            
          }
        });
    },

    changeChatStatus : function(status){
      $.ajax({
          type: "POST",
          url: "/chat/change-my-chat-status/"+status,
          success: function(messages){
            $("li.js-chat-open[data-slug='"+APP_USER_SLUG+"'] .icon-status").removeClass("online offline away");
            $("li.js-chat-open[data-slug='"+APP_USER_SLUG+"'] .icon-status").data("status", status.status).addClass(status.status);
            console.log("success response from : /chat/change-my-chat-status/"+status, messages);
            
          },
          error: function(messages){
            console.log("error response from : /chat/change-my-chat-status/"+status, messages);
            
          }
        });
    },

    getCountUserOnline : function(){
      console.log("getCountUserOnline");
      if(SOCKET_ENABLED == "true"){
        Chat.sendJson({ "action" : "getCountUserOnline",
                                        "senderSlug" : Chat.clientInformation.senderSlug });
      }
    },

    initNewSender: function(data){
      console.log("SLUGS", data.senderSlug, APP_USER_SLUG);
      if(data.senderSlug == APP_USER_SLUG) return;

      console.log("initNewSender", data);
      var li = '<li class="inline-items js-chat-open" data-slug="'+data.senderSlug+'" data-name="'+data.senderUsername+'" '+
                   'data-type="user" '+ 
                   'data-thumb="'+data.senderThumb+'">'+
                    '<div class="author-thumb margin-right-15">'+
                      '<img src="/uploads/imgProfil/' + data.senderThumb + '" alt="image" height="42">'+
                      '<span class="icon-status '+data.status+'" data-status="'+data.status+'"></span>'+
                      '<span class="count-toread-msg badge d-none" data-count="0">0</span>'+
                    '</div>'+
                '</li>';
      $("ul.chat-users").prepend(li);
      Chat.initBtnStartChat();

    },

    initNewReceiver: function(data){
      console.log("SLUGS", data.receiverSlug, APP_USER_SLUG);
      if(data.receiverSlug == APP_USER_SLUG) return;

      console.log("initNewReceiver", data);
      var li = '<li class="inline-items js-chat-open" data-slug="'+data.receiverSlug+'" data-name="'+data.receiverUsername+'" '+
                   'data-type="user" '+ 
                   'data-thumb="'+data.receiverThumb+'">'+
                    '<div class="author-thumb margin-right-15">'+
                      '<img src="/uploads/imgProfil/' + data.receiverThumb + '" alt="image" height="42">'+
                      '<span class="icon-status '+data.status+'" data-status="'+data.status+'"></span>'+
                      '<span class="count-toread-msg badge d-none" data-count="0">0</span>'+
                    '</div>'+
                '</li>';
      $("ul.chat-users").prepend(li);
      Chat.initBtnStartChat();
      Chat.addChatContact(data.receiverSlug);
    },

    addChatContact : function(slug){
      $.ajax({
          type: "POST",
          url: "/relation/add-chat-contact/"+slug,
          success: function(messages){
            console.log("success response from : /relation/add-chat-contact/"+slug, messages);
            
          },
          error: function(messages){
            console.log("error response from : /relation/add-chat-contact/"+slug, messages);
            
          }
        });
    },
    

    initBtnStartChat: function(){

      $(".js-chat-open").off().on('click', function () {

          console.log("window height", $(window).height());
          var chatHeight = $(window).height() - 70;
          $(".popup-chat").css("maxHeight", chatHeight);
          $(".popup-chat .mCustomScrollbar").css("maxHeight", chatHeight-160).css("minHeight", chatHeight-160);

          //init data in form chat
          //to send message to the good channel
          $(".form-chat textarea.chat-msg").attr("data-slug-receiver", $(this).data("slug"));
          $(".form-chat textarea.chat-msg").data("slug-receiver", $(this).data("slug"));
          $(".form-chat textarea.chat-msg").attr("data-type-receiver", $(this).data("type"));
          $(".form-chat textarea.chat-msg").data("type-receiver", $(this).data("type"));
          $(".form-chat textarea.chat-msg").focus();

          Chat.clientInformation.receiverType = $(".form-chat textarea.chat-msg").data("type-receiver");
          Chat.clientInformation.receiverSlug = $(".form-chat textarea.chat-msg").data("slug-receiver");
          Chat.clientInformation.receiverThumb = $(this).data("thumb");

          var html =  '<img src="/uploads/imgProfil/'+$(this).data("thumb")+'" data-thumb="'+$(this).data("thumb")+'" '+
                            'alt="author" class="mCS_img_loaded margin-right-5 float-left" height="35"> '+

                      '<a href="/page/'+$(this).data("slug")+'" class="letter-white elipsis" id="chat-contact-name">' + 
                        '<b>'+$(this).data("name") +'</b>' +
                      '</a>';

          html += '<br><span class="more-title-chat">';
          html += '<i class="fa fa-microphone"></i> / <i class="fa fa-camera margin-right-10"></i>';

          
          html +=   '<a href="javascript:" '+
                        'id="btn-start-jitsi" data-slug="'+$(this).data("slug") + '"' +
                                            ' data-type="'+$(this).data("type")+ '"' +
                                            ' class="letter-white"'+
                                            ' title="'+ t.t("Audio & video call on Jitsi") + '">' + 
                        '<img src="/img/jitsi.png" height="20">' +
                      '</a>';
          
          if($(this).data("discord") != "" && $(this).data("discord") != null) 
            html +=   ' · <a href="'+$(this).data("discord")+'" '+
                        'target="_blank" class="letter-white" title="'+ t.t("Contact on Discord") + '">' + 
                        '<img src="/img/discord-new-logo.png" height="20">' +
                      '</a>';

            
          html += '</span>';

          //actualise le contenu du header de la fenêtre de chat
          //avec le nom de la Page avec laquelle on discute            
          $(".popup-chat .title").html(html);

          $(".popup-chat .icon-status").removeClass("online offline away")
                                       .addClass($("li.js-chat-open[data-slug='"+$(this).data("slug")+"'] .icon-status").data("status"));
          
          //clear la conversation dans la fenetre de chat
          $("ul.chat-message-field").html("");

          var nbMsgRead = parseInt($(".js-chat-open[data-slug='"+$(this).data("slug")+"'] .count-toread-msg").html());
          var nbTotalMsgNotRead = parseInt($("#site-header-responsive .label-avatar.message").html());
          var newNbMsgNotRead = nbTotalMsgNotRead - nbMsgRead;
          console.log("calculate nb not read", nbMsgRead, nbTotalMsgNotRead, newNbMsgNotRead);
          if(newNbMsgNotRead > 0)
            $("#site-header-responsive .label-avatar.message").html(newNbMsgNotRead)
                                                              .data("count", newNbMsgNotRead);
          else
            $("#site-header-responsive .label-avatar.message").html("0")
                                                              .data("count", "0")
                                                              .addClass("d-none");

          //re-init à 0 alert count msg toread un side-bar-right
          $(".js-chat-open[data-slug='"+$(this).data("slug")+"'] .count-toread-msg")
          .html(0)
          .data("count", 0).addClass("d-none");


          $('.popup-chat-responsive').addClass('open-chat');

          if($("ul.chat-users .js-chat-open[data-slug='"+Chat.clientInformation.receiverSlug+"']").length <= 0){
            Chat.clientInformation.receiverUsername = $(this).data("name");
            Chat.initNewReceiver(Chat.clientInformation);
            console.log("open ???", $("ul.chat-users .js-chat-open[data-slug='"+Chat.clientInformation.receiverSlug+"']").length);
        
          }

          //si on a déjà un historique de conversation on l'affiche
          if(typeof Chat.history[$(this).data("slug")] != "undefined"){
            var history = Chat.history[$(this).data("slug")];
            $.each(history, function(key, value){
              $("ul.chat-message-field").append(value.html);
              //remove flag "toread" (show msg with green color) before to register in client history
              //cause this message will realy be read by receiver
              history[key].html = value.html.replace(new RegExp("toread", 'g'), "");
            });
            Chat.initBtnDeleteMsg();


            console.log("init setTimeout toread");
            setTimeout(function(){
              $("ul.chat-message-field li").removeClass("toread");
              //Chat.
              console.log("clear toread");
            }, 3000);

            $(".popup-chat-responsive .mCustomScrollbar").animate({ 
                  scrollTop: $(".popup-chat-responsive .notification-list.chat-message-field").height() }, 0);


            console.log("SOCKET_ENABLED", SOCKET_ENABLED);
            if(SOCKET_ENABLED == "false"){
              console.log("SOCKET_ENABLED FALSE updateCurrentChat");
              Chat.updateCurrentChat();
            }

          } 
          else{ //sinon on va chercher l'historique en DB (5 derniers messages)
            Chat.getHistory($(this).data("slug"), $(this).data("name"));
          }

          var currentSlug = $(this).data("slug");
          var currentName = $(this).data("name");
          $(".popup-chat-responsive .mCustomScrollbar").off().on("scroll", function(e){ 
            //console.log("on scroll", $(this).scrollTop());
            if($(this).scrollTop() == 0){
              var date = $(".popup-chat-responsive .notification-list li.msg").first().data("date-created");
              console.log("DATE ?", date, moment(date).format("YYYY-MM-DD h:m:SZ"), moment(date).unix(), moment(date).format("x"));
              Chat.getMoreHistory(currentSlug, currentName, moment(date).format("x"));
            }
          }); 

          $(".load-before-conv").off().on("click", function(e){ 
            //console.log("on .load-before-conv click > Chat.getMoreHistory");
              var date = $(".popup-chat-responsive .notification-list li.msg").first().data("date-created");
              console.log("DATE ?", date, moment(date).format("YYYY-MM-DD h:m:SZ"), moment(date).unix(), moment(date).format("x"));
              Chat.getMoreHistory(currentSlug, currentName, moment(date).format("x"));
            
          }); 

          if(SOCKET_ENABLED == "false"){
            Chat.initTimerChat();
          }

          //initialisation des conversations audio/video via Jitsi
          $('#btn-start-jitsi').off().click(function(){
            var jitsiLink = "nexxo-" + $(this).data("slug"); //Chat.randJitsi();
            if($(this).data("type") == "user")
              jitsiLink += "-" + APP_USER_SLUG;

            var msg = "<center><i class='fa fa-microphone fa-2x'></i> / <i class='fa fa-camera fa-2x'></i><br>"+
                      t.t("join an audio/video call on Jitsi") + " :<br>" + 
                      'https://meet.jit.si/'+jitsiLink+" </center>";

            Chat.sendMessage(msg);
          });


          return false;
      });

      if(SOCKET_ENABLED == "false"){
        $(".js-chat-close").off().click(function(){
            Chat.stopTimerChat();
        });
      }else{
        $(".js-chat-maximize").off().click(function(){
            $(".popup-chat.popup-chat-responsive").toggleClass("maximize");
        });
      }
    },

    randJitsi : function() {
      var text = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

      for (var i = 0; i < 16; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

      return text;
    },


    initTimerChat: function(){
      if(Chat.timerChat != false)
          clearInterval(Chat.timerChat);

      Chat.timerChat = setInterval(function(){
          Chat.updateCurrentChat();
        }, INTERVAL_TIMER_CHAT);
    },

    stopTimerChat : function(){
      if(Chat.timerChat != false)
          clearInterval(Chat.timerChat);
      Chat.timerChat = false;
    },

    updateCurrentChat: function(){
      var receiverSlug =  Chat.clientInformation.receiverSlug;
      $("#icon-loading-chat").removeClass("transparent").addClass("fa-spin");
      $.ajax({
          type: "POST",
          url: "/chat/get-toread/"+receiverSlug,
          success: function(messages){
            console.log("response from : /chat/get-toread/"+receiverSlug, messages);
            setTimeout(function(){
              $("#icon-loading-chat").addClass("transparent").removeClass("fa-spin");
            }, 500);

            if(messages.length == 0){
              
            }else{
              $.each(messages, function(key, message){
                Chat.appendMessage(message);
                Chat.upTitlePage();
                nexxo.playSound("message");
              });
            }
          }
        });
    },

    appendMessage: function(data, scroll="bottom"){
        var receivedClass="";
        console.log("appendMessage", Chat.clientInformation.senderSlug ,"!=", data.senderSlug);
        if(Chat.clientInformation.senderUsername != data.senderUsername){
          receivedClass = "received";
        }
        if(data.senderSlug == data.receiverSlug){
          receivedClass = "";
        }

        var toreadClass = (typeof data.toread != "undefined" && data.toread == true && receivedClass == "received") ? "toread" : "";
        //console.log("toreadClass", toreadClass, data);
        
        // Append List Item
        var li = '<li id="msg-'+data.id+'" class="msg '+receivedClass+' '+toreadClass+'" data-date-created="'+data.created.date+'">'+
                  '<div class="author-thumb">'+
                    '<a href="/page/'+data.senderSlug+'" target="_blank">'+
                      '<img src="/uploads/imgProfil/'+data.senderThumb+'" alt="author" class="mCS_img_loaded" height="30" title="'+data.senderUsername+'">'+
                    '</a>'+
                  '</div>'+
                  '<div class="notification-event" data-toggle="tooltip" data-original-title="'+ data.senderUsername + '"> ' +
                    '<div class="margin-5">'+
                      '<b>'+data.senderUsername+'</b>'+
                      ' · <small>'+ data.createdDisplay+'</small>';
                      if(data.senderSlug == APP_USER_SLUG) {             // ·      
            li +=      '<a href="javascript:" class="btn-open-delete-chat-msg margin-left-10" title="'+t.t('delete this message')+'" data-id="'+data.id+'" style="color:unset;"><i class="fa fa-trash"></i></a>';
            li +=      '<div class="div-delete-chat-msg d-none" id="'+data.id+'">'+
                          '<a href="javascript:" class="btn-delete-chat-msg letter-red margin-left-10" data-id="'+data.id+'" style="color:unset;"><i class="fa fa-check"></i> '+t.t("Yes, delete")+'</a>'+
                          ' <a href="javascript:" class="btn-cancel-chat-msg letter-blue margin-left-10" data-id="'+data.id+'" style="margin-left:10px; color:unset;"><i class="fa fa-times"></i> '+t.t("No, cancel")+'</a>'+
                       '</div>';
        }

            li +='</div>' +
                    
                    '<span class="chat-message-item">'+
                      data.message;//+'</span>';//+
                    /*'<span class="notification-date margin-left-10 margin-right-10"><time class="entry-date updated" '+
                          'datetime="'+data.created+'">'+data.created+'</time>';*/
        
            li +=   '</span>'+
                  '</div>'+
                '</li>';

        var read = false;
        //si le message correspond à la conversation en cours, ouverte dans la fenêtre de chat,
        //on affiche le message tout de suite
        if( ($("textarea.chat-msg").data('slug-receiver') == data.senderSlug ||
             $("textarea.chat-msg").data('slug-receiver') == data.receiverSlug) &&
             $('.popup-chat-responsive').hasClass('open-chat') == true){

            if(scroll == "bottom"){
              $("ul.chat-message-field").append(li);
              $(".popup-chat-responsive .mCustomScrollbar").animate({ 
              scrollTop: $(".popup-chat-responsive .notification-list.chat-message-field").height() }, 
              0);
            }
            if(scroll == "top"){
              var height = $("ul.chat-message-field li.msg").first().height();
              //console.log("scroll top", height, $(".popup-chat-responsive .mCustomScrollbar").scrollTop());
              $("ul.chat-message-field").prepend(li);
              $(".popup-chat-responsive .mCustomScrollbar").animate({ 
              scrollTop: $(".popup-chat-responsive .mCustomScrollbar").scrollTop()+height }, 
              0);
            }

            
            $("#lbl-sending-chat-msg").addClass("d-none");

            //remove flag "toread" (show msg with green color) before to register in client history
            //cause this message will realy be read by receiver
            li = li.replace(new RegExp("toread", 'g'), "");
            
            //init timer pour changer la couleur des messages lus
            //vert > violet
            console.log("init setTimeout toread");
            setTimeout(function(){
              $("ul.chat-message-field li").removeClass("toread");
              console.log("clear toread");
            }, 3000);

            read = true;

        }else{
          /* USED ONLY WHEN SOCKET ARE ENABLED */
          if(SOCKET_ENABLED == "true") {
            //sinon on incrémente le compteur dans le menu-right
            var count = 
            $(".js-chat-open[data-slug='"+data.senderSlug+"'] .count-toread-msg")
                        .html(); count++;

            $(".js-chat-open[data-slug='"+data.senderSlug+"'] .count-toread-msg")
                        .html(count)
                        .data("count", count).removeClass("d-none");

            $(".mobile-app-tabs .label-avatar.message").html(count).removeClass("d-none");


            if(Chat.clientInformation.senderSlug != data.senderSlug &&
              $(".js-chat-open[data-slug='"+data.senderSlug+"']").data("type") == "user"){
              nexxo.playSound("message");
            }

            //move li to the top of the list
            /*var liHtml = $(".sidebar--large li.js-chat-open[data-slug='"+data.senderSlug+"']").clone();
            $(".sidebar--large li.js-chat-open[data-slug='"+data.senderSlug+"']").remove();
            $(".sidebar--large ul.chat-users").prepend(liHtml);
            $(".sidebar--large li.js-chat-open[data-slug='"+data.senderSlug+"']").data("count", count);
            var liHtml = $(".sidebar--small li.js-chat-open[data-slug='"+data.senderSlug+"']").clone();
            $(".sidebar--small li.js-chat-open[data-slug='"+data.senderSlug+"']").remove();
            $(".sidebar--small ul.chat-users").prepend(liHtml);
            Chat.initBtnStartChat();*/
            notificationObj.prependChatContact(data.senderSlug, count);


          }
        }

        //récupère le nom du channel
        var foreinSlug = (data.receiverSlug != Chat.clientInformation.senderSlug) ? data.receiverSlug : data.senderSlug;

        //si le channel local n'est pas ouvert
        //on créé le channel
        if(typeof Chat.history[foreinSlug] == "undefined")
          Chat.history[foreinSlug] = new Array();

        //mémorise le message
        Chat.history[foreinSlug].push({ html: li, id: data.id });

        /*if(scroll == "bottom")
        //scroll en bas de la fenêtre de chat
        $(".popup-chat-responsive .mCustomScrollbar").animate({ 
            scrollTop: $(".popup-chat-responsive .notification-list.chat-message-field").height() }, 
            200);*/

        Chat.initBtnDeleteMsg();

        return read;
    },

    initBtnDeleteMsg : function(){
      console.log("init btn delete message chat");
        $(".btn-open-delete-chat-msg").off().click(function(){
          var id = $(this).data("id");

          $(".div-delete-chat-msg#"+id).removeClass('d-none');

          $(".btn-cancel-chat-msg").off().click(function(){
            var id = $(this).data("id");
            $(".div-delete-chat-msg#"+id).addClass('d-none');
          });

          $(".btn-delete-chat-msg").off().click(function(){
            var id = $(this).data("id");

            $(".popup-chat li#msg-"+id).html("<i class='fa fa-spin fa-circle-o-notch'></i> "+t.t("Deleting your message"));
            $.ajax({
              type: "POST",
              url: "/chat/delete-message/"+id,
              success: function(res){
                   if(res.error == false){
                    $(".popup-chat li#msg-"+id).remove();

                    var history = Chat.history[Chat.clientInformation.receiverSlug];
                    if(history != null)
                      $.each(history, function(key, value){
                        if(value.id == id){
                          console.log("removed from history 1", history);
                          console.log("removed from history 2", value);
                          history.splice(key, 1);
                          console.log("removed from history 3", history);
                        }
                      });

                   }else{
                    toastr.error(res.errorMsg);
                   }
                   $(".div-delete-chat-msg#msg-"+id).addClass('d-none');
              },
              error: function(error){
                console.log("error from : /chat/delete-message/", error);
                toastr.error(res.errorMsg);
                $(".div-delete-chat-msg#"+id).addClass('d-none');
              }
            });
          });
        });

    },

    connectUser: function(){
        Chat.clientInformation.message = "";
        Chat.clientInformation.action = "connectUser";
        console.log("Chat.clientInformation", Chat.clientInformation);
        Chat.sendJson(Chat.clientInformation);
    },

    sendMessage: function(text){
        Chat.clientInformation.message = text;
        Chat.clientInformation.action = "sendMessage";
        console.log("Chat.clientInformation", Chat.clientInformation);

        $("#lbl-sending-chat-msg").removeClass("d-none");

        console.log("sendMessage SOCKET_ENABLED", SOCKET_ENABLED);
        if(SOCKET_ENABLED == "false") {
          console.log("sendMessage by ajax");
          Chat.sendMsgByAjax(Chat.clientInformation);
        }else{
          Chat.sendJson(Chat.clientInformation);
        }
    },

    sendMsgByAjax : function(clientInfo){
      var li = "<li id='info-msg-chat'>"+
                    "<i class='fa fa-circle-o-notch fa-spin margin-right-5'></i> <b>"+ t.t("Sending your message")+"</b>" +
                  "</li>";
        $("ul.chat-message-field li#info-msg-chat").remove();
        $("ul.chat-message-field").append(li);
        
        $.ajax({
          type: "POST",
          url: "/chat/send-message/",
          data: {
            message: clientInfo
          },
          success: function(res){            
              console.log("response from : /chat/send-message/", res.message);
              $("ul.chat-message-field li#info-msg-chat").remove();
              Chat.appendMessage(res.message);
        },
          error: function(error){
            console.log("error from : /chat/send-message/", error);
          }
        });
    },

    getHistory: function(slug, name){
        var li = "<li id='info-msg-chat'>"+
                    "<i class='fa fa-refresh fa-spin margin-right-5'></i> <b>"+ t.t("Loading ...")+"</b>" +
                  "</li>";
        $("ul.chat-message-field li#info-msg-chat").remove();
        $("ul.chat-message-field").prepend(li);
        
        $.ajax({
          type: "POST",
          url: "/chat/get-history/"+slug,
          success: function(messages){
            $("ul.chat-message-field li#info-msg-chat").remove();
            console.log("response from : /chat/get-history/"+slug, messages);
            if(messages.length == 0){
              var li = "<li>"+
                          t.t("You never chated with ")+"<b>"+name+"</b>"+".<br>" +
                          t.t("Send your first message now !") +
                        "</li>";
              $("ul.chat-message-field").append(li);
            }else{
              $.each(messages, function(key, message){
                Chat.appendMessage(message);
              });
            }
          },
          error: function(){
            $("ul.chat-message-field li#info-msg-chat").remove();
          }
        });
    },

    getMoreHistory: function(slug, name, date){
        console.log("getMoreHistory", slug, name, date);
        var li = "<li id='info-msg-chat'>"+
                    "<i class='fa fa-refresh fa-spin margin-right-5'></i> <b>"+ t.t("Loading ...")+"</b>" +
                  "</li>";
        $("ul.chat-message-field li#info-msg-chat").remove();
        $("ul.chat-message-field").prepend(li);

        $.ajax({
          type: "POST",
          url: "/chat/get-more-history/"+slug+"/"+date,
          success: function(messages){
            console.log("response from : /chat/get-more-history/"+slug, messages);
            $("ul.chat-message-field li#info-msg-chat").remove();
            if(messages.length == 0){
              var li = "<li id='info-msg-chat'>"+
                          "<i class='fa fa-check'></i> "+ t.t("All messages are there") +
                        "</li>";
              $("ul.chat-message-field").prepend(li);
            }else{
              $.each(messages, function(key, message){
                Chat.appendMessage(message, "top");
              });
            }
          },
          error: function(){
            $("ul.chat-message-field li#info-msg-chat").remove();
          }
        });
    },

    setReadHistory : function(slug){
      $.ajax({
          type: "POST",
          url: "/chat/set-read-history/"+slug,
          success: function(res){
            console.log("response from : /chat/set-read-history/"+slug, res);
          }
        });
    },

    broadcastNotification: function(notif, whatHtml="", whatAnswerId=null){ 
      console.log("broadcastNotification SOCKET_ENABLED", SOCKET_ENABLED);
      if(SOCKET_ENABLED == "false") {
        console.log("broadcastNotification cancel");
        return; 
      }

      console.log("broadcastNotification", notif, "whatAnswerId", whatAnswerId);
      //alert("wait !");
      if(whatAnswerId != null) notif.whatAnswerId = whatAnswerId;

      Chat.sendJson({
        senderSlug: Chat.clientInformation.senderSlug,
        action: "broadcastNotification",
        notif: notif,
        whatHtml: whatHtml,
      });
    },

    sendJson: function(json){ //console.log("sendJson 1");
      if(json != false)
        Chat.memSendJson = json;

      if(Chat.conn.readyState == 1){
        console.log("sendJson 3, readyState", Chat.conn.readyState, "json", json);
        
        Chat.conn.send(JSON.stringify(json));
        Chat.memSendJson = "";

        Chat.showChatServerStatus(true);
        return;
      }

      if(Chat.conn.readyState != 1){  
          console.log("sendJson, error socket disconnected, readyState", Chat.conn.readyState);
          Chat.showChatServerStatus(false);
          Chat.initChat();
      }
    },

    showChatServerStatus: function(connected){
      if(connected){
        $(".lbl-socket-connected i.fa").removeClass("letter-red").addClass("letter-green");
        $(".lbl-socket-connected").attr("title", t.t("The chat server is online"));
      }else{
        $(".lbl-socket-connected i.fa").removeClass("letter-green").addClass("letter-red");
        $(".lbl-socket-connected").attr("title", t.t("The chat server is offline. You will be automaticaly reconnected as soon as possible, please wait."));
      }
    }


};