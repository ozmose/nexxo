var t = {

	t : function(key){
		if(typeof LOCALE != "undefined"){
	      if(typeof this[LOCALE][key] != "undefined"){
	         return this[LOCALE][key];
	      }
	    }
	    return key;
	},

	'en': { }, //default lang : empty
	'fr': {
		"Request sent !": "Demande envoyée !",
		"You follow this page.": "Vous êtes abonné cette page.",
		"You don't follow this page anymore.": "You don't follow this page anymore.",
		"You participate to this event.": "Vous participez à cet événement",
		"You don't participate to this event.": "Vous ne participez plus à cet événement",
		"An error sending your like request": "Une erreur est survenue pendant l'envoi de votre like",
		"You like this page.": "Vous aimez cette page.",
		"You don't like it anymore.": "Vous n'aimez plus cette page",
		"You are no longer friend/member.": "Vous n'êtes plus ami/membre",
		"You don't follow this page anymore.": "Vous n'êtes plus abonné à cette page",
		"You are no longer an administrator of this page": "Vous n'êtes plus administrateur de cette page",
		"You don't participate it anymore.": "Vous ne participez plus.",
		"Report sent": "Signalement envoyé",
		"Vote sent": "Vote envoyé",
		"Sorry, an error has occured ": "Désolé, une erreur est survenu",
		"Sorry, an error has occured": "Désolé, une erreur est survenu",
		"other notification(s)" : "autre(s) notification(s)",
		"You never chated with ": "Vous n'avez jamais discuté avec ",
		"Send your first message now !": "Envoyez lui votre premier message dès maintenant !",
		"Loading ...": "Chargement en cours ...",
		"All messages are there": "Tous les messages sont déjà affichés",
		"Move this marker to the position you want": "Déplacez l'icon sur la position qui vous convient",
		"Save this position": "Enregistrer cette position",
		"Quick view": "Apperçu",
		"loading preview": "Chargement de l'aperçu",
		"Loading comments": "Chargement des commentaires",
		"Sending your comment": "Envoi de votre commentaire",
		"Sending your message": "Envoi de votre message",
		"select": "sélectionner",
		"Request accepted : you are friends !": "Demande acceptée : vous êtes amis !",
		"Request accepted : here is a new member in your page !" : "Demande acceptée : nouveau membre pour votre page",
		"Request refused": "Demande refusée",
		"Map is loading": "Chargement de la carte",
		"Request accepted : you added a new admin !": "Demande acceptée : nouvel administrateur pour votre page",
		"Your text message is empty. Please write something before to send your post !": "Le texte de votre message est vide. Merci d'écrire quelque chose avant d'envoyer votre message.",
		"This page is not localised": "Cette page n'est pas géo-localisée",
		"Your post has been edited": "Votre message a été modifié",
		"Your news has been deleted": "Votre message a été supprimé",
		"Post shared !": "La publication a bien été partagée",
		"Error sharing post": "Une erreur est survenue pendant le partage du message. Merci de recharger la page, et essayer à nouveau.",
		"Deleting your message": "Suppression de votre message en cours",
		"Yes, delete": "Oui, supprimer",
		"No, cancel": "Non, annuler",
		"Looking for this address": "Nous recherchons la position de l'addresse indiquée",
		"delete this message" : "supprimer ce message",
		"Sorry, this image is too big ! (> 2MB) Thank you to reduce the size of your image or to choose a smaller image." : "Désolé, la taille de cette image est trop grande (> 2MB). Merci de réduire la taille de votre image, ou d'en choisir une plus petite.",
		"Contact on Discord" : "Discuter sur Discord",
		"join an audio/video call on Jitsi" : "rejoindre une appel audio/vidéo sur Jitsi",
		"Audio & video call on Jitsi" : "appel audio/vidéo sur Jitsi",
		"Website" : "Site web",
		"Looking for the position" : "Nous recherchons la position de l'addresse indiquée",
		"The chat server is online": "Vous êtes connecté au serveur de notification en direct",
		"The chat server is offline. You will be automaticaly reconnected as soon as possible, please wait.": "Vous êtes déconnecté de serveur de notification en direct. Vous serez automatiquement reconnecté dès que possible, merci de patienter."
	}
};