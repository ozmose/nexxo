var pageObj = {

	canvasId : "map-container",

	showMapUpdatePos : function(afterLoad){ console.log("call showMapUpdatePos");
		var lat = $("#page_latitude").val();
		var lng = $("#page_longitude").val();
		var coordinates = new Array(lat, lng);
		var name = $("#page_name").val();
		var type = currentFormType;

        Sig.init(pageObj.canvasId, function(){
        	$("#"+pageObj.canvasId).removeClass("d-none");
            $(".app-search").addClass("sig-open");

            var scrollWheelZoom = ($("body").width() < 767) ? false : true;
	        Sig.loadMap(pageObj.canvasId, {
                center : coordinates,
                zoom: 8,
                maxZoom: 17,
                scrollWheelZoom: scrollWheelZoom
            });

            var icon = Sig.getMarkerIcon(type);
            var markerOptions = { icon : icon };

            if(Sig.markerDrag != false){
                console.log("removeLayer");
                Sig.map.removeLayer(Sig.markerDrag);
            }

            Sig.markerDrag = L.marker(coordinates, markerOptions)
                            .addTo(Sig.map);

            Sig.markerDrag.bindPopup("<div class='text-center'>"+
            							"<b>"+t.t("Move this marker to the position you want")+"</b>"+
            							"<button class='btn bg-green margin-top-10' id='btn-validate-position'>"+
            								"<i class='fa fa-check'></i> "+t.t("Save this position")+
            							"</button>" +
            						 "</div>")
                            .openPopup()
                            .dragging.enable();

            $("#page_latitude").val(Sig.markerDrag.getLatLng().lat);
            $("#page_longitude").val(Sig.markerDrag.getLatLng().lng);
            Sig.markerDrag.on('dragend', function(e){
                this.openPopup();  
                $("#page_latitude").val(this.getLatLng().lat);
                $("#page_longitude").val(this.getLatLng().lng);

	            $(".app-search #btn-validate-position, .app-agenda #btn-validate-position").click(function(){
	            	var lat = $("#page_latitude").val();
	            	var lng = $("#page_longitude").val();
	            	var coordinates = new Array(lng, lat);
	            	pageObj.saveAddressPosition(
	            				 $("#page_country").val(),
	            				 $("#page_city").val(),
	            				 $("#page_streetAddress").val(),
	            				 coordinates
	            			);
	            });
            });

            $(".geoloc-result.conf-pos").removeClass("d-none");

            if(afterLoad != null)
            	afterLoad();
            
        });
    },

	initFormCreatePage : function(){
		$("#page_city").keyup(function(){
            //if(timeout != false) clearTimeout(timeout);
            $('.dropdown-menu').html('<li><a href="#address-block" class="dropdown-item">'+
                                     '<i class="fa fa-spin fa-refresh"></i></a></li>').show();

        });
		console.log("#page_latitude ?", $("#page_latitude").val());
		if($("#page_latitude").val() == "false" || $("#page_latitude").val() == ""){
        	$("#check-user-geoloc").prop('checked', false);
		}else{
			pageObj.showMapUpdatePos();
		}

        $("#check-user-geoloc").change(function(){
          console.log( $("#check-user-geoloc").is(':checked'));
          if($("#check-user-geoloc").is(':checked')){
            var countryCode = $("#page_country").val();
            var city = $("#page_city").val();
            var street = $("#page_streetAddress").val();
            $(".geoloc-result").removeClass("d-none");
          	//$("#"+pageObj.canvasId).removeClass("d-none")
            $("#geoloc-result").html('<li class="inline-items">'+
                                        '<div class="col-sm-8 no-padding">'+
                                            '<i class="fa fa-spin fa-refresh"></i> '+
                                            '<b>'+t.t('Looking for this address')+'</b>'+
                                        '</div>'+
                                     '</li>');

            Sig.callNominatim(countryCode, city, street);
            
          }else{
          	$("#"+pageObj.canvasId).addClass("d-none");
          	$(".geoloc-result").addClass("d-none");
          	$("#page_latitude").val("false");
			$("#page_longitude").val("false");
		
          }
        });

        $("#page_isPrivate").change(function(){
          if($("#page_isPrivate").is(':checked')){
            $("#lbl-isprivate-false").addClass("d-none");
            $("#lbl-isprivate-true").removeClass("d-none");
          }else{
            $("#lbl-isprivate-false").removeClass("d-none");
            $("#lbl-isprivate-true").addClass("d-none");
          }
        });

        if($("#page_isPrivate").is(':checked')){
            $("#lbl-isprivate-false").addClass("d-none");
            $("#lbl-isprivate-true").removeClass("d-none");
        }else{
            $("#lbl-isprivate-false").removeClass("d-none");
            $("#lbl-isprivate-true").addClass("d-none");
        }


        if(typeof formStartDate != "undefined" && formStartDate != ""){
        	$("#page_startDate").val(formStartDate);
        	$("#page_endDate").val(formEndDate);
        }
        //pageObj.initInputDate(".js-datepicker#page_startDate");
       // pageObj.initInputDate(".js-datepicker#page_endDate");
	},

	initInputDate : function(id){
		var date_select_field = $(id);
		var date = date_select_field.val() != "" ? date_select_field.val() : moment();
		date_select_field.daterangepicker({
            startDate: date,
            autoUpdateInput: true,
            singleDatePicker: true,
            autoApply: true,
            //showDropdowns: true,
            alwaysShowCalendars: true,
            timePicker: true,
            timePickerIncrement: 15,
            timePicker24Hour: true,
            locale: {
                format: 'YYYY-MM-DD HH:mm'
            }
        });
        date_select_field.on('focus', function () {
            $(this).closest('.form-group').addClass('is-focused');
        });
        date_select_field.on('apply.daterangepicker', function (ev, picker) {
            console.log("picker.startDate", picker.startDate);
            $(this).val(picker.startDate.format('YYYY-MM-DD HH:mm'));
            $(this).closest('.form-group').addClass('is-focused');
        });
        date_select_field.on('hide.daterangepicker', function () {
            if ('' === $(this).val()){
                $(this).closest('.form-group').removeClass('is-focused');
            }
        });
	},

	initFormConfidentiality : function(){
		$(".btn-select-confidentiality").click(function(){
			var confkey = $(this).data("confkey");
			var confval = $(this).data("confval");
			var slug = $(this).data("slug");
			pageObj.saveConfidentiality(confkey, confval, slug);
		});
	},
	
	initFormPreferences : function(){
		$.each($(".btn-select-preferences"), function(key, val){
			$(this).prop('checked', $(this).data('confval'));
		});

		$(".btn-select-preferences").change(function(){
			var confkey = $(this).data("confkey");
			var confval = $(this).prop('checked');
			var slug = $(this).data("slug");
			console.log("confkey", confkey, confval, slug);
			pageObj.savePreferences(confkey, confval, slug);
	    });
	},

	saveConfidentiality : function(confkey, confval, slug){
		$.ajax({
	      type: "POST",
	      data : { "confidentiality" : { 
	                  "confkey" 	: confkey,
	                  "confval" 	: confval
	                }
	      },
	      url: "/save-confidentiality/"+slug,
	      success: function(res){ //alert("déjà" + res.html); console.log("new comment ok", res);
	        console.log("success /save-confidentiality", res);

	        if(res.error == false){
	          	$(".block-select-conf[data-confkey='"+confkey+"'] .btn-select-confidentiality").removeClass("active");
	          	$(".block-select-conf[data-confkey='"+confkey+"'] .btn-select-confidentiality[data-confval='"+confval+"']").addClass("active");
	        	toastr.success(res.html);
	        }else{
	          toastr.error(res.html);
	        }
	      }
	    });
	},

	savePreferences: function(confkey, confval, slug){
		$.ajax({
	      type: "POST",
	      data : { "preferences" : { 
	                  "confkey" 	: confkey,
	                  "confval" 	: confval,
	                }
	      },
	      url: "/save-preferences/"+slug,
	      success: function(res){ //alert("déjà" + res.html); console.log("new comment ok", res);
	        console.log("success /save-preferences/"+slug, res);
	        //toastr.success(res.html);
	        /*if(typeof res.error == "undefined"){
	          toastr.success(res.errorMsg);
	        }else{
	          toastr.error(res.errorMsg);
	        }*/
	      }
	    });
	},

	saveAddressPosition: function(country, city, streetAddress, coordinates){
		$.ajax({
	      type: "POST",
	      data : {    "country" 		: country,
	                  "city" 			: city,
	                  "streetAddress" 	: streetAddress,
	                  "coordinates" 		: coordinates,
	               
	      },
	      url: "/save-position/"+APP_USER_SLUG,
	      success: function(res){ //alert("déjà" + res.html); console.log("new comment ok", res);
	        console.log("success /save-position/"+APP_USER_SLUG, res);
	        toastr.success("Votre position a été mise à jour. L'interface doit être actualisée, merci de patienter.");
	        setTimeout(function(){
	        	location.reload();
	        }, 2000);
	        /*if(typeof res.error == "undefined"){
	          toastr.success(res.errorMsg);
	        }else{
	          toastr.error(res.errorMsg);
	        }*/
	      }
	    });
	}

};