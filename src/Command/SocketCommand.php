<?php
// myapplication/src/sandboxBundle/Command/SocketCommand.php
// Change the namespace according to your bundle
namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

// Include ratchet libs
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;

// Change the namespace according to your bundle
use App\Sockets\Chat;

// Change the namespace according to your bundle
use App\Services\IoSecureServer;

class SocketCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('sockets:start-chat')
            // the short description shown while running "php bin/console list"
            ->setHelp("Starts the chat socket")
            // the full command description shown when running the command with
            ->setDescription('Starts the chat socket')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(['','',
            '===============',
            'Start Chat Socket open on 0.0.0.0:4044, env:production (IoSecureServer) x - '.`pwd`,
            '==============='
        ]);
        
        $server = IoSecureServer::factory(
            new HttpServer(
                new WsServer(
                    new Chat($this->getContainer())
                )
            ),
            4044, '0.0.0.0', array(  'local_cert' => './enc/nexxociety.co-2019-04-22.crt',
                                    'local_pk' => './enc/nexxociety.co-2019-04-22.key',
                                    'allow_self_signed' => true,
                                    'verify_peer' => false)
        );
        
        $server->run();
    }
}