<?php

// Change the namespace according to your bundle, and that's all !
namespace App\Sockets;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

use App\Entity\User;
use App\Entity\Page;
use App\Entity\Relation;
use App\Entity\ChatMessage;
use Ratchet\Wamp\WampServerInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;

class Pusher implements WampServerInterface {
    protected $channelUsers;

    //protected $channels;
    //protected $channelUsers;

    public function __construct(ContainerInterface $container) {
        $this->channelUsers = array();//new \SplObjectStorage;
        //$this->channels = new \SplObjectStorage;
        $this->container = $container;
    }

     public function onSubscribe(ConnectionInterface $conn, $topic) {
    }
    public function onUnSubscribe(ConnectionInterface $conn, $topic) {
    }
    public function onCall(ConnectionInterface $conn, $id, $topic, array $params) {
        // In this application if clients send data it's because the user hacked around in console
        $conn->callError($id, $topic, 'You are not allowed to make calls')->close();
    }
    public function onPublish(ConnectionInterface $conn, $topic, $event, array $exclude, array $eligible) {
        // In this application if clients send data it's because the user hacked around in console
        $conn->close();
    }


    public function onOpen(ConnectionInterface $conn) {
        // Store the new connection to send messages to later
        //$this->channels->attach($conn);

        echo "\nNew connection! ({$conn->resourceId})\n";
    }

    public function onBlogEntry($msg){
          echo sprintf("\n-------------------- onBlogEntry ".$msg);
          dd("ollaala");
    }

    public function onMessage(ConnectionInterface $from, $msg) {

        echo sprintf("\n-------------------- onMessage ".$msg);
        
        $msg = json_decode($msg);
        $slug = isset($msg->senderSlug) ? $msg->senderSlug : @$msg->slug;
        echo sprintf("\n-------------------- onMessage from -".$slug."-  action -".$msg->action."- ");

        /*******************************
            ACTION 1 : connectUser
        ********************************/
        if($msg->action == "connectUser"){
            //si le user n'a pas de channel
            if(!isset($this->channelUsers[$msg->senderSlug])){
                //mise à jour du token
                $em = $this->container->get('doctrine_mongodb')->getManager();
                $pageRepo = $em->getRepository(Page::class);
                $page = $pageRepo->findOneBy(array("slug" => $msg->senderSlug));

                $token = "";
                if($page->getChatToken() == $msg->senderToken){
                    $token = $page->initChatToken();
                    $em->flush($page);
                }else{
                    echo sprintf("\nERROR TOKEN 1 : ".$msg->senderToken. ', '.
                                                     $page->getChatToken() );
                    return;
                }

                $this->channelUsers[$msg->senderSlug] = array("token"    => $token,
                                                              "senderUsername" => $msg->senderUsername,
                                                              "channel" => new \SplObjectStorage,
                                                              "status" => $msg->status);

                $this->channelUsers[$msg->senderSlug]["channel"]->attach($from);
                
                echo sprintf("\nSAVE CHANNEL IN SESSION - ".$msg->senderUsername);
                $chan = $this->channelUsers[$msg->senderSlug]["channel"];
                $this->container->get('session')->set('mychan', $chan);

                dump($chan);
                dump($this->container->get('session')->get('mychan'));

                $msg->newToken = $token;
                $from->send(json_encode($msg));
                echo sprintf("\nCHANNEL -".$msg->senderUsername."- CONNECTED"."\n********* new token : ".$token);
            }else{
                if($this->channelUsers[$msg->senderSlug]["token"] != $msg->senderToken){
                    echo sprintf("\nERROR TOKEN 2 : ".$msg->senderToken. ', '.
                                                     $this->channelUsers[$msg->senderSlug]["token"]);
                    return;
                }else{
                    echo sprintf("\nOPEN PARALLELE CHANNEL -".$msg->senderUsername."- token : ".$msg->senderToken);
                    $this->channelUsers[$msg->senderSlug]["channel"]->attach($from);
                }
            }

            echo sprintf("\nCONNECT MY GROUPS -".$msg->senderSlug."- ");
            $em = $this->container->get('doctrine_mongodb')->getManager();
            $pageRepo = $em->getRepository(Page::class);
            $page = $pageRepo->findOneBy(array("slug" => $msg->senderSlug));
            //enregistre le status en db
            $page->setChatStatus($msg->status);
            $em->flush();
            //enregistre le statut sur mon channel
            $this->channelUsers[$msg->senderSlug]["status"] = $msg->status;

            $myFriends = $page->getRelations()->getChatContacts();
            $status = array();
            foreach ($myFriends as $key => $friend) {
                if($friend->getType() != Page::TYPE_USER){
                    $slug = $friend->getSlug();

                    //si le channel n'existe pas, il faut le créer
                    if(!isset($this->channelUsers[$slug])){
                        $this->channelUsers[$slug] = array( "token"    => "no-token", //no need
                                                            "senderUsername" => "no-username",//no need
                                                            "channels" => array(),
                                                            "status" => "online");//new \SplObjectStorage);

                        echo sprintf("\nCREATE CHANNEL FOR GROUP -".$msg->receiverSlug."-");
                    }

                    $this->channelUsers[$slug]["channels"][$msg->senderSlug] = new \SplObjectStorage;
                    $this->channelUsers[$slug]["channels"][$msg->senderSlug]->attach($from);
                    echo sprintf("\nUPDATED GROUP CONNECTION -".$msg->senderSlug."- IN CHANNEL -".$slug."-");
                }else{
                    //on envoie un message à tous mes amis pour leur dire que je suis connecté
                    $this->broadcastChangeStatus($msg->senderSlug, $friend->getSlug(), $msg->status);
                }

                //si le friend n'est pas connecté, il est "offline"
                //sinon on prend son status actuel
                $friendStatus = isset($this->channelUsers[$friend->getSlug()]) ? 
                                    $this->channelUsers[$friend->getSlug()]["status"] : "offline";

                $status[$friend->getSlug()]["status"] = $friendStatus;
            }

            $from->send(json_encode(array("action"=>"status", 
                                          "status" => $status)));
        }
        /*******************************
            ACTION 2 : connectUser
        ********************************/
        elseif($msg->action == "sendMessage"){


            if($this->channelUsers[$msg->senderSlug]["token"] != $msg->senderToken){
                    echo sprintf("\nERROR TOKEN 3 : ".$msg->senderToken. ', '.
                                                     $this->channelUsers[$msg->senderSlug]. ', '.
                                                     $this->channelUsers[$msg->senderSlug]["token"]);
                    return;
            }

            if($msg->receiverType == Page::TYPE_USER){
                //si le channel du receveur exist (si le receveur est connecté au chat)
                if(isset($this->channelUsers[$msg->receiverSlug])){
                    foreach ($this->channelUsers[$msg->receiverSlug]["channel"] as $key => $chanel) {

                        echo sprintf("\nSEND MSG TO CHANNEL -".$this->channelUsers[$msg->receiverSlug]["senderUsername"]."- ".
                                     "chanel : ".$chanel->resourceId. " - msg : ".$msg->message);

                        //BROADCAST USER RECEIVER (all connection of same user)
                        $chanel->send(json_encode($msg));
                        $this->checkIsMyContact($msg);
                    }

                    //BROADCAST USER SENDER (all connection of same user)
                    foreach ($this->channelUsers[$msg->senderSlug]["channel"] as $key => $chanel) {
                        $chanel->send(json_encode($msg));
                    }
                }else{
                    echo sprintf("\nthe receiver -".$msg->receiverSlug."- is not connected, this msg will be save in db");
                }
            }
            elseif($msg->receiverType != Page::TYPE_USER){
                //si un user essaie d'envoyer un message à une Page de type "group" (!= user) 
                //et que personne n'a ouvert le channel avant lui
                //on créé le channel de la Page
                if(!isset($this->channelUsers[$msg->receiverSlug])){
                    $this->channelUsers[$msg->receiverSlug] = array("token"    => "no-token", //no need
                                                                    "senderUsername" => "no-username",//no need
                                                                    "channels" => array(),
                                                                    "status" => "online");//new \SplObjectStorage);

                    echo sprintf("\nCREATE CHANNEL FOR GROUP -".$msg->receiverSlug."-");
                }

                //on actualise le channel du sender (au cas où il aurait changé)
                $this->channelUsers[$msg->receiverSlug]["channels"][$msg->senderSlug] = new \SplObjectStorage;
                $this->channelUsers[$msg->receiverSlug]["channels"][$msg->senderSlug]->attach($from);
                echo sprintf("\nUPDATE -".$msg->senderSlug."- IN CHANNEL -".$msg->receiverSlug."-");
                
                $msg->senderSlug = $msg->receiverSlug;
                //BROADCAST ALL USER REGISTRED IN GROUP CHANNEL
                foreach ($this->channelUsers[$msg->receiverSlug]["channels"] as $key => $chanels) {
                    foreach ($chanels as $k => $chan) {
                        $chan->send(json_encode($msg));
                    }
                }
                echo sprintf("\nSEND MSG TO CHANNEL GROUP -".$msg->receiverSlug."- ".
                            "(".count($this->channelUsers[$msg->receiverSlug]["channels"]).") ".
                            "- msg : ".$msg->message);
            }

            $em = $this->container->get('doctrine_mongodb')->getManager();
            $chatMsg = new ChatMessage($msg);
            $em->persist($chatMsg);
            $em->flush();
        }
        /*******************************
            ACTION 3 : connectUser
        ********************************/
        elseif($msg->action == "changeStatus"){
            $this->saveChangeStatus($msg->slug, $msg->status);
        }
        else{
            echo sprintf("\nno action found");
        }


            echo sprintf("\n===============");
    }

    public function onClose(ConnectionInterface $conn) {

        echo sprintf("\n-------------------- onClose ");

        // The connection is closed, remove it, as we can no longer send it messages
        //$this->clients->detach($conn);
        $mySlug = "";
        foreach ($this->channelUsers as $key => $channels) { 
            echo "\ngo to each channel ** ".$key;
            if(isset($channels["channel"]))
            foreach ($channels["channel"] as $keyC => $channel) { 
                //dump($channel);
                echo "\neach channel ** keyC:".$keyC." /\ conn:".$conn->resourceId." / conn:".$channel->resourceId;
               //$test = $channel["channel"]->resourceId;

                if ($conn->resourceId == $channel->resourceId) {
                    echo sprintf("\n∕∕ DETACH ** ".$key);
                    $this->channelUsers[$key]["channel"]->detach($conn);
                    unset($this->channelUsers[$key]);
                    echo sprintf("\n∕∕ UNSET ** ".$key);
                    $mySlug = $key;
                }
            }
        }

        echo sprintf("\n change status to OFFLINE for channel  -".$mySlug."-");

        //enregistre le statut sur mon channel
        //$this->channelUsers[$mySlug]["status"] = "offline";
        
        $em = $this->container->get('doctrine_mongodb')->getManager();
        $pageRepo = $em->getRepository(Page::class);
        $page = $pageRepo->findOneBy(array("slug" => $mySlug));
        $myFriends = $page->getRelations()->getFriends();

        echo sprintf("\BROADCAST NEW STATUS OFFLINE TO MY FRIENDS");
        foreach ($myFriends as $key => $friend) {
            if($friend->getType() == Page::TYPE_USER)
            $this->broadcastChangeStatus($mySlug, $friend->getSlug(), "offline");
        }

        //clear Page::chatToken
        echo "\n Connection {$conn->resourceId} has disconnected\n";
    }

    public function broadcastChangeStatus($mySlug, $myFriendSlug, $myNewStatus){
        if(isset($this->channelUsers[$myFriendSlug])){
            foreach ($this->channelUsers[$myFriendSlug]["channel"] as $k1 => $chan) {
                $chan->send(json_encode(array("action"=>"friendChangeStatus", 
                                               "slug" => $mySlug,
                                               "status"=>$myNewStatus)));
            }
        }
        echo sprintf("\nBROADCAST MY NEW STATUS ** ** of -".$mySlug."-  to -".$myFriendSlug."-");
    }

    public function saveChangeStatus($mySlug, $myNewStatus){
        //récupère la page en db
        //TODO : check token !
        $em = $this->container->get('doctrine_mongodb')->getManager();
        $pageRepo = $em->getRepository(Page::class);
        $page = $pageRepo->findOneBy(array("slug" => $mySlug));

        //enregistre le status en db
        $page->setChatStatus($myNewStatus);

        $em->flush();
        //enregistre le statut sur mon channel
        $this->channelUsers[$mySlug]["status"] = $myNewStatus;

        $myFriends = $page->getRelations()->getFriends();
        echo sprintf("\nSAVE NEW STATUS ** ** of -".$mySlug."-");
        foreach ($myFriends as $key => $friend) {
            if($friend->getType() == Page::TYPE_USER)
            $this->broadcastChangeStatus($mySlug, $friend->getSlug(), $myNewStatus);
        }
    }

    public function checkIsMyContact($msg){
        echo sprintf("\nCHECK IS MY CONTACT ** ** of -".$msg->senderSlug."- && ".$msg->receiverSlug);
        $em = $this->container->get('doctrine_mongodb')->getManager();
        $pageRepo = $em->getRepository(Page::class);
        $myPage = $pageRepo->findOneBy(array("slug" => $msg->senderSlug));
        $distPage = $pageRepo->findOneBy(array("slug" => $msg->receiverSlug));
        $myPage->getRelations()->addChatContacts($distPage);
        $distPage->getRelations()->addChatContacts($myPage);
    }

    public function sendNotif(){

        echo sprintf("\nSEND NOTIF ?".sizeof($this->channelUsers));
        foreach ($this->channelUsers as $key => $value) {
            echo sprintf("\nSEND NOTIF OK - ".$key);
        }
        echo sprintf("\nSEND NOTIF OKK");
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }
}