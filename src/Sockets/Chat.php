<?php

// Change the namespace according to your bundle, and that's all !
namespace App\Sockets;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

use App\Entity\User;
use App\Entity\Page;
use App\Entity\Relation;
use App\Entity\Notification;
use App\Entity\ChatMessage;

use App\Services\Helper;

use Symfony\Component\DependencyInjection\ContainerInterface;

use Doctrine\Common\Cache\ArrayCache;

class Chat implements MessageComponentInterface {
    protected $channelUsers;
    //protected $channels;
    //protected $channelUsers;

    public function __construct(ContainerInterface $container) {
        $this->channelUsers = array();//new \SplObjectStorage;
        $this->usersOnline = array();//new \SplObjectStorage;

        //$this->channels = new \SplObjectStorage;
        $this->container = $container;
        echo "Construct new chat server : ok\n";
    }

    public function onOpen(ConnectionInterface $conn) {
        // Store the new connection to send messages to later
        //$this->channels->attach($conn);

        //echo "\nNew connection! ({$conn->resourceId})\n";
    }

    /*public function onBlogEntry($msg){
          echo sprintf("\n-------------------- onBlogEntry ".$msg);
          dd("ollaala");
    }*/

    public function onMessage(ConnectionInterface $from, $msg) {

        //echo sprintf("\n-------------------- onMessage ".$msg);
        
        $msg = json_decode($msg);
        $slug = isset($msg->senderSlug) ? $msg->senderSlug : @$msg->slug;
        //echo sprintf("\n-------------------- onMessage from -".$slug."-  action -".$msg->action."- ");


        //used to test crashing supervisor
        if($msg->action == "crash"){
            //echo 100/0;
            throw new Error('An error occured');
            exit;
        }

        /*******************************
            ACTION 1 : connectUser
        ********************************/
        if($msg->action == "connectUser"){
            //si le user n'a pas de channel
            if(!isset($this->channelUsers[$msg->senderSlug])){
                //mise à jour du token
                $em = $this->container->get('doctrine_mongodb')->getManager();
                $pageRepo = $em->getRepository(Page::class);
                $page = $pageRepo->findOneBy(array("slug" => $msg->senderSlug));

                if($page == null) return;

                $token = "";
                if($page->getChatToken() == $msg->senderToken){
                    $token = $page->initChatToken();
                    $em->flush($page);
                }else{
                    echo sprintf("\nERROR TOKEN 1 : ".$msg->senderToken. ', '.
                                                      $page->getChatToken() );
                    return;
                }

                $this->channelUsers[$msg->senderSlug] = array("token"    => $token,
                                                              "senderUsername" => $msg->senderUsername,
                                                              "channel" => new \SplObjectStorage,
                                                              "status" => $msg->status);

                $this->channelUsers[$msg->senderSlug]["channel"]->attach($from);
                $this->usersOnline[$msg->senderSlug] = true;
                //echo sprintf("NEW CHANNEL CREATED -".$msg->senderUsername."- ONLINE COUNT ".count($this->usersOnline)."\n");

                $msg->newToken = $token;
                $from->send(json_encode($msg));
                //echo sprintf("\nCHANNEL -".$msg->senderUsername."- CONNECTED"."\n********* new token : ".$token);
            }else{
                if($this->channelUsers[$msg->senderSlug]["token"] != $msg->senderToken){
                    //echo sprintf("\nERROR TOKEN 2 : ".$msg->senderToken. ', '.
                    //                                 $this->channelUsers[$msg->senderSlug]["token"]);
                    return;
                }else{
                    //echo sprintf("\nOPEN PARALLELE CHANNEL -".$msg->senderUsername."- token : ".$msg->senderToken);
                    $this->channelUsers[$msg->senderSlug]["channel"]->attach($from);
                }
            }

            //echo sprintf("\nCONNECT MY GROUPS -".$msg->senderSlug."- ");
            $em = $this->container->get('doctrine_mongodb')->getManager();
            $em->clear();
            $pageRepo = $em->getRepository(Page::class);
            $page = $pageRepo->findOneBy(array("slug" => $msg->senderSlug));
            //enregistre le status en db
            //$page->setChatStatus($msg->status);
            $em->flush();
            //enregistre le statut sur mon channel
            $this->channelUsers[$msg->senderSlug]["status"] = $msg->status;

            $myFriends = $page->getRelations()->getChatContacts();
            $status = array();
            $chatRepo = $em->getRepository(ChatMessage::class);
                
            foreach ($myFriends as $key => $friend) { //echo sprintf("\n COO -".$friend->getSlug()."-");
                if($friend->getType() != Page::TYPE_USER){
                    $slug = $friend->getSlug();

                    //si le channel n'existe pas, il faut le créer
                    if(!isset($this->channelUsers[$slug])){
                        $this->channelUsers[$slug] = array( "token"    => "no-token", //no need
                                                            "senderUsername" => "no-username",//no need
                                                            "channels" => array(),
                                                            "status" => "online");//new \SplObjectStorage);

                        //echo sprintf("\nCREATE CHANNEL FOR GROUP -".$msg->receiverSlug."-");
                    }

                    $this->channelUsers[$slug]["channels"][$msg->senderSlug] = new \SplObjectStorage;
                    $this->channelUsers[$slug]["channels"][$msg->senderSlug]->attach($from);
                    //echo sprintf("\nUPDATED GROUP CONNECTION -".$msg->senderSlug."- IN CHANNEL -".$slug."-");
                }else{
                    //on envoie un message à tous mes amis pour leur dire que je suis connecté
                    $this->broadcastChangeStatus($msg->senderSlug, $friend->getSlug(), $msg->status);
                }

                //si le friend n'est pas connecté, il est "offline"
                //sinon on prend son status actuel
                $friendStatus = isset($this->channelUsers[$friend->getSlug()]) ? 
                                    $this->channelUsers[$friend->getSlug()]["status"] : "offline";

                $status[$friend->getSlug()]["status"] = $friendStatus;

                //récupère les messages non-lus (reçu quand senderSlug était déconnecté)
                $messages = $chatRepo->findBy(array("senderSlug" => $friend->getSlug(), 
                                                    "receiverSlug" => $msg->senderSlug,
                                                    "toRead" => true));
                //envoie le nombre de messages non-lus
                $status[$friend->getSlug()]["msgToRead"] = count($messages);
            }

            $from->send(json_encode(array("action"=>"status", 
                                          "status" => $status)));
        }
        /*******************************
            ACTION 2 : sendMessage
        ********************************/
        elseif($msg->action == "sendMessage"){
            $msg->created = new \DateTime();
            $msg->createdDisplay = $msg->created->format('d/m · H')."h".$msg->created->format('i');

            $em = $this->container->get('doctrine_mongodb')->getManager();
            $chatMsg = new ChatMessage($msg);
            $em->persist($chatMsg);
            $em->flush();

            $msg->id = $chatMsg->getId();
            $msg->message = Helper::linkToHtml($msg->message);

            if($this->channelUsers[$msg->senderSlug]["token"] != $msg->senderToken){
                    //echo sprintf("\nERROR TOKEN 3 : ".$msg->senderToken. ', '.
                                                     //$this->channelUsers[$msg->senderSlug]. ', '.
                                                     //$this->channelUsers[$msg->senderSlug]["token"]);
                    return;
            }
            
            if($msg->receiverType == Page::TYPE_USER){
                //si le channel du receveur exist (si le receveur est connecté au chat)
                if(isset($this->channelUsers[$msg->receiverSlug])){
                    foreach ($this->channelUsers[$msg->receiverSlug]["channel"] as $key => $chanel) {

                        //echo sprintf("\nSEND MSG TO CHANNEL -".$this->channelUsers[$msg->receiverSlug]["senderUsername"]."- ".
                        //             "chanel : ".$chanel->resourceId. " - msg : ".$msg->message);

                        //BROADCAST USER RECEIVER (all connection of same user)
                        $chanel->send(json_encode($msg));
                        $this->checkIsMyContact($msg);
                    }

                    //BROADCAST USER SENDER (all connection of same user)
                    foreach ($this->channelUsers[$msg->senderSlug]["channel"] as $key => $chanel) {
                        //dd($msg);
                        $chanel->send(json_encode($msg));
                    }
                }else{
                    //BROADCAST USER SENDER (all connection of same user)
                    foreach ($this->channelUsers[$msg->senderSlug]["channel"] as $key => $chanel) {
                        $chanel->send(json_encode($msg));
                    }
                    //echo sprintf("\nthe receiver -".$msg->receiverSlug."- is not connected, this msg will be save in db");
                }
            }
            elseif($msg->receiverType != Page::TYPE_USER){
                //si un user essaie d'envoyer un message à une Page de type "group" (!= user) 
                //et que personne n'a ouvert le channel avant lui
                //on créé le channel de la Page
                if(!isset($this->channelUsers[$msg->receiverSlug])){
                    $this->channelUsers[$msg->receiverSlug] = array("token"    => "no-token", //no need
                                                                    "senderUsername" => "no-username",//no need
                                                                    "channels" => array(),
                                                                    "status" => "online");

                    //echo sprintf("\nCREATE CHANNEL FOR GROUP -".$msg->receiverSlug."-");
                }

                //on actualise le channel du sender (au cas où il aurait changé)
                $this->channelUsers[$msg->receiverSlug]["channels"][$msg->senderSlug] = new \SplObjectStorage;
                $this->channelUsers[$msg->receiverSlug]["channels"][$msg->senderSlug]->attach($from);
                //echo sprintf("\nUPDATE -".$msg->senderSlug."- IN CHANNEL -".$msg->receiverSlug."-");
                
                $msg->senderSlug = $msg->receiverSlug;
                //BROADCAST ALL USER REGISTRED IN GROUP CHANNEL
                foreach ($this->channelUsers[$msg->receiverSlug]["channels"] as $key => $chanels) {
                    foreach ($chanels as $k => $chan) {
                        $chan->send(json_encode($msg));
                    }
                }
                //echo sprintf("\nSEND MSG TO CHANNEL GROUP -".$msg->receiverSlug."- ".
                //            "(".count($this->channelUsers[$msg->receiverSlug]["channels"]).") ".
                //            "- msg : ".$msg->message);
            }

        }
        /*******************************
            ACTION 3 : changeStatus
        ********************************/
        elseif($msg->action == "changeStatus"){
            $this->saveChangeStatus($msg->slug, $msg->status);
        }
        /*******************************
            ACTION 4 : broadcastNotification
        ********************************/
        elseif($msg->action == "broadcastNotification"){
            $this->broadcastNotification(@$msg->notif->id, @$msg->notif->html, @$msg->whatHtml, 
                                         @$msg->notif->whatAnswerId, @$msg->senderSlug, @$msg->notif->newTotal);
        }
        /*******************************
            ACTION 5 : getCountUserOnline
        ********************************/
        elseif($msg->action == "getCountUserOnline"){

            //echo sprintf("\n action getCountUserOnline ^^^^^^^^^^^^^^^^^^");
            foreach ($this->channelUsers[$msg->senderSlug]["channel"] as $key => $chanel) {
                $chanel->send(json_encode(array( "action"=>"getCountUserOnline", 
                                               "countUserOnline" => count($this->usersOnline)
                                            )));
            }
        }
        /*******************************
            ACTION 6 : keepAlive
        ********************************/
        elseif($msg->action == "keepAlive"){
            $date = new \DateTime('NOW');
            //echo sprintf("\nkeepAlive - ". $msg->senderSlug. " - ".$date->format("H:i:s"));
            $em = $this->container->get('doctrine_mongodb')->getManager();
            $pageRepo = $em->getRepository(Page::class);
            $page = $pageRepo->findOneBy(array("slug" => $msg->senderSlug));
            $page->setDateOnline(new \Datetime());
            $em->flush($page);
        }
        else{
            //echo sprintf("\nno action found");
        }


        //echo sprintf("\n===============");
    }

    public function onClose(ConnectionInterface $conn) {

        //echo sprintf("\n-------------------- onClose ");
        // The connection is closed, remove it, as we can no longer send it messages
        //$this->clients->detach($conn);
        $mySlug = "";
        foreach ($this->channelUsers as $key => $channels) { 
            //echo "\ngo to each channel ** ".$key;
            if(isset($channels["channel"]))
            foreach ($channels["channel"] as $keyC => $channel) { 
                //dump($channel);
                //echo "\nclose each channel ** keyC:".$keyC." /\ conn:".$conn->resourceId." / conn:".$channel->resourceId;
               //$test = $channel["channel"]->resourceId;

                if ($conn->resourceId == $channel->resourceId) {
                    //echo sprintf("\n∕∕ DETACH ** ".$key);
                    $this->channelUsers[$key]["channel"]->detach($conn);
                    unset($this->channelUsers[$key]);
                    unset($this->usersOnline[$key]);
                    //echo sprintf("\n∕∕ UNSET ** ".$key);
                    $mySlug = $key;
                }
            }
        }

        //echo sprintf("\n change status to OFFLINE for channel  -".$mySlug."-");

        //enregistre le statut sur mon channel
        //$this->channelUsers[$mySlug]["status"] = "offline";
        
        $em = $this->container->get('doctrine_mongodb')->getManager();
        $pageRepo = $em->getRepository(Page::class);
        $page = $pageRepo->findOneBy(array("slug" => $mySlug));
        if($page != null) $myFriends = $page->getRelations()->getChatContacts();

        //echo sprintf("\BROADCAST NEW STATUS OFFLINE TO MY FRIENDS");
        foreach ($myFriends as $key => $friend) {
            if($friend->getType() == Page::TYPE_USER)
            $this->broadcastChangeStatus($mySlug, $friend->getSlug(), "offline");
        }

        //clear Page::chatToken
        //echo "\n Connection {$conn->resourceId} has disconnected\n";
    }

    public function broadcastChangeStatus($mySlug, $myFriendSlug, $myNewStatus){
        if(isset($this->channelUsers[$myFriendSlug])){
            foreach ($this->channelUsers[$myFriendSlug]["channel"] as $k1 => $chan) {
                $chan->send(json_encode(array("action"=>"friendChangeStatus", 
                                               "slug" => $mySlug,
                                               "status"=>$myNewStatus)));
            }
        }
        //echo sprintf("\nBROADCAST MY NEW STATUS ** ** of -".$mySlug."-  to -".$myFriendSlug."-");
    }

    public function saveChangeStatus($mySlug, $myNewStatus){
        //récupère la page en db
        //TODO : check token !
        $em = $this->container->get('doctrine_mongodb')->getManager();
        $pageRepo = $em->getRepository(Page::class);
        $page = $pageRepo->findOneBy(array("slug" => $mySlug));

        //enregistre le status en db
        $page->setChatStatus($myNewStatus);

        $em->flush();
        //enregistre le statut sur mon channel
        $this->channelUsers[$mySlug]["status"] = $myNewStatus;

        $myFriends = $page->getRelations()->getChatContacts();
        //echo sprintf("\nSAVE NEW STATUS ** ** of -".$mySlug."-");
        foreach ($myFriends as $key => $friend) {
            if($friend->getType() == Page::TYPE_USER)
                $this->broadcastChangeStatus($mySlug, $friend->getSlug(), $myNewStatus);
            //else
            //    echo sprintf("\nNOT USER -".$friend->getType()."-");
         
        }
    }

    public function checkIsMyContact($msg){
        //echo sprintf("\nCHECK IS MY CONTACT ** ** of -".$msg->senderSlug."- && ".$msg->receiverSlug);
        $em = $this->container->get('doctrine_mongodb')->getManager();
        $pageRepo = $em->getRepository(Page::class);
        $myPage = $pageRepo->findOneBy(array("slug" => $msg->senderSlug));
        $distPage = $pageRepo->findOneBy(array("slug" => $msg->receiverSlug));
        $myPage->getRelations()->addChatContacts($distPage);
        $distPage->getRelations()->addChatContacts($myPage);
    }

    public function sendNotif(){

        //echo sprintf("\nSEND NOTIF ?".sizeof($this->channelUsers));
        foreach ($this->channelUsers as $key => $value) {
            //echo sprintf("\nSEND NOTIF OK - ".$key);
        }
        //echo sprintf("\nSEND NOTIF OKK");
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }

    public function broadcastNotification($idNotif, $html, $whatHtml, $whatAnswerId, $senderSlug, $newTotal=null){
        //echo sprintf("\nbroadcastNotification ? id - ".$idNotif." - NAME ? ");

        $em = $this->container->get('doctrine_mongodb')->getManager();
        $em->clear(); //very important : clear cache before new request
        $pageRepo = $em->getRepository(Page::class);
        $pageSender = $pageRepo->findOneBy(array("slug" => $senderSlug));

        $notifRepo = $em->getRepository(Notification::class);
        $notif = $notifRepo->findOneBy(array("id" => $idNotif));

        //dd($pageSender);
        //if($notif->getAboutType() != "comment"){
            $objRepo = $em->getRepository($notif->getClass($notif->getAboutType()));
            $notifAboutObj = $objRepo->findOneBy(array("id" => $notif->getAboutId()));
        // }else{
        //     $csRepo = $em->getRepository(CommentStream::class);
        //     $cs = $csRepo->findOneBy(array('parentId' => $notif->getAboutId(),
        //                                    'parentType' => $notif->getAboutIType()) 
        //                                   );
            
        //     $notifAboutObj = $cs->getCommentById($aboutId);
        // }

        $notifJson = $notif->getJson($pageSender);
        
        //dd($notifAboutObj);
        if($notif->getAboutType() == "page")
            $notifJson["aboutObj"] = $notifAboutObj->getJson($pageSender);
        else
            $notifJson["aboutObj"] = $notifAboutObj->getJson();
        
        $notifJson["newTotal"] = $newTotal;

        //set whatObject before to send notifications to the view
        
        //echo sprintf("\nhas target ? ".count($notif->getTargets())." - ");
        //dump(!empty($notif->getTargets()));
       
        if(!empty($notif->getTargets())){
        foreach ($notif->getTargets() as $key => $target) {
            //echo sprintf("\n *** TARGET ? ***");
            //dump($target); 
            //echo sprintf("\n ");
            $targetSlug = $target->getSlug();
            //echo sprintf("\nbroadcast this notification ? senderSlug - ".$senderSlug." ||| targetSlug - ".$targetSlug);
            if(isset($this->channelUsers[$targetSlug]) && $targetSlug != $senderSlug){
                foreach ($this->channelUsers[$targetSlug]["channel"] as $k1 => $chan) {
                    $chan->send(json_encode(array( "action"=>"receiveNotification", 
                                                   "notif" => $notifJson,
                                                   "html" => $html,
                                                   "whatHtml" => @$whatHtml,
                                                   "whatAnswerId" => $whatAnswerId)));
                    //echo sprintf("\nbroadcast this notification ? k1 - ".$k1." targetSlug - ".$targetSlug);
                }
            }
        }}

        //for friends request, we dont need to keep the notif if db after broadcasted
        if( $notif->getVerb() == Notification::VERB_REQUEST_FRIEND ||
            $notif->getVerb() == Notification::VERB_REQUEST_ADMIN){
            $em->remove($notif);
            $em->flush();
        }
    }

}