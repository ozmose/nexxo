<?php

namespace App\Services;
use Symfony\Component\Translation\Translator;
use Symfony\Component\DomCrawler\Crawler;

use App\Entity\Page;

/**
 * Class Helper
 */
class InteropTranslator
{
    public static function getClientMap($clientKey) {

    	$clientMap = array(
    					"communecter" => array(
    						"name" 			=> array("name"),
    						"streetAddress" => array("address", "streetAddress"),
    						"city" 			=> array("address", "addressLocality"),
    						"country" 		=> array("address", "addressCountry"),
    						"coordinates" 	=> array("geo"),
    						 )
    					);

    	return @$clientMap[$clientKey] ? $clientMap[$clientKey] : array();
    }


    public static function translate($sourceKey, $data, $type) {

    	$newPage = new Page();
    	$newPage->setId(rand(1000, 100000));
    	$newPage->setSlug("noslug");
    	$newPage->setIsActive(true);
    	$newPage->setConfidentialityByKey("SHOW_ADDRESS", "CONF_NEXXO");
    	$newPage->setConfidentialityByKey("SHOW_GEOPOS", "CONF_NEXXO");
    	$newPage->setType($type);
    	$newPage->setSourceKey($sourceKey);

    	$map = InteropTranslator::getClientMap($sourceKey);

    	//dump($data);
    	if($sourceKey == "communecter"){
    		if(true 
    			//&& @$data->shortDescription && $data->shortDescription != "" //keep data only when it is a geolocated data
    			//&& @$data->image //keep data only when it is a geolocated data
    			){

	       		if(@$data->name)
	    			$newPage->setName($data->name);

	    		if(@$data->shortDescription)
	    			$newPage->setDescription($data->shortDescription);
	    		
	    		if(@$data->address->streetAddress)
	    			$newPage->setStreetAddress($data->address->streetAddress);
	    		
	    		if(@$data->address->addressLocality)
	    			$newPage->setCity($data->address->addressLocality);
	    		
	    		if(@$data->address->addressCountry)
	    			$newPage->setCountry($data->address->addressCountry);
	    		
	    		if(@$data->geo)
	    			$newPage->setCoordinates(array( $data->geo->longitude, $data->geo->latitude ));

	    		if(@$data->tags){
	    			$newPage->setTags($data->tags);
	    			$newPage->setTagsStrFromArray($data->tags);
	    		}

	    		if(@$data->image){
	    			$newPage->setImageBanner($data->image);
	    			$newPage->setImageProfil($data->image);
	    		}


	    		if(@$data->url->communecter){
	    			$infos = $newPage->getInformations();
	    			$infos->setCommunecter($data->url->communecter);
	    			$newPage->setInformations($infos);
	    		}
	    		if(@$data->url->website){
	    			$infos = $newPage->getInformations();
	    			$website = $data->url->website;
	    			if( !preg_match('/http(s?)\:\/\//i', $website) ) {
					    // URL does NOT contain http:// or https://
					    $website = "http://".$website;
					}
	    			$infos->setWebsite($website);
	    			$newPage->setInformations($infos);
	    		}

	    	}else{
	    		return null;
	    	}
	    }elseif($sourceKey == "geosse"){
			if(true 
			//&& @$data->shortDescription && $data->shortDescription != "" //keep data only when it is a geolocated data
			//&& @$data->image //keep data only when it is a geolocated data
			){
				if(@$data->geometry)
    				$newPage->setCoordinates($data->geometry->coordinates);

	       		//dump("name ");// . $data->properties->name);
	    		if(@$data->properties->name)
	    			$newPage->setName($data->properties->name);

	    		if(@$data->properties->description)
	    			$newPage->setDescription($data->properties->description);
	    
	    		if(@$data->properties->url){
	    			$infos = $newPage->getInformations();
	    			$website = $data->properties->url;
	    			if( !preg_match('/http(s?)\:\/\//i', $website) ) {
					    // URL does NOT contain http:// or https://
					    $website = "http://".$website;
					}
	    			$infos->setWebsite($website);
	    			$newPage->setInformations($infos);
	    		}
	    	}else{
	    		return null;
	    	}

    	}elseif($sourceKey == "presdecheznous"){

    		$website = @$data->showUrl ? @$data->showUrl : @$data->website ? @$data->website : "";
	    		
			if(true 
			&& (@$data->abstract || @$data->description) && $data->geo != "" //keep data only when it is a geolocated data
			&& $website != ""
			//&& @$data->image //keep data only when it is a geolocated data
			){
				if(@$data->geo)
	    			$newPage->setCoordinates(array( $data->geo->longitude, $data->geo->latitude ));

	       		//dump("name ");// . $data->properties->name);
	    		if(@$data->name)
	    			$newPage->setName($data->name);

	    		if(@$data->description)
	    			$newPage->setDescription($data->description);
	    
	    		if(@$data->address->streetAddress)
	    			$newPage->setStreetAddress($data->address->streetAddress);
	    		
	    		if(@$data->address->addressLocality)
	    			$newPage->setCity($data->address->addressLocality);
	    		
	    		if(@$data->address->addressCountry)
	    			$newPage->setCountry($data->address->addressCountry);
	    		
		    	if(@$data->sourceKey)
			    	$newPage->setSourceKey($data->sourceKey);

			    if(@$data->images){
	    			//$newPage->setImageBanner($data->images[0]);
	    			$newPage->setImageProfil($data->images[0]);
	    		}

	    		if($website != ""){
	    			$infos = $newPage->getInformations();
	    			//$website = $url;
	    			if( !preg_match('/http(s?)\:\/\//i', $website) ) {
					    // URL does NOT contain http:// or https://
					    $website = "http://".$website;
					}
	    			$infos->setWebsite($website);
	    			$newPage->setInformations($infos);
	    		}
	    	}else{
	    		return null;
	    	}

    	}else{
    		return null;
    	}
    	
		//dump($newPage);
    	return $newPage;
    }

}