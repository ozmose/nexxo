<?php
namespace App\Repository;
 
use Doctrine\ORM\EntityRepository;
use Doctrine\ODM\MongoDB\DocumentRepository;

use App\Services\Helper;
 
class ContactRepository extends DocumentRepository
{
    public function get()
    {

    	return $this->findBy(array("scope" => "P1W"), array("count" => "DESC"), 10);
    }

}
