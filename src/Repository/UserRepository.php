<?php
// src/Repository/UserRepository.php
namespace App\Repository;
 
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ODM\MongoDB\DocumentRepository;

use App\Services\Helper;
 
class UserRepository extends DocumentRepository implements UserLoaderInterface
{
    public function loadUserByUsername($username)
    {
        return $this->createQueryBuilder()
            ->field('email')->equals((string) $username)
            ->getQuery();
    }

    public function loadUserByRealUsername($username)
    {
        return $this->createQueryBuilder()
            ->field('username')->equals((string) $username)
            ->getQuery();
    }


    public function getLastCreated(){
        return $this->createQueryBuilder()
            ->field('isActive')->equals(true)
            ->sort('created', 'desc')
            ->limit(40) 
            ->getQuery();
    }


    public function getFullSearch($searchStr){
        $searchRegExp = Helper::accentToRegex($searchStr);
        return $this->createQueryBuilder()
            ->field('isActive')->equals(true)
            ->field('username')->equals(new \MongoRegex("/.*{$searchRegExp}.*/i"))
            ->sort('created', 'desc')
            ->limit(40) 
            ->getQuery();
    }
}
