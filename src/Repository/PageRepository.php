<?php
namespace App\Repository;
 
use Doctrine\ORM\EntityRepository;
use Doctrine\ODM\MongoDB\DocumentRepository;

use App\Services\Helper;
use App\Entity\Page;
use App\Entity\Report;
 
class PageRepository extends DocumentRepository
{
   	
    public function getLastCreated(){
        return $this->createQueryBuilder()
            //->field('isActive')->equals(true)
            ->sort('created', 'desc')
            ->limit(40) 
            ->getQuery();
    }


    public function getFriendSuggest($myFriends, $myId, $position){ //dd($myFriends);
        $aroundKm = 50;
        $res = array(); $i=0;
        //search for local suggestion (max 50*2*2*2 = 400km)
        while(empty($res) && $i < 3){ $i++;
            $request = array( '$and' => array(array('id' => array('$nin' => $myFriends)),
                                              array('id' => array('$ne' => $myId))),
                                              'type' => Page::TYPE_USER);
            
            if($position[0] != "false" && $position[1] != "false")
                $request["coordinates"] = array('$geoWithin' => 
                                            array('$centerSphere'=> 
                                                array(array($position[0], 
                                                            $position[1]), $aroundKm/6378)) );

            $request['isPrivate'] = array('$ne' => true);
            $request['isActive'] = true;

            $res = $this->findBy($request, array("created" => "DESC"), 10);
            $aroundKm = $aroundKm*2;
        }
        //if no result, search for recent suggestions
        if(empty($res) || sizeof($res) < 5){
            $res2 = $this->findBy(array( '$and' => array(array('id' => array('$nin' => $myFriends)),
                                                        array('id' => array('$ne' => $myId))),
                                        'type' => Page::TYPE_USER,
                                        'isPrivate' => array('$ne' => true)

                                        ), array("created" => "DESC"), 5);
            $res = array_merge($res, $res2);
        }
        return $res;
    }
    
    public function getPageSuggest($myFriends, $myId, $position, $types=false){ //dd($myFriends);   
        $aroundKm = 200;
        $res = array(); $i=0;
        //search for local suggestion (max 50*2*2*2 = 400km)
        while(empty($res) && $i < 3){ $i++;
            $request = array( '$and' => array(array('id' => array('$nin' => $myFriends)),
                                              array('id' => array('$ne' => $myId))),
                                              'type' => array('$nin' =>  array(Page::TYPE_USER, Page::TYPE_AGORA)),
                                        );

            if($position[0] != "false" && $position[1] != "false")
                $request["coordinates"] = array('$geoWithin' => 
                                            array('$centerSphere'=> 
                                                array(array($position[0], 
                                                            $position[1]), $aroundKm/6378)) );

            $request['isPrivate'] = array('$ne' => true);
            $request['isActive'] = true;

            //filter by type
            if($types != false){
                $request['type'] = $types; //types must be an array
            }
            
            $res = $this->findBy($request, array("created" => "DESC"), 10);
            
            $aroundKm = $aroundKm*2;
        }
        
        //if no result, search for recent suggestions
        if(empty($res) || sizeof($res) < 5){
            $request = array( '$and' => array(array('id' => array('$nin' => $myFriends)),
                                              array('id' => array('$ne' => $myId))),
                                'type' => array('$nin' =>  array(Page::TYPE_USER, Page::TYPE_AGORA)),
                                'isPrivate' => array('$ne' => true),
                                'isActive' => true);
            //filter by type
            if($types != false){
                $request['type'] = $types; //array('$in' => $types); //types must be an array
            }

            $res2 = $this->findBy($request, array("created" => "DESC"), 5);

            $res = array_merge($res, $res2);
        }

        //dump($request);
        return $res;
    }
    
    public function getPageReported($position){ //dd($myFriends);   
        $aroundKm = 200;
        $res = array(); $i=0;
        //search for local suggestion (max 50*2*2*2 = 400km)
        while(count($res) < 5 && $i < 3){ $i++;
            if($position[0] != "false" && $position[1] != "false")
                $request["coordinates"] = array('$geoWithin' => 
                                            array('$centerSphere'=> 
                                                array(array($position[0], 
                                                            $position[1]), $aroundKm/6378)) );

            $request['report'] = array('$exists' => true);
            
            $res = $this->findBy($request, array("created" => "DESC"), 10);
            foreach ($res as $key => $page) {
                $report = $page->getReport();
                if($report != null && $report->getStatus() != Report::STATUS_VOTE_OPEN){
                    unset($res[$key]);
                }
            }

            $aroundKm = $aroundKm*2;
        }

        
        return $res;
    }

    public function getFullSearch($searchStr){
        $searchRegExp = Helper::accentToRegex($searchStr);
        $query = $this->createQueryBuilder()
                ->field('isActive')->equals(true)
                ->field('name')->equals(new \MongoRegex("/.*{$searchRegExp}.*/i"))
                ->sort('created', 'desc')
                ->limit(200) 
                ->getQuery();

        return $query;

    }

    public function getGeoSearch($searchStr, $aroundCoordinates, $aroundKm){
        $searchRegExp = Helper::accentToRegex($searchStr);
        dd($searchStr);
        $res = $this->findBy(array('coordinates' => 
                                    array('$geoWithin' => 
                                        array('$centerSphere'=> 
                                            array(array($aroundCoordinates[1], 
                                                        $aroundCoordinates[0]), $aroundKm/6378)) ),
                                    'name' => new \MongoRegex("/.*{$searchRegExp}.*/i"),
                                    'isPrivate' => array('$ne' => true)
                                    )
                            );

       
        return $res;

    }

    public function getGlobalSearch($searchStr, $types, $aroundCoordinates, 
                                    $aroundKm, $startDate="false", $endDate="false", $myAgenda=false, $myEventsLinks=array()){
        $request = array();
        
        if(!empty($aroundCoordinates)){
            $request['coordinates'] = array('$geoWithin' => 
                                        array('$centerSphere'=> 
                                            array(array($aroundCoordinates[1], 
                                                        $aroundCoordinates[0]), $aroundKm/6378)) );
        }
        
        $limit = 100;
        if(!empty($searchStr)){
            $limit = 500;
            $searchRegExp = Helper::accentToRegex($searchStr);
            $searchRegExpTag = Helper::accentToRegex($searchStr);
            //$request['name'] = new \MongoRegex("/.*{$searchRegExp}.*/i");
           // $request['tags'] = array('$in'=>array(new \MongoRegex("/.*{$searchRegExp}.*/i")));
            $request['$or'] = array(array('name' => new \MongoRegex("/.*{$searchRegExp}.*/i")),
                                    array('tags' => array('$in'=>array(new \MongoRegex("/.*{$searchRegExp}.*/i")))));

        }

        //if(empty($types)) $types = Page::getTypeMainSearchSimple();

        if(!empty($types)){
            $typesArray = explode(",", $types);
        }else{
            $typesArray = Page::getTypeMainSearchSimple();
        }
        
        $request['type'] = array('$in' => $typesArray);

        if($startDate != "false" && $endDate != "false"){
            if(!isset($request['$or'])) $request['$or'] = array();
            $request['$or'] = array(array("startDate" => 
                                            array('$gte' => new \Datetime($startDate),
                                                  '$lte' => new \Datetime($endDate)))
                                            ,
                                    array("endDate" => 
                                            array('$gte' => new \Datetime($startDate),
                                                  '$lte' => new \Datetime($endDate))
                                            ,
                                        ),
                                    array("startDate" => 
                                            array('$lte' => new \Datetime($endDate)),
                                          "endDate" => 
                                            array('$gte' => new \Datetime($startDate))
                                            
                                        )
                                    );
            //dd($request);
        
        }

        //dd($participates);$groupsId  = $this->objsToId($page->getInGroups());
        if($myAgenda == true)
            $request['id'] = array('$in' => $myEventsLinks);


        $request['isPrivate'] = array('$ne' => true);
        $request['isActive'] = true;

        //dump($request);
        $totalCount = $this->findBy($request);
        $res = $this->findBy($request, array("created" => "DESC"), $limit);
        return array("res" => $res, "totalCount" => count($totalCount));
    }


    public function getEvents($searchStr, $startDate, $endDate, $aroundCoordinates=false, $aroundKm=0){
        $request = array();
        
        if($aroundCoordinates != false){
            $request['coordinates'] = array('$geoWithin' => 
                                        array('$centerSphere'=> 
                                            array(array($aroundCoordinates[1], 
                                                        $aroundCoordinates[0]), $aroundKm/6378)) );
        }
        
        if(!empty($searchStr) && $searchStr != "-"){
            $searchRegExp = Helper::accentToRegex($searchStr);
            $request['name'] = new \MongoRegex("/.*{$searchRegExp}.*/i");
        }

        $request['type'] = "event";
        
        $request['startDate'] = array('$gte' => new \Datetime($startDate),
                                      '$lte' => new \Datetime($endDate));

        $request['endDate'] =array('$gte' => new \Datetime($startDate),
                                   '$lte' => new \Datetime($endDate));

        $request['isPrivate'] = array('$ne' => true);
        $request['isActive'] = true;

        $res = $this->findBy($request, array("created" => "DESC"), 200);
        return $res;
    }


    public function getCountPage($type, $position=false, $aroundKm=0){ //dd($myFriends);
        $res = array();
        $request = array();
        
        if($type != "")
        $request = array('type' => $type);
        
        $request['isActive'] = true;
        
        if($position != false && $position[0] != "false" && $position[1] != "false")
            $request["coordinates"] = array('$geoWithin' => 
                                        array('$centerSphere'=> 
                                            array(array($position[0], 
                                                        $position[1]), $aroundKm/6378)) );

        $res = count($this->findBy($request));
        
        return $res;
    }

    public function getCountUserActive(){ //dd($myFriends);
        $request = array('type' => Page::TYPE_USER,
                         'isActive' => true);
        $res = count($this->findBy($request));
        return $res;
    }

    public function getCountFirstStep(){ //dd($myFriends);
        $request = array('type' => Page::TYPE_USER,
                         'firstStep' => array('$ne'=>0));
        $res = $this->findBy($request);
        return $res;
    }

}
