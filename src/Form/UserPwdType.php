<?php
namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Translation\TranslatorInterface;

class UserPwdType extends AbstractType
{

    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
       
        $builder
            ->add('password', PasswordType::class, array(
                //'type' => PasswordType::class,
                'required'   => true,
                'label' => $this->translator->trans('Current password', array(), "form"),
                //'second_options' => array('label' => 'Confirmer le mot de passe'),
            ));
    
        $builder
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'required'   => true,
                'first_options'  => array('label' => $this->translator->trans('New password', array(), "form")),
                'second_options' => array('label' => $this->translator->trans('Confirm new password', array(), "form")),
            ));
        

        //$builder->add('terms', CheckboxType::class, array('property_path' => 'termsAccepted'));
        //https://symfony.com/doc/master/bundles/DoctrineMongoDBBundle/form.html
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
        ));
    }
}
