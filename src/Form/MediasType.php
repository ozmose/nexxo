<?php
namespace App\Form;

use App\Entity\User;
use App\Entity\Page;
use App\Entity\Group;
use App\Entity\Medias;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
// use Symfony\Component\Form\Extension\Core\Type\BoolType;

class MediasType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title',          TextType::class,    ['required'   => true]) 
            ->add('description',    TextType::class,    ['required'   => true]) 
            ->add('url',            TextType::class,    ['required'   => true]) 
            ->add('image',          TextType::class,    ['required'   => true]) ;
            //->add('preview',        RadioType::class, ['required'   => true]);       
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            "allow_extra_fields" => true
            //'data_class' => Medias::class,
        ));
    }

}
