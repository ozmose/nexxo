<?php
namespace App\Form;

use App\Entity\User;
use App\Entity\Page;
use App\Services\Helper;
use App\Services\Country;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Translation\TranslatorInterface;

use Doctrine\Bundle\MongoDBBundle\Form\Type\DocumentType;

class PageType extends AbstractType
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {   //dd($options["attr"]["isAdminOf"]);
        
        $builder
            ->add('name', TextType::class, [
                'label' => $this->translator->trans('Name of your page', array(), "form"),
                'required'   => true ]);

        $builder
            ->add('tagsStr', TextType::class, [
                'label' => $this->translator->trans('#tags', array(), "form"),
                'required'   => false ]);

        if(@$options["pageType"] != Page::TYPE_USER)
            $builder    
            ->add('parentPage', DocumentType::class, array(
                                'class' => Page::class,
                                'label' => $this->translator->trans("Who is the owner of this page ?", array(), "form"),
                                'choice_label' => 'name',
                                'choices' => $options["adminPage"],
                            ));

        $builder    
            ->add('isPrivate', CheckboxType::class, array(
                            'required'   => false
                            ));

        if(@$options["pageType"] == Page::TYPE_EVENT){
            $builder
            ->add('startDate', DateTimeType::class, [
                  'widget' => 'single_text',
                  'html5' => false,
                  'attr' => ['class' => 'js-datepicker'],
                  'label' => $this->translator->trans('Start date', array(), "form"),
                  'required'   => true ])

            ->add('endDate', DateTimeType::class, [
                  'widget' => 'single_text',
                  'html5' => false,
                  'attr' => ['class' => 'js-datepicker'],
                  'label' => $this->translator->trans('End date', array(), "form"),
                  'required'   => false ]);
        }

        if(!@$options["pageType"] || 
             $options["pageType"] != Page::TYPE_USER && @$options["pageType"] != Page::TYPE_EVENT)
        {
            $builder->add('type', ChoiceType::class, [
                    'label' => $this->translator->trans('What kind of structure does your page represent ?', array(), "form"),
                    'choices'  => array(
                        $this->translator->trans('Freegroup',    array(), "page") => "freegroup",
                        $this->translator->trans('Project',      array(), "page") => "project",
                        $this->translator->trans('Association',  array(), "page") => "association",
                        $this->translator->trans('Business',     array(), "page") => "business",
                        $this->translator->trans('Agora',        array(), "page") => "agora",
                        ),
                    'required'   => true  ]);

        }elseif(@$options["pageType"] == Page::TYPE_EVENT){
            $builder->add('type', ChoiceType::class, [
                    'choices'  => array( 'Event'         => 'event'),
                    'required'   => true  ]);
        }

        $builder
            ->add('description', TextareaType::class, [
                'label' => $this->translator->trans('Main description', array(), "form"),
                'required'   => false
            ])
            ->add('country', ChoiceType::class, [
                'label' =>  $this->translator->trans('Choose a country', array(), "form"),
                'choices'  => Country::getCountryList(),
                'data' => 'FR',
                'preferred_choices' => array('France'        => 'FR',
                                             'Belgique'      => 'BE',
                                             'Germany'       => 'DE',
                                             'Italia'        => 'IT',
                                             'España'        => 'ES',
                                             'United Kingdom'=> 'GB',
                                             'Switzerland'   => 'CH'),
                'required'   => false  ])

            ->add('city', TextType::class, [
                'label' =>  $this->translator->trans('City / Town', array(), "form"),
                'required'   => false
            ])
            ->add('streetAddress', TextType::class, [
                'label' =>  $this->translator->trans('Street', array(), "form"),
                'required'   => false
            ])
            ->add('latitude', HiddenType::class)
            ->add('longitude', HiddenType::class)

            ;

            //dd($builder);
       
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // uncomment if you want to bind to a class
            'data_class' => Page::class,
            'pageType'=>'user',
            'adminPage'=>array()
        ]);
    }
}
