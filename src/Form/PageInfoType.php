<?php
namespace App\Form;

use App\Entity\User;
use App\Entity\Page;
use App\Entity\PageInfo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Translation\TranslatorInterface;

class PageInfoType extends AbstractType
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', TextType::class, [
                'label' => $this->translator->trans('E-mail', array(), "form"),
                'required'   => false ])

            ->add('website', TextType::class, [
                'label' => $this->translator->trans('Website', array(), "form"),
                'required'   => false ])

            ->add('communecter', TextType::class, [
                'label' => $this->translator->trans('Communecter', array(), "form"),
                'required'   => false ])

            ->add('facebook', TextType::class, [
                'label' => $this->translator->trans('Facebook', array(), "form"),
                'required'   => false ])

            ->add('twitter', TextType::class, [
                'label' => $this->translator->trans('Twitter', array(), "form"),
                'required'   => false ])

            ->add('youtube', TextType::class, [
                'label' => $this->translator->trans('Youtube', array(), "form"),
                'required'   => false ])

            ->add('discord', TextType::class, [
                'label' => $this->translator->trans('Discord', array(), "form"),
                'required'   => false ]);

       
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            //'data_class' => PageInfo::class,
            //"csrf_protection" => false,
            "allow_extra_fields" => false
        ));
    }

}
