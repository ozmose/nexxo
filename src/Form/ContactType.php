<?php
namespace App\Form;

use App\Entity\User;
use App\Entity\Page;
use App\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Translation\TranslatorInterface;

use Doctrine\Bundle\MongoDBBundle\Form\Type\DocumentType;

use Doctrine\ODM\MongoDB\DocumentRepository;

class ContactType extends AbstractType
{

    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	//$translator = new Translator();
		$builder
            ->add('object', TextType::class, [
                  'label' => $this->translator->trans("What's about ?", array(), "contact"),
                  'required'   => false ])

            ->add('text', TextareaType::class, [
                  'label' => $this->translator->trans("Your message", array(), "contact"),
                  'required'   => true ])

            ->add('type', HiddenType::class, [
                  'required'   => true ])
            ;

      
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Contact::class,
            "csrf_protection" => true,
            "allow_extra_fields" => false,
        ));
    }
}
