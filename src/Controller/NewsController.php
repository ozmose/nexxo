<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

use App\Form\NewsType;
use App\Entity\User;
use App\Entity\Page;
use App\Entity\Group;
use App\Entity\News;
use App\Entity\CommentStream;
use App\Entity\Comment;
use App\Entity\Notification;
use App\Entity\Report;
use App\Entity\Tag;
use App\Form\CommentType;

class NewsController extends Controller
{

    /**
     * @Route("/news/stream/{filter}/{idTarget}/{isDashboard}/{timestamp}", name="stream")
     */
    public function upStream($filter = "all", $idTarget, $isDashboard, $timestamp=null, AuthorizationCheckerInterface $authChecker){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $news = new News();
                
        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
        $pageTarget = $pageRepo->findOneBy(array("id" => $idTarget));

        $newsRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(News::class);


        $tags = isset($_POST["tags"]) ? $_POST["tags"] : array();
        if($isDashboard=="true"){ //dd($pageTarget->getCoordinates());
            $posts = $newsRepo->getPostsDashboard($newsRepo, $this->getUser()->getMyUserPage(), $pageTarget->getCoordinates(), $pageTarget, $filter, $tags, $timestamp);
        }elseif($isDashboard=="false"){
            $posts = $newsRepo->getPostsPage($pageTarget, $this->getUser()->getMyUserPage(), $filter, 20, $tags, $timestamp);
        }

        //inc number of view of each post
        $em = $this->get('doctrine_mongodb')->getManager();
        foreach ($posts as $key => $post) $post->incViewCount();
            $em->flush();

        $commentS = new CommentStream();
        $comment = new Comment();
        $commentForm = $this->createForm(CommentType::class, $comment);

        return $this->render('news/blocks/items-grid.html.twig', 
                             array("posts" => $posts,
                                   "filter" => $filter,
                                   "page" => $pageTarget,
                                   "dashboard" => $isDashboard,
                                   "commentForm" => false,//$commentForm->createView(),
                                   ));
    }

    /**
     * @Route("/news/quickview-post/{id}", name="news-quickview-post")
     */
    public function quickviewPost($id, Request $request, AuthorizationCheckerInterface $authChecker){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }
        
        $myPage = $this->getUser()->getMyUserPage();
        
        $newsRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(News::class);
        $news = $newsRepo->findOneBy(array("id" => $id));

        $cs = new CommentStream();
        $comment = new Comment();
        $commentForm = $this->createForm(CommentType::class, $comment);

        $showComment = ($news != null && $news->getCommentStream() === null) ? false : true;

        return $this->render('news/blocks/news-post.html.twig', 
                 array('post' => $news,
                       'page' => $myPage,
                       "showCommentBlock" => $showComment,
                       'commentForm' => $commentForm->createView()));
    }

    /**
     * @Route("/news/send-post/{idSigned}/{idTarget}", name="news-send-post")
     */
    public function sendPost($idSigned, $idTarget, Request $request, AuthorizationCheckerInterface $authChecker){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }


        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
        $page = $pageRepo->findOneBy(array("id" => $idSigned));
        $pageTarget = $pageRepo->findOneBy(array("id" => $idTarget));

        /*
            TODO : check if idSigned & idTarget are compatible 
            if they are friends ok
            if idTarget is ok to receive msg from idSigned
        */      

        $groupRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Group::class);
        
        $news = new News();
        $form = $this->createForm(NewsType::class, $news);
        $form->handleRequest($request); 

        $reqNews = $request->request->get('news');
        if(@$reqNews["medias"]){
            //dd("heu non pas de media");
            $medias = $reqNews["medias"];
            $news->setMedias($medias);
        }
        
        
        if ($form->isSubmitted() && $form->isValid()) {

            $myUserPage = $this->getUser()->getMyUserPage();
            $news->setAuthor($this->getUser()->getMyUserPage());
            $news->setSigned($page);
            $news->setTarget($pageTarget);

            if(@$reqNews["scopeGeo"]){
                $scopeGeo = $reqNews["scopeGeo"];
                $news->setScopeGeo($scopeGeo);
            }

            Tag::registerTags($news->getTags(), $this->get('doctrine_mongodb')->getManager(), true);

            $isSharable = $news->getIsSharable() == "true" ? true : false; 
           
            $news->setIsSharable($isSharable);
           
            //convert string id (of groups) to Group Objects
            if($news->getScopeGroups() != null){
                $scopeGroups = $news->getScopeGroups();
                $realScopeGroups = array();
                foreach ($scopeGroups as $key => $scopeGroup) {
                    $realScopeGroups[] = $groupRepo->findOneBy(array("id" => $scopeGroup));
                }
                $news->setScopeGroups($realScopeGroups);
            }
            
            $news->setCreated(new \Datetime());

            $data = $news->getImage();
         
            // Enregistre la news dans la bdd
            $em = $this->get('doctrine_mongodb')->getManager();
            $em->persist($news);
            $em->flush();


            //traitement des images ?
            //$data = $news->getImage();
            if($data != null){
                //echo "euh ok ";
                $date = new \Datetime();
                $dateid = $date->format('Y-m-d-H-i');
                $fileName = $dateid."_".$news->getId().'.png';

                list($type, $data) = explode(';', $data);
                list(, $data)      = explode(',', $data);
                $data = base64_decode($data);
                file_put_contents($this->getParameter('img_news_directory').'/'.$fileName, $data);
               
                $news->setImage($fileName);
            }
            $em->flush();

            // Créé le flux de commentaire
            $commentS = new CommentStream();
            $comment = new Comment();
            $commentForm = $this->createForm(CommentType::class, $comment);


            /* SEND NOTIFICATION */
            $notifObj = Notification::sendNotification($this->getUser()->getMyUserPage(), Notification::VERB_POST_NEWS,
                                                       $news->getId(), "news", null, null, $em, $this);
            $notif = array("id"=>$notifObj->getId(),
                           "html"=> $this->renderView('notification/item-notification.html.twig',
                                                      array('notif' => $notifObj)));

            $html = $this->renderView('news/blocks/news-post.html.twig', 
                    array( "post" => $news,
                           "commentForm" => $commentForm->createView()
                        ));

            return $this->json(array('error' => false,
                                     'notif' => $notif,
                                     'html' => $html));

        }
        else{

            /*$errors = array();
            foreach ($form->getErrors() as $error) {
                $errors[] = $error->getMessage();
            }

            foreach ($form->all() as $childForm) {
                if ($childForm instanceof FormInterface) {
                    if ($childErrors = $this->getErrorsFromForm($childForm)) {
                        $errors[$childForm->getName()] = $childErrors;
                    }
                }
            }

            dd($errors);*/
            
            return $this->json(array('error' => true, 
                                     'errorMsg' => 'invalid form submited',
                                     'msg' => $form->getErrors(true),
                                     'isValid' => $form->isValid()));
        }
        //dd($form->getErrors(true));
        return $this->json(array('error' => true,
        					     'errorMsg' => 'bug',
                                 'msg' => $form->getErrors(true)));
    }


    /**
     * @Route("/news/share-post/{postId}/{confirmed}", name="news-share-post")
     */
    public function sharePost($postId, $confirmed="false", Request $request, AuthorizationCheckerInterface $authChecker){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $em = $this->get('doctrine_mongodb')->getManager(); 

        $newsRepo = $em->getRepository(News::class);
        $postShared = $newsRepo->findOneBy(array("id" => $postId));
        

        if($postShared == null)
            return $this->json(array( 'error' => 'true',
                                      'errorMsg' => "Sorry, this publication no longer exists in our database. ".
                                                    "Impossible to share something that does not exist. ".
                                                    "The post will self-destruct in 3 seconds."));

        if($postShared->getIsActive() == false && $postShared->getReport()->getClosedByType() == Report::CLOSED_BY_ADMIN)
            return $this->json(array(  'error' => 'true',
                                       'errorMsg' => "Sorry, this publication has been locked by an admin. ".
                                                     "Shares are no longer accepted.".
                                                     "The post will self-destruct in 3 seconds."));

        if($postShared->getIsActive() == false && $postShared->getReport()->getClosedByType() == Report::CLOSED_BY_CO_MODERATION)
            return $this->json(array(  'error' => 'true',
                                       'errorMsg' => "Sorry, this publication has been locked by collective moderation.".
                                                     "Shares are no longer accepted. ".
                                                     "The post will self-destruct in 3 seconds."));


        if($postShared->getNewsShared() != null)
            $postShared = $postShared->getNewsShared();

        if($confirmed == "false" || $postShared->getIsActive() == false){
            $html = $this->renderView('news/modals/modal-share.html.twig', 
                    array( "post" => $postShared ));

            return $this->json(array('html' => $html));

        }else{
            $text = isset($_POST["news"]["text"]) ? $_POST["news"]["text"] : "";
            $radiusKm = isset($_POST["news"]["radiusKm"]) ? $_POST["news"]["radiusKm"] : false;
            $isSharable = isset($_POST["news"]["isSharable"]) ? $_POST["news"]["isSharable"] : false;
            $scopeGeo = isset($_POST["news"]["scopeGeo"]) ? $_POST["news"]["scopeGeo"] : false;
            $scopeType = isset($_POST["news"]["scopeType"]) ? $_POST["news"]["scopeType"] : "friends";
            $signedId = isset($_POST["news"]["signedId"]) ? $_POST["news"]["signedId"] : false;

            $myUserPage = $this->getUser()->getMyUserPage();

            $signed = $myUserPage;
            if($signedId != false){
                $pageRepo = $em->getRepository(Page::class);
                $signed = $pageRepo->findOneBy(array("id" => $signedId));
            }
            
            $post = new News();
            $post->setAuthor($myUserPage);
            $post->setText($text);
            $post->setSigned($signed);
            $post->setTarget($signed);
            $post->setScopeType($scopeType);
            $post->setIsSharable(true);

            Tag::registerTags($post->getTags(), $em, false);

            if($scopeGeo != false)
                $post->setScopeGeo($scopeGeo);
            
            if($radiusKm != false)
                $post->setRadiusKm($radiusKm);
            

            $post->setNewsShared($postShared);
            $postShared->addSharedBy($post);

            $post->setCreated(new \Datetime());

            $em = $this->get('doctrine_mongodb')->getManager();
            $em->persist($post);
            $em->flush();

            $commentS = new CommentStream();
            $comment = new Comment();
            $commentForm = $this->createForm(CommentType::class, $comment);
        
            /* SEND NOTIFICATION */
            $notifObj = Notification::sendNotification($this->getUser()->getMyUserPage(), Notification::VERB_SHARE,
                                                      $post->getId(), "news", null, null, $em, $this);
            $notif = array("id"=>$notifObj->getId(),
                           "html"=> $this->renderView('notification/item-notification.html.twig',
                                                      array('notif' => $notifObj)));

            $html = $this->renderView('news/blocks/news-post.html.twig', 
                    array( "post" => $post,
                           "commentForm" => $commentForm->createView()
                        ));

            return $this->json(array('notif' => $notif,
                                     'html' => $html));
        }
    }

    /**
     * @Route("/news/send-like/{postId}/{likeType}", name="news-send-like")
     */
    public function sendLike($postId, $likeType, Request $request, AuthorizationCheckerInterface $authChecker){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $newsRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(News::class);
        $post = $newsRepo->findOneBy(array("id" => $postId));
        
        if($post == null)
            return $this->json(array( 'error' => 'true',
                                      'errorMsg' => "Sorry, this publication no longer exists in our database. ".
                                                    "Impossible to likes something that does not exist. ".
                                                    "The post will self-destruct in 3 seconds."));

        if($post->getIsActive() == false && $post->getReport()->getClosedByType() == Report::CLOSED_BY_ADMIN)
            return $this->json(array(  'error' => 'true',
                                       'errorMsg' => "Sorry, this publication has been locked by an admin. ".
                                                     "Likes are no longer accepted.".
                                                     "The post will self-destruct in 3 seconds."));

        if($post->getIsActive() == false && $post->getReport()->getClosedByType() == Report::CLOSED_BY_CO_MODERATION)
            return $this->json(array(  'error' => 'true',
                                       'errorMsg' => "Sorry, this publication has been locked by collective moderation.".
                                                     "Likes are no longer accepted. ".
                                                     "The post will self-destruct in 3 seconds."));


        //dd($post);
        $addOk = false; $newTotal = 0;
        if($likeType == "likes"){
            $addOk = $post->addLike($this->getUser()->getMyUserPage());
            if($addOk == false){ //si j'aime déjà, je retire mon like
                $removeOk = $post->removeLike($this->getUser()->getMyUserPage());
            }
        }
        if($likeType == "dislikes"){
            $addOk = $post->addDislike($this->getUser()->getMyUserPage());
            if($addOk == false){ //si je dislike déjà, je retire mon dislike
                $removeOk = $post->removeDislike($this->getUser()->getMyUserPage());
            }
        }

        $em = $this->get('doctrine_mongodb')->getManager();
        $em->persist($post);
        $em->flush();

        if($addOk == true){ 
        /* SEND NOTIFICATION */
        $verb = $likeType == "likes" ? Notification::VERB_LIKE : Notification::VERB_DISLIKE;
        $notifObj = Notification::sendNotification($this->getUser()->getMyUserPage(), $verb,
                                                  $postId, "news", null, null, $em, $this);
        $notif = array("id"=>$notifObj->getId(),
                       "html"=> $this->renderView('notification/item-notification.html.twig',
                                                  array('notif' => $notifObj)));
        }else{
            $notif = false;
        }

        return $this->json(array('addError' => !$addOk,
                                 'newTotal' => array("likes" => count($post->getLikes()), 
                                                     "dislikes" => count($post->getDislikes())
                                                    ), 
                                 'notif' => $notif));
        
    }


    /**
     * @Route("/news/single-post/{id}", name="news-single-post")
     */
    public function singlePost($id, AuthorizationCheckerInterface $authChecker){
    	if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $em = $this->get('doctrine_mongodb')->getManager();
        $newsRepo = $em->getRepository(News::class);
        $news = $newsRepo->findOneBy(array("id" => $id));
        $news->incViewCount();
        $em->flush();

        $commentS = new CommentStream();
        $comment = new Comment();
        $commentForm = $this->createForm(CommentType::class, $comment);

        if($news->getReport() != null && $news->getReport()->getStatus() == Report::STATUS_DISABLED){
             return $this->render('news/post-disabled.html.twig', 
                     array('post' => $news));
        }else{
            //dump($commentForm->createView());
            return $this->render('news/single-post.html.twig', 
           			 array('post' => $news,
                           'commentForm' => $commentForm->createView()));
        }
    }

    /**
     * @Route("/news/edit-text/{newsId}", name="news-edit-text")
     */
    public function editText($newsId, Request $request, AuthorizationCheckerInterface $authChecker){

        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $em = $this->get('doctrine_mongodb')->getManager();

        $csRepo = $em->getRepository(News::class);
        $news = $csRepo->findOneBy(array('id' => $newsId));
        
        //if i'm not admin of the page who signed the news
        if($this->getUser()->getMyUserPage()->getRelations()->getImAdminOf($news->getSigned()->getSlug()) == false &&
           $this->getUser()->getMyUserPage()->getRelations()->getImAdminOf($news->getTarget()->getSlug()) == false){
            return $this->json(array('error' => true,
                                     'msgText' => "Sorry, you must be admin of the page to edit his news", 
                                    )
                           );
        }

        $htmlNews = "";

        $testNews = new News();
        $form = $this->createForm(NewsType::class, $testNews);
        $form->handleRequest($request); 
        if ($form->isSubmitted() && $form->isValid()) {

            if(isset($_POST["news"]["text"])){
                $news->setText($_POST["news"]["text"]);
                $em->flush($news);
                Tag::registerTags($news->getTags(), $em, false);
            }

            $htmlNews = $this->renderView('news/blocks/news-post-text.html.twig', 
                    array( "post" => $news ));
            //dd($news);
        }else{
            //dd($form->getErrors(true));
            $htmlNews = "Une erreur est survenue";

        
            //dump($form->getErrors(true));
            //dd("not valid");
        }

        //$em->flush();

        /*$verb = $likeType == "likes" ? Notification::VERB_LIKE : Notification::VERB_DISLIKE;
        $notifObj = Notification::sendNotification($this->getUser()->getMyUserPage(), $verb,
                                                  $parentId, $parentType, $commentId, "comment", $em, $this);
        
        $notifObj->setWhatObj($comment);
        $notif = array("id"=>$notifObj->getId(),
                       "html"=> ($addOk == true) ? $this->renderView('notification/item-notification.html.twig',
                                                                        array('notif' => $notifObj)) : ""
                       );*/
        

        return $this->json(array('error' => false,
                                 'newText' => $htmlNews, 
                                 //'notif' => $notif
                                 )
                           );
    }

    /**
     * @Route("/news/delete/{newsId}", name="news-delete")
     */
    public function delete($newsId, Request $request, AuthorizationCheckerInterface $authChecker){

        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $em = $this->get('doctrine_mongodb')->getManager();

        $newsRepo = $em->getRepository(News::class);
        $news = $newsRepo->findOneBy(array('id' => $newsId));
        
        $removed = false;
        if($news != null){

           if($this->getUser()->getMyUserPage()->getRelations()->getImAdminOf($news->getSigned()->getSlug()) ||
              $this->getUser()->getMyUserPage()->getRelations()->getImAdminOf($news->getTarget()->getSlug())) {
            
                $cs = $news->getCommentStream();

                $reportRepo = $em->getRepository(Report::class);
                $report = $reportRepo->findOneBy(array('aboutId' => $newsId, 'aboutType'=>'news'));
                if($report != null){
                    $em->remove($report); 
                    $em->flush();
                }

                $news->cleanRemove($em);
                
                /*$notifRepo = $em->getRepository(Notification::class);
                $notifs = $notifRepo->findBy(array('aboutId' => $newsId, 'aboutType'=>'news'));
                foreach ($notifs as $key => $notif) {
                    $em->remove($notif); 
                }

                if($report != null) $em->remove($report); 
                if($cs != null)     $em->remove($cs); 
                if($news != null)   $em->remove($news);
                
                $em->flush();*/
                $removed = true;
            }
        }

        //dump($this->getUser()->getMyUserPage()->getRelations()->getImAdminOf($news->getSigned()->getSlug()));
        //dump($this->getUser()->getMyUserPage()->getRelations()->getImAdminOf($news->getTarget()->getSlug()));

        return $this->json(array('error' => !$removed,
                                 //'newText' => $comment->getTextHtml(), 
                                 //'notif' => $notif
                                 )
                           );
    }
}
