<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use App\Form\PageType;
use App\Form\NewsType;
use App\Form\CommentType;
use App\Entity\User;
use App\Entity\News;
use App\Entity\Page;
use App\Entity\CommentStream;
use App\Entity\Comment;

use App\Services\Helper;
//echo `pwd`; exit;

class AgendaController extends Controller
{
    
    /**
     * @Route("/agenda", name="agenda")
     */
    public function agenda(Request $request, AuthorizationCheckerInterface $authChecker){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $myUser = $this->getUser();

        $newPage = new Page();
        $isAdminOf = $this->getUser()->getMyUserPage()->getRelations()->getIsAdminOf()->toArray();
        
        $form = $this->createForm(PageType::class, $newPage, 
                                    array("adminPage"=>$isAdminOf, "pageType"=>Page::TYPE_EVENT));
        
        //if($search == "-") 
        $search = "";
        $myUser = $this->getUser();
        $myPosition = $myUser->getMyPosition();
        
        //$aroundCoordinates = null;
        //if($request->query->get('lat') != null && $request->query->get('lon') != null)
        //$aroundCoordinates = array(floatval($request->query->get('lat')), 
        //                            floatval($request->query->get('lon')));

        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);

        

        return $this->render('agenda/agenda.html.twig', 
                             array( 'form' => $form->createView(),
                                    'myPosition' => $myPosition,
                                    'action' => 'create-page'
        ));
    }


    
    /**
     * @Route("/get-events/{search}/{startDate}/{endDate}", name="get-events")
     */
    public function getEvents($search, $startDate, $endDate, AuthorizationCheckerInterface $authChecker){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);

        $res = $pageRepo->getEvents($search, $startDate, $endDate
                                    //$aroundCoordinates,
                                    //floatval($request->query->get('radius'))
                                );
    
        $jsonRes = $this->getJsonRes($res, $this->getUser()->getMyUserPage());
        return $this->json($jsonRes);
    }
    
    /**
     * @Route("/get-event-view/{eventId}", name="get-event-view")
     */
    public function getEventView($eventId, AuthorizationCheckerInterface $authChecker){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
        $event = $pageRepo->findOneBy(array("id" => $eventId));

        return $this->render('agenda/event-view.html.twig', array("allEvents" => array($event)));
    }

    /**
     * @Route("/get-events-view/", name="get-events-view")
     */
    public function getEventsView(AuthorizationCheckerInterface $authChecker){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        if(@$_POST["ids"]){
            $eventsIds = $_POST["ids"];
            $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
            $events = $pageRepo->findBy(array("id" => array('$in'=>$eventsIds)));

            return $this->render('agenda/event-view.html.twig', array("allEvents" => $events));
        }else{
            return $this->json(array("error"=>"no ids given"));
        }
    }

    /**
     * @Route("/create-event", name="agenda-create-event")
     */
    public function createEvent(AuthorizationCheckerInterface $authChecker){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $myUser = $this->getUser();

        $newPage = new Page();
        $isAdminOf = $this->getUser()->getMyUserPage()->getRelations()->getIsAdminOf()->toArray();
        
        $form = $this->createForm(PageType::class, $newPage, 
                                    array("adminPage"=>$isAdminOf, "pageType"=>Page::TYPE_EVENT));
        
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $newPage->setOwner($myUser);
            $slug = Helper::slugify($newPage->getName(), true);
            $newPage->setSlug($slug);
            
            if($newPage->getLongitude() != null && $newPage->getLatitude() != null)
            $newPage->setCoordinates([(float)$newPage->getLongitude(), (float)$newPage->getLatitude()]);

            $newPage->setCreated(new \Datetime());

            //add the current user page in admins
            $newPage->getRelations()->addAdmin($myUser->getMyUserPage());
            $userPage = $myUser->getMyUserPage();
            $userPage->getRelations()->addIsAdminOf($newPage);

            // Enregistre la page dans la bdd
            $em = $this->get('doctrine_mongodb')->getManager();
            $em->persist($newPage);
            $em->flush();

            return $this->redirectToRoute('page', array("slug"=>$newPage->getSlug()));
        }

        return $this->render('page/create-page.html.twig', 
            array('form' => $form->createView(),
                  'action' => 'create-page'));

        return $this->render('agenda/forms/form-event.html.twig', [
        ]);
    }


    /**
     * Convert search result objects to Json<br>
     * (to use datas in javascript, mainly for SIG to display datas on map)<br>
     * if you want your page appear in result, set excludeMe to false
     * @return array
     */
    private function getJsonRes($res, $userPage, $excludeMe=true){ 
        $jsonRes = array();
        $myPageId = $this->getUser()->getMyUserPage()->getId();
        foreach ($res as $key => $value) {
            if($excludeMe == false || $value->getId() != $myPageId)
            $jsonRes[] = $value->getJson($userPage);        
        }
        return $jsonRes;
    }


}