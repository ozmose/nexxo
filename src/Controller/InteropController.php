<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Cookie;

use App\Form\UserType;
use App\Form\NewsType;
use App\Form\ContactType;
use App\Form\CommentType;
use App\Form\PageType;
use App\Form\PageInfoType;
use App\Entity\User;
use App\Entity\News;
use App\Entity\Page;
use App\Entity\CommentStream;
use App\Entity\Comment;
use App\Entity\Tag;
use App\Entity\Contact;

use App\Services\Helper;
use App\Services\InteropTranslator;

use Unirest;

//echo `pwd`; exit;

class InteropController extends Controller
{
    /**
     * Route : /search-interop/{search}/{renderPartial}", name="search-interop"
     * @Route("/search-interop/{search}/{renderPartial}", name="search-interop")
     */
    public function searchInterop($search="", $renderPartial="false",
                            Request $request, AuthorizationCheckerInterface $authChecker)
    {

        if (!$authChecker->isGranted('ROLE_USER')) { throw new AccessDeniedException(); }

        if($search == "-") $search = "";
        $search = preg_replace("/></", "", $search);
        $search = preg_replace("/ /", ",", $search);
        
        $myUser = $this->getUser();
        $myPosition = $myUser->getMyPosition();
        
        $aroundCoordinates = null;
        if($request->query->get('lat') != null && $request->query->get('lon') != null)
        $aroundCoordinates = array(floatval($request->query->get('lat')), 
                                    floatval($request->query->get('lon')));

        $myAgenda = $request->query->get('myagenda') != null ? $request->query->get('myagenda') : false;
        

        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);

        /*$allRes = $pageRepo->getGlobalSearch($search,  
                                          $request->query->get('types'), 
                                          $aroundCoordinates,
                                          floatval($request->query->get('radius')),
                                          $startDate, 
                                          $endDate,
                                          $myAgenda,
                                          array()
                                        );
        $res = $allRes["res"];*/
        $bounds = $request->query->get('bounds');
        //dump($bounds);
        $headers = array('Accept' => 'application/json');
        $params = array("tags" => $search, "limit" => 1000); //dd($params);
        $type = $request->query->get('types');
        if($type == "") $type = "organization";
        //$firstRes = Unirest\Request::get('http://presdecheznous.fr/api/elements.json?limit=100&bounds='.$bounds.'&token=5c6d1af4f1ba7');
        $firstRes = Unirest\Request::get('http://presdecheznous.fr/api/elements.json?limit=500&categories='.$search.'&bounds='.$bounds.'&token=5c6d1af4f1ba7');

        //$firstRes = Unirest\Request::get('https://transiscope.gogocarto.fr/api/elements.json?limit=500&categories='.$search.'&bounds='.$bounds.'');
        
        //AMAP 539
        //Producteur/Artisan 538
        //Marché 533

        //$firstRes = Unirest\Request::get('https://communecter.org/api/'.$type.'/get', $headers, $params);
        //$firstRes = Unirest\Request::get('http://sct1.scrutari.net/sct/geosse/json?flt-bbox=-5.4534286,41.2632185,9.8678344,51.268318&fieldvariant=geosse&type=geojson&lang=fr&origin=js-geosse&warnings=1&version=3');


        //dd($firstRes->body->data);
        //dd($firstRes->body->features);
        //
        //$firstRes = json_decode($firstRes->raw_body);
        $firstRes = $firstRes->body->data;

        $typeNxo = "freegroup";
        $allRes = array(); $allTags = array();
        $i=0; $iMax = 1000;
        foreach ($firstRes as $key => $res) { if($key > $iMax) break;
            //$newPage = InteropTranslator::translate("communecter", $res, $typeNxo);
            //dump($res); exit; return;
            $newPage = InteropTranslator::translate("presdecheznous", $res, $typeNxo);
            if($newPage != null) {
                $allRes[] = $newPage;
                foreach ($newPage->getTags() as $key => $tag) {
                    if(!array_key_exists($tag, $allTags)) $allTags[$tag] = 1;
                    else $allTags[$tag]++;
                }
            }
        }

        $endAllTags = array("AMAP"=>539, "Producteur/Artisan"=>538, "Marché"=>533, "La Ruche qui dit oui"=>536, "Éco-construction"=>562,
                            "Énergie renouvelables"=>566, "Jardins partagés"=>575, "Grainothèque"=>576, "Friperie"=>613, "Produits en vrac"=>828, "Point de collecte"=>812, "Collectifs citoyens" => 802 );
        /*foreach ($allTags as $tag => $count) {
            if($count > 5) $endAllTags[] = $tag;
        }*/
        //dump($endAllTags);
        //dd($allRes);//["meta"]["entities"]);

        $totalCount = 0; //count($allRes);//["totalCount"];

        $jsonRes = $this->getJsonRes($allRes, $this->getUser()->getMyUserPage());
        $params = array(
                "results" => $allRes,
                "totalCount" => $totalCount,
                'jsonRes' => $jsonRes,
                "yourSearch" => $search,
                'myPosition' => $myPosition,
                'typeSearch' => Page::getTypeInteropSearch(),
                'endAllTags' => $endAllTags
            );

        if(!empty($request->query->get('types')))   $params["typesSearch"] = $request->query->get('types');
        if($aroundCoordinates != null)              $params["coordinatesSearch"] = $aroundCoordinates;
        if(!empty($request->query->get('radius')))  $params["radiusSearch"] = floatval($request->query->get('radius'));
       
         
        //dd($jsonRes);
        if($renderPartial=="false"){
            return $this->render('interop/search.html.twig', $params);
        }elseif($renderPartial=="json"){
            return $this->json(array('jsonRes' => $jsonRes));
        }else{
            return $this->render('interop/search-res-list.html.twig', $params);
        }
    }




    /**
     * Convert search result objects to Json<br>
     * (to use datas in javascript, mainly for SIG to display datas on map)<br>
     * if you want your page appear in result, set excludeMe to false
     * @return array
     */
    private function getJsonRes($res, $userPage, $excludeMe=true){ 
        $jsonRes = array();
        $myPageId = $this->getUser()->getMyUserPage()->getId();
        foreach ($res as $key => $value) {
            if($excludeMe == false || $value->getId() != $myPageId)
            $jsonRes[] = $value->getJson($userPage);        
        }
        return $jsonRes;
    }


    private function objsToId($array){
      $res = array();
      foreach($array as $key => $val)  { $res[] = new \MongoId($val->getId()); }
      return $res;
    }



    
    /**
     * Route : /import-json, name="import-json"
     * @Route("/import-json", name="import-json")
     */
    public function importJson(Request $request, AuthorizationCheckerInterface $authChecker)
    {
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }
        
        $string = file_get_contents("ateliers_citoyens.geojson");
        $json = json_decode($string, true);
        
        dd($json);
        foreach ($json["features"] as $key => $value) {
            $page = new Page();
            $page->setName($value["properties"]["Name"]);
            $page->setDescription($value["properties"]["description"]);

            $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
            $slug = Helper::getUniqueSlug($page->getName(), $pageRepo);
            $page->setSlug($slug);

            $page->setType(Page::TYPE_FREEGROUP);
            $page->setTags(array("#atelierCitoyens", "#RIC"));
            $page->setCoordinates($value["geometry"]["coordinates"]);
            $page->setIsActive(true);
            $page->setCreated(new \Datetime());
            $page->setOwner($this->getUser());
            $page->setConfidentialityByKey("SHOW_GEOPOS", "CONF_NEXXO");
            //dd($page);

            $em = $this->get('doctrine_mongodb')->getManager();
            $em->persist($page);
            $em->flush();
        }

        return $this->json(array('jsonRes' => "ok"));
    }

}