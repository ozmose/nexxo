<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

use App\Entity\Media;

use Symfony\Component\DomCrawler\Crawler;
use Psr\Log\LoggerInterface;

class MediaController extends Controller
{    
    /**
     * Route : /mediatheque", name="mediatheque"
     * @Route("/mediatheque", name="mediatheque")
     */
    public function mediatheque(Request $request, AuthorizationCheckerInterface $authChecker){

    	$em = $this->get('doctrine_mongodb')->getManager();
    	$mediaRepo = $em->getRepository(Media::class);

    	$channels = Media::getChannels();
    	$videos = array();
    	foreach ($channels as $key => $channel) {
    		$medias = $mediaRepo->findBy(array("channelUrl" => $channel), array("created" => "DESC"), 6);
    		$videos[$channel]["videos"] = array();
    		foreach ($medias as $k => $media) {
    			$videos[$channel]["name"] = $media->getChannelName();
    			$videos[$channel]["description"] = $media->getChannelDescription();
    			$videos[$channel]["thumb"] = $media->getChannelThumb();
    			$videos[$channel]["videos"][] = $media;
    		}
    	}
    	

    	$params = array("videos" => $videos);
    	return $this->render('mediatheque/home.html.twig', $params);
    }

    /**
     * Route : /mediatheque/load/{search}/{renderPartial}", name="mediatheque-load"
     * @Route("/mediatheque/load/{search}/{renderPartial}", name="mediatheque-load")
     */
    public function load($search="", $renderPartial="false", LoggerInterface $logger, 
    								 						 Request $request, 
    								 						 AuthorizationCheckerInterface $authChecker){

    	$channels = Media::getChannels();

    	$em = $this->get('doctrine_mongodb')->getManager();
    	$mediaRepo = $em->getRepository(Media::class);

    	$parsed = 0;//array();
	    foreach($channels as $k => $url){  $logger->info("LOAD YOUTUBE CHANNEL - ".$url); //error_log("LOAD YOUTUBE CHANNEL - ".$url);
	    	$html = file_get_contents($url);
	        $crawler = new Crawler($html);

	        //dd($crawler->html());

	        $channelName = $crawler->filter("meta[name='title']")->attr("content");
	        $channelDescription = $crawler->filter("meta[name='description']")->attr("content");
	        $channelThumb = $crawler->filter("img.appbar-nav-avatar")->attr("src");
	        //dd($crawler->html());


	        $container = $crawler->filter(".channels-content-item")->each(function (Crawler $node, $i) {
				    return $node->html();
				});//->html();

	        /*$parsed[$url] = array("name" => $channelName, 
	        					  "description" => $channelDescription, 
	        					  "thumb" => $channelThumb,
	        					  "videos" => array());*/

	        foreach ($container as $key => $value) {
	        	$crawler = new Crawler($value);
	        	$title = $crawler->filter(".yt-uix-tile-link")->html();
	        	$href = $crawler->filter(".yt-uix-tile-link")->attr("href");
	        	$imgSrc = $crawler->filter("img")->attr("src");
	        	

        		$media = $mediaRepo->findOneBy(array("href" => $href));
        		if($media == null){
        			$media = new Media(array( "channelName" => $channelName, 
				        					  "channelDescription" => $channelDescription, 
				        					  "channelThumb" => $channelThumb,
				        					  "channelUrl" => $url,
				        					  "title" => $title, 
		        							  "href" => $href, 
		        							  "imgSrc" => $imgSrc,
		        							  "created" => new \Datetime()
				        				));
        			$em->persist($media);
        			$em->flush();

                    $parsed++;

        		}else{
        			break;
        		}
	        } 

	    }
        
        return $this->json(array('error' => false, 
                                 'msg' => $parsed.' nouvelle vidéos ont été trouvées'));
    	//$params = array("videos" => $parsed);
    	//return $this->render('mediatheque/media-list.html.twig', $params);

    }

}
