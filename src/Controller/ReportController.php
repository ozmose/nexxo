<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Translation\TranslatorInterface;

use App\Entity\User;
use App\Entity\News;
use App\Entity\Page;
use App\Entity\CommentStream;
use App\Entity\Notification;
use App\Entity\Comment;
use App\Entity\Report;

use App\Services\Helper;

class ReportController extends Controller
{
    /**
     * @Route("/save-report/{aboutType}/{aboutId}/{whatType}/{whatId}", name="save-report")
     */
    public function saveReport($aboutType, $aboutId, $whatType=null, $whatId=null, 
                                    TranslatorInterface $translator,  AuthorizationCheckerInterface $authChecker)
    {
    	if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }
        if($this->checkType($aboutType)==false) 
        	return $this->json(array('error' => true, 'aboutType' => $aboutType));

        //check if the element is already reported
        $reportRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Report::class);

        $request = array("aboutType" => $aboutType, "aboutId" => $aboutId);
        if($whatId!=null && $whatType == "comment")
        	$request["commentId"] = $whatId;

        $report = $reportRepo->findOneBy($request);
        //dd($report);
            
        $notif = false;

        //s'il y a plus de $maxReport : la procedure de vote s'ouvre
        $maxReport = Report::getMaxReport();

        $em = $this->get('doctrine_mongodb')->getManager();
	    if($report != null){
            
            if($report->getDateEndVote() != null && $report->getDateEndVote() < new \Datetime())
               return $this->redirectToRoute('close-reports');

           /* COMMENTAIRE REASON */
           $CS = $report->getCommentStream();
           if($CS->getParentType() == null){
                $CS->setParentId($report->getId());
                $CS->setParentType('report');
                $report->setCommentStream($CS);

                $em->persist($CS);
            }
            
            $reason = @$_POST['reason'] != "" ? @$_POST['reason'] : 
                      $translator->trans("did not justify his report", array(), "report");

            $comment = new Comment();
            $comment->setText($reason);
            $comment->setSigned($this->getUser()->getMyUserPage());
            $comment->setAuthor($this->getUser()->getMyUserPage());
            $CS->addComment($comment);
            /* COMMENTAIRE REASON */

	    	//if report exists, add me in authors
	    	$report->addAuthor($this->getUser()->getMyUserPage());
	    	if(sizeof($report->getAuthors()) >= $maxReport && $report->getStatus() == Report::STATUS_OPEN){
	    		$report->setStatus(Report::STATUS_VOTE_OPEN);
                $date = new \Datetime();
                $report->setDateStartVote(new \Datetime());
                //$report->setDateEndVote($date->add(new \DateInterval("PT60S")));
                $report->setDateEndVote($date->add(new \DateInterval("P3D")));

                $notifObj = Notification::sendNotification( $this->getUser()->getMyUserPage(), 
                                                            Notification::VERB_REPORT_VOTE_OPEN,
                                                            $aboutId, $aboutType, $whatId, $whatType, $em, $this);
                $notif = array("id"=>$notifObj->getId(),
                               "html"=> $this->renderView('notification/item-notification.html.twig',
                                                          array('notif' => $notifObj)));
	    	}
        }else{
        	//if report not exists, create the report
	        $report = new Report($aboutId, $aboutType, $whatId, $this->getUser()->getMyUserPage());
            $em->persist($report);
            /* COMMENTAIRE REASON */
            $CS = $report->getCommentStream();
            if($CS->getParentType() == null){
                $CS->setParentId($report->getId());
                $CS->setParentType('report');
                $report->setCommentStream($CS);

                $em->persist($CS);
            }
            
            $reason = @$_POST['reason'] != "" ? @$_POST['reason'] : 
                      $translator->trans("did not justify his report", array(), "report");

            $comment = new Comment();
            $comment->setText($reason);
            $comment->setSigned($this->getUser()->getMyUserPage());
            $comment->setAuthor($this->getUser()->getMyUserPage());
            $CS->addComment($comment);
            /* COMMENTAIRE REASON */

        }

        //dd($notif);
        
        $em->flush();

        $class = Report::getClass($report->getAboutType());
        if($class=="App\Entity\Comment") $class="App\Entity\News";        
        $repo = $this->get('doctrine_mongodb')->getManager()->getRepository($class);
        $aboutObj = $repo->findOneBy(array("id" => $report->getAboutId()));
        
        if($whatId == null){
        	$aboutObj->setReport($report); 
        	$em->flush();       	
        }else{
        	$CS = $aboutObj->getCommentStream();
        	$comment = $CS->getCommentById($whatId);
        	if($comment != false){
        		//dd($comment);
	        	$comment->setReport($report);
	        	$CS->setCommentById($whatId, $comment);
	        	$em->flush($CS);       	
	        }
        }
        return $this->json(array('error' => false, 'notif' => $notif));
    }

	/**
     * @Route("/all-reports/{aboutType}", name="all-report")
     */
    public function getAllReports($aboutType)
    {

    }

	/**
     * @Route("/report/{aboutType}/{aboutId}/{commentId}", name="report")
     */
    public function getReport($aboutType, $aboutId, $commentId=null)
    {

    }

	/**
     * CRON
     * @Route("/close-reports", name="close-reports")
     */
    public function closeReports(AuthorizationCheckerInterface $authChecker)
    {
    	if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $em = $this->get('doctrine_mongodb')->getManager();
        
        //get the report
        $reportRepo = $em->getRepository(Report::class);
        $reports = $reportRepo->findBy(array('status'=>Report::STATUS_VOTE_OPEN,
                                             'dateEndVote' => array('$lt'=>new \Datetime())
                                             ));

        $countUp = 0; $countDown = 0;
        foreach ($reports as $key => $report) {
            if($report->getDateEndVote() != null && $report->getDateEndVote() < new \Datetime()){
                $result = $report->getVoteResult();
                if($result == "down") $countDown++;
                if($result == "up") $countUp++;

                $this->closeAReport($report, $result, "CO", $em);
            }
        }
        
        return $this->json(array('error' => false, 
                                 "msg" => "It works !",
                                 "countUp" => $countUp,
                                 "countDown" => $countDown));
    }
	

	/**
     * Enable superadmins to close a report
     * @Route("/close-reports-admin/{reportId}/{voteVal}", name="close-report-admin")
     */
    public function closeReportAdmin($reportId, $voteVal, AuthorizationCheckerInterface $authChecker)
    {
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $em = $this->get('doctrine_mongodb')->getManager();
        
        if($this->getUser()->isSuperAdmin() == true){   
            //get the report
            $reportRepo = $em->getRepository(Report::class);
            $report = $reportRepo->findOneBy(array('id'=>$reportId));

            //if($report->getDateEndVote() != null && $report->getDateEndVote() < new \Datetime())
            //    return $this->json(array('error' => true, "msg" => "Sorry, the votation is not ended !"));

            if($report != null) $this->closeAReport($report, $voteVal, "ADMIN", $em);
            else return $this->json(array('error' => true, "msg" => "This report does not exists anymore."));

            return $this->render('report/bar-vote-admin.html.twig', array("report" => $report));
        }else{

            return $this->json(array('error' => true, "msg" => "Your are not admin bro !"));
        }
        
    }

    /**
     * Enable user to close a report if the voteEnd is over
     * And before CRON execution
     * @Route("/close-report/{id}", name="close-report")
     */
    public function closeReport($id, AuthorizationCheckerInterface $authChecker)
    {
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $em = $this->get('doctrine_mongodb')->getManager();
        
        //get the report
        $reportRepo = $em->getRepository(Report::class);
        $report = $reportRepo->findOneBy(array('id'=>$id));

        if($report == null) 
            return $this->json(array('error' => true, "msg" => "Sorry, this report does not exists anymore."));

        if($report->getDateEndVote() < new \Datetime()){   
            $result = $report->getVoteResult();
            $aboutObj = $this->closeAReport($report, $result, "CO", $em);
            
            if($report->getAboutType() == "page")
                return $this->redirectToRoute('page', array("slug"=>$aboutObj->getSlug()));

            if($report->getAboutType() == "news")
                return $this->redirectToRoute('news-single-post', array("id"=>$report->getAboutId()));

            return $this->redirectToRoute('my-dashboard');
        }else{
            return $this->json(array('error' => true, "msg" => "Sorry, the vote session is not ended."));
        }
        
    }

    private function closeAReport($report, $result, $origin="ADMIN"/* || "CO" */, $em){

        $class = Report::getClass($report->getAboutType());
        //if($report->getCommentId() != null) $class="App\Entity\News";        
        $repo = $em->getRepository($class);
        $aboutObj = $repo->findOneBy(array("id" => $report->getAboutId()));

        if($report->getCommentId() != null)
            $aboutObj = $aboutObj->getCommentStream()->getCommentById($report->getCommentId());

        $verb = "";

        if($result == "down"){
            $report->setStatus(Report::STATUS_DISABLED);
            $aboutObj->setIsActive(false);

            $this->closeReportCascadeDown($report, $aboutObj, $em);
            if($origin=="ADMIN"){
                $report->setClosedByType(Report::CLOSED_BY_ADMIN);
                $report->setClosedByUser($this->getUser()->getMyUserPage());
            }elseif($origin=="CO"){
                $report->setClosedByType(Report::CLOSED_BY_CO_MODERATION);
            }

            $em->flush($report);
            $verb = Notification::VERB_REPORT_DISABLED;
            
        }
        if($result == "up"){
            $report->setStatus(Report::STATUS_ENABLED);
            
            $aboutObj->setIsActive(true);
            $aboutObj->removeReport();
            
            $em->remove($report);
            $em->flush();

            //supprime les notifications précédentes
            $notifRepo = $em->getRepository(Notification::class);
            $notifs = $notifRepo->findBy(array('aboutType'=>$report->getAboutType(),
                                               'aboutId' => $report->getAboutId(),
                                               'whatId'=>$report->getCommentId())); 
            foreach ($notifs as $n => $notif) {
                $em->remove($notif);
                $em->flush();
            }

            if($origin=="ADMIN")  $verb = Notification::VERB_REPORT_CLEARED_BYADMIN;
            elseif($origin=="CO") $verb = Notification::VERB_REPORT_CLEARED_BYCO;

        }

        $notifObj = Notification::sendNotification( $this->getUser()->getMyUserPage(), 
                                                    $verb,
                                                    $report->getAboutId(), $report->getAboutType(), 
                                                    $report->getCommentId(), 
                                                    $report->getCommentId()!=null ? "comment" : null, 
                                                    $em, $this);
        $em->flush($aboutObj);
        return $aboutObj;
    }
	

    private function closeReportCascadeDown($report, $aboutObj, $em){
        if($report->getAboutType() == "news" && $report->getCommentId() == null){ 
            $aboutObj->cleanLock($em);
        }

        if($report->getAboutType() == "page"){ 
            $aboutObj->cleanLock($em);
        }

        //supprime les notifications précédentes
        $notifRepo = $em->getRepository(Notification::class);
        $notifs = $notifRepo->findBy(array('aboutType'=>$report->getAboutType(),
                                           'aboutId' => $report->getAboutId(),
                                           'whatId'=>$report->getCommentId(),
                                           'verb'=>Notification::VERB_REPORT_VOTE_OPEN)); 
        foreach ($notifs as $n => $notif) {
            $em->remove($notif);
            $em->flush();
        }
    }


    /**
     * @Route("/recover-report/{id}", name="recover-report")
     */
    public function recoverReport($id, AuthorizationCheckerInterface $authChecker)
    {
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $em = $this->get('doctrine_mongodb')->getManager();
        
        //get the report
        $reportRepo = $em->getRepository(Report::class);
        $report = $reportRepo->findOneBy(array('id'=>$id));

        if($report->getAboutType() == "page"){

            if($report->getStatus() == Report::STATUS_DISABLED){
                $report->setStatus(Report::STATUS_VOTE_OPEN);
                $date = new \Datetime();
                $report->setDateStartVote(new \Datetime());
                //$report->setDateEndVote($date->add(new \DateInterval("PT60S")));
                $report->setDateEndVote($date->add(new \DateInterval("P3D")));

                $em->flush($report);
            }

            $pageRepo = $em->getRepository(Page::class);
            $page = $pageRepo->findOneBy(array('id'=>$report->getAboutId()));

            $notifObj = Notification::sendNotification( $this->getUser()->getMyUserPage(), 
                                                        Notification::VERB_REPORT_VOTE_OPEN,
                                                        $report->getAboutId(), $report->getAboutType(), 
                                                        $report->getCommentId(), 
                                                        $report->getCommentId()!=null ? "comment" : null, 
                                                        $em, $this);

            return $this->redirectToRoute('page', array("slug"=>$page->getSlug()));
        }
            return $this->json(array('error' => true, "msg" => "You cant recover ".$report->getAboutType()));
    }

    /**
     * @Route("/all-reports-pages", name="all-reports-pages")
     */
    public function allReportsPages(AuthorizationCheckerInterface $authChecker)
    {
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $em = $this->get('doctrine_mongodb')->getManager();
        //get the report
        $reportRepo = $em->getRepository(Report::class);
        $reports = $reportRepo->findBy(array('status'=>Report::STATUS_VOTE_OPEN, "aboutType" => 'page'));

        foreach ($reports as $key => $report) {
            $pageRepo = $em->getRepository(Page::class);
            $page = $pageRepo->findOneBy(array('id'=>$report->getAboutId()));
            $reports[$key]->setAboutObj($page);
        }

        return $this->render('report/all-reports-pages.html.twig', array("reports" => $reports));
    }
    
    
    /**
     * @Route("/all-reports-closed-pages", name="all-reports-closed-pages")
     */
    public function allReportsClosedPages(AuthorizationCheckerInterface $authChecker)
    {
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $em = $this->get('doctrine_mongodb')->getManager();
        //get the report
        $reportRepo = $em->getRepository(Report::class);
        $reports = $reportRepo->findBy(array('status'=>Report::STATUS_DISABLED, "aboutType" => 'page'));

        foreach ($reports as $key => $report) {
            $pageRepo = $em->getRepository(Page::class);
            $page = $pageRepo->findOneBy(array('id'=>$report->getAboutId()));
            $reports[$key]->setAboutObj($page);
        }

        return $this->render('report/all-reports-closed-pages.html.twig', array("reports" => $reports));
    }
    

    /**
     * @Route("/all-reports-closed-news", name="all-reports-closed-news")
     */
    public function allReportsClosedNews(AuthorizationCheckerInterface $authChecker)
    {
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $em = $this->get('doctrine_mongodb')->getManager();
        //get the report
        $reportRepo = $em->getRepository(Report::class);
        $reports = $reportRepo->findBy(array('status'=>Report::STATUS_DISABLED, 
                                             "aboutType" => 'news',
                                             "commentId"=>array('$exists'=>false)), array('created'=>"DESC"), 100);

        foreach ($reports as $key => $report) {
            $newsRepo = $em->getRepository(News::class);
            $news = $newsRepo->findOneBy(array('id'=>$report->getAboutId()));
            $reports[$key]->setAboutObj($news);
        }

        return $this->render('report/all-reports-closed-news.html.twig', array("reports" => $reports));
    }
    

    /**
     * @Route("/all-reports-closed-comments", name="all-reports-closed-comments")
     */
    public function allReportsClosedComments(AuthorizationCheckerInterface $authChecker)
    {
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $em = $this->get('doctrine_mongodb')->getManager();
        //get the report
        $reportRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Report::class);
        $reportsCS = $reportRepo->findBy(array('status'=>Report::STATUS_DISABLED, "commentId"=>array('$exists'=>true)));
        //$reports = array();
        foreach($reportsCS as $key => $report){
          $repo = $this->get('doctrine_mongodb')->getManager()->getRepository(CommentStream::class);
          $cs = $repo->findOneBy(array("parentId" => $report->getAboutId(), 
                                       "parentType" => $report->getAboutType()));
          $report->setAboutObj($cs->getCommentById($report->getCommentId()));
          
        }
        return $this->render('report/all-reports-closed-comments.html.twig', array("reports" => $reportsCS));
    }
    

    /**
     * @Route("/save-vote-report/{voteVal}/{aboutType}/{aboutId}/{commentId}", name="save-vote-report")
     */
    public function saveVoteReport($voteVal, $aboutType, $aboutId, $commentId=null, AuthorizationCheckerInterface $authChecker)
    {
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }
        if($this->checkType($aboutType)==false) 
        	return $this->json(array('error' => true, "msg" => "error aboutType", 'aboutType' => $aboutType));

        //get the report
        $reportRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Report::class);

        $request = array("aboutType" => $aboutType, "aboutId" => $aboutId);
        if($commentId!=null)
        	$request["commentId"] = $commentId;

        $report = $reportRepo->findOneBy($request);

        $em = $this->get('doctrine_mongodb')->getManager();
	    if($report != null){
	    	if($voteVal == "up")
	    		$report->addVoteUp($this->getUser()->getMyUserPage());
	    	if($voteVal == "down")
	    		$report->addVoteDown($this->getUser()->getMyUserPage());

	    	$em->flush();
        }else{
        	return $this->json(array('error' => false, "msg" => "report not found"));
        }

        return $this->render('report/bar-vote.html.twig', array("report" => $report));
        //return $this->json(array('error' => false));
    }


    /**
     * @Route("/report/modal-howitwork", name="report-modal-howitwork")
     */

    public function getModalHowitwork(AuthorizationCheckerInterface $authChecker)
    {

        return $this->render('report/modals/how-it-works.html.twig', array("maxReport" => Page::getMaxReport()));
    }
    /**
     * @Route("/report/modal-confirm", name="report-modal-confirm")
     */

    public function getModalConfirm(AuthorizationCheckerInterface $authChecker)
    {
        return $this->render('report/modals/confirm.html.twig');
    }

    private function checkType($aboutType){
    	return ($aboutType == "page" || $aboutType == "news" || $aboutType == "comment");
    }

}