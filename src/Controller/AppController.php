<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Cookie;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Console\Input\ArrayInput;

//use Symfony\Component\Process\Process;

use App\Form\UserType;
use App\Form\NewsType;
use App\Form\ContactType;
use App\Form\CommentType;
use App\Form\PageType;
use App\Form\PageInfoType;
use App\Entity\User;
use App\Entity\News;
use App\Entity\Page;
use App\Entity\CommentStream;
use App\Entity\Comment;
use App\Entity\Tag;
use App\Entity\Contact;

use App\Services\Helper;
use App\Sockets\StartSocketServer;

//echo `pwd`; exit;

class AppController extends Controller
{
    /**
     * @Route("/", name="index")
     */
    public function index(Request $request)
    {
        
        //dump($request->getPreferredLanguage());
        //dd($request->getSession()->get('_locale'));
        /*
        $userPrefLang = $request->getPreferredLanguage();
        $currentLocale = $request->getSession()->get('_locale');

        if($currentLocale == null && $userPrefLang == "fr"){
            $request->setLocale('fr');
            $request->getSession()->set('_locale', "fr");
        }*/

        // création du formulaire
        $user = new User();
        // instancie le formulaire avec les contraintes par défaut, + la contrainte registration pour que la saisie du mot de passe soit obligatoire
        $form = $this->createForm(UserType::class, $user,[
           'validation_groups' => array('User', 'registration'),
        ]);

        return $this->render('theme/homepage.html.twig',
            array('form' => $form->createView(), "maxReport" => Page::getMaxReport() )
            );

//        return $this->redirectToRoute('homepage');

    }

    /**
     * @Route("/home", name="homepage")
     */
    public function home()
    {
        // création du formulaire
        $user = new User();
        // instancie le formulaire avec les contraintes par défaut, + la contrainte registration pour que la saisie du mot de passe soit obligatoire
        $form = $this->createForm(UserType::class, $user,[
           'validation_groups' => array('User', 'registration'),
        ]);

        return $this->render('theme/homepage.html.twig',
            array('form' => $form->createView(), "maxReport" => Page::getMaxReport() )
            );
    }

    /**
     * @Route("/my-dashboard/{filter}", name="my-dashboard")
     */
    public function dashboard($filter = "all", Request $request, AuthorizationCheckerInterface $authChecker){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        //dump($request->getLocale());
        //dump($request->getSession()->get('_locale'));
        //$request->setLocale("en");

        $myUser = $this->getUser();
        $myPage = $myUser->getMyUserPage();
        
        if($myPage->getFirstStep() > 0){
            //$request->getSession()->getFlashBag()->add('success', "Validation e-mail ok, votre compte est activé.");
            return $this->redirectToRoute('first-step');
        }
        
        $news = new News();
        $formNews = $this->createForm(NewsType::class, $news);

        
        $myPage = $myUser->getMyUserPage();
        $myPosition = $myUser->getMyPosition();
  
        $newsRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(News::class);
        $posts = $newsRepo->getPostsDashboard($newsRepo, $myPage, $myPosition, $myPage);
        
        //inc number of view of each post
        $em = $this->get('doctrine_mongodb')->getManager();
        foreach ($posts as $key => $post) $post->incViewCount();
        $em->flush();

        //get my friends ids
        $myFriends = $myPage->getRelations()->getFriends()->toArray();
        $friendsId = array();
        foreach ($myFriends as $key => $friend) { $friendsId[] = $friend->getId(); }
         
        //get my follows ids (i follow)
        $myFollows = $myPage->getRelations()->getFollows()->toArray();
        $followsId = array();
        foreach ($myFollows as $key => $follow) { $followsId[] = $follow->getId(); }
        
        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);

        $friendSuggest = $pageRepo->getFriendSuggest($friendsId, $myPage->getId(), $myPosition);
        $pageSuggest = $pageRepo->getPageSuggest($followsId, $myPage->getId(), $myPosition);
        $agoraSuggest = $pageRepo->getPageSuggest($followsId, $myPage->getId(), $myPosition, Page::TYPE_AGORA);
       
        $pageReport = $pageRepo->getPageReported($myPosition);
        

        $postsUp    = $newsRepo->getPostsForward($newsRepo, $myPosition, $myPage, 
                        array("#bonneNouvelle"), "likes", 1);
        $postsDown  = $newsRepo->getPostsForward($newsRepo, $myPosition, $myPage, 
                        array("#mauvaiseNouvelle"), "dislikes", 1);

        if(empty($postsUp)) $postsUp = array(false);
        if(empty($postsDown)) $postsDown = array(false);
        
        $commentS = new CommentStream();
        $comment = new Comment();
        $commentForm = $this->createForm(CommentType::class, $comment);

        $tagsRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Tag::class);
        $topTenTags = $tagsRepo->getTopTen();

        return $this->render('dashboard/dashboard.html.twig', 
                             array("friendSuggest" => $friendSuggest,
                                   "pageSuggest" => $pageSuggest,
                                   "agoraSuggest" => $agoraSuggest,
                                   "pageReport" => $pageReport,
                                   "posts" => $posts,
                                   "postsUp" => $postsUp,
                                   "postsDown" => $postsDown,
                                   "page" => $myPage,
                                   "filter" => $filter,
                                   "formNews" => $formNews->createView(),
                                   "commentForm" => $commentForm->createView(),
                                   "myPosition" => $myPosition,
                                   "topTenTags" => $topTenTags));
    }


    /**
     * @Route("/first-step/{firstStep}", name="first-step")
     */
    public function firstStep(Int $firstStep=null, Request $request)
    {          
        $myUser = $this->getUser();
        $myPage = $myUser->getMyUserPage();
        //dd($firstStep);
        
        if($firstStep === 0){ 
            $myPage->setFirstStep(0);
            $em = $this->get('doctrine_mongodb')->getManager();
            $em->flush($myPage);
            //dd("ok firstStep");
            return $this->redirectToRoute('my-dashboard');
        }

        if($firstStep != null && $myPage->getFirstStep() == ($firstStep-1)){
            $myPage->setFirstStep($firstStep);
            $em = $this->get('doctrine_mongodb')->getManager();
            $em->flush($myPage);
        }

        if($firstStep == null)
            $firstStep = $myPage->getFirstStep();

        
        $subview = "form-main-info";
        $form = null;
        if($firstStep == 1){
            $isAdminOf = $myPage->getRelations()->getIsAdminOf()->toArray();
            $form = $this->createForm(PageType::class, $myPage, 
                                      array("adminPage"=>$isAdminOf, "pageType"=>$myPage->getType()));
            $subview = "form-address-geopos";
        }

        if($firstStep == 2){
            $isAdminOf = $myPage->getRelations()->getIsAdminOf()->toArray();
            $form = $this->createForm(PageType::class, $myPage, 
                                      array("adminPage"=>$isAdminOf, "pageType"=>$myPage->getType()));
            $subview = "form-address-geopos";
        }
        if($firstStep == 3){
            $form = $this->createForm(PageInfoType::class, $myPage->getInformations());
            $subview = "form-perso-info";
        }
        if($firstStep == 4){
            $myPage->initConfidentiality();
            $subview = "form-confidentiality";
        }
        if($firstStep == 5){
            $subview = "form-preferences";
        }

        $formView = $form != null ? $form->createView() : null;
        return $this->render('/first-step/first-step-'.$firstStep.'.html.twig', [
            "page" => $myPage,
            "form" => $formView,
            "action" => 'page-settings',
            "subview" => $subview
        ]);
    }


    /**
     * @Route("/admin", name="admin")
     */
    public function admin()
    {
        return $this->render('admin.html.twig', [

        ]);
    }


    

    /**
     * @Route("/tchat", name="tchat")
     */
    public function tchat(){
       
        return $this->render('theme/landing/tchat.html.twig', [
        ]);
    }

    /**
     * @Route("/about", name="about")
     */
    public function about(){
       
        return $this->render('theme/landing/about.html.twig', [
        ]);
    }

    /**
     * @Route("/roadmap", name="roadmap")
     */
    public function roadMap(){
       
        return $this->render('theme/landing/roadmap.html.twig', [
        ]);
    }

    /**
     * @Route("/faq", name="faq")
     */
    public function faq(){
       
        return $this->render('theme/landing/faq.html.twig', [
        ]);
    }


    /**
     * @Route("/help", name="help")
     */
    public function help(){
       
        return $this->render('theme/landing/help.html.twig', [
        ]);
    }
    /**
     * @Route("/cgu", name="cgu")
     */
    public function cgu(){
       
        return $this->render('theme/landing/cgu.html.twig', [
        ]);
    }

    /**
     * @Route("/legal", name="legal")
     */
    public function legal(){
       
        return $this->render('theme/landing/legal.html.twig', [
        ]);
    }

    /**
     * @Route("/rgpd", name="rgpd")
     */
    public function rgpd(){
       
        return $this->render('theme/landing/rgpd.html.twig', [
        ]);
    }

    /**
     * @Route("/app/load-sig-assets", name="load-sig-assets")
     */
    public function loadSigAssets(){
       
        return $this->render('theme/sig-assets.html.twig', [
        ]);
    }

    /**
     * @Route("/app/get-info-url", name="get-info-url")
     */
    public function getInfoUrl(Request $request, AuthorizationCheckerInterface $authChecker){

        if (!$authChecker->isGranted('ROLE_USER')) { throw new AccessDeniedException(); }

        //require('../src/Services/simple_html_dom.php');
        $urlShared = $request->query->get('urlShared');

        $metadata = Helper::exctractMetaData($urlShared);
        $metadata["url"] = $urlShared;
        $metadata["preview"] = true;
        // return $this->render('news/blocks/media-shared.html.twig', [
        //                         'urlShared' => $urlShared,
        //                         'metadata'  => $metadata,
        // ]);

        //dd($metadata);
        return $this->json(array('urlShared' => $urlShared,
                                 //'urlShared' => $request->query->get('urlShared'),
                                 'metadata' => $metadata));

    }

    
    /**
     * @Route("/app/contact-admin/{type}", name="contact-admin")
     */
    public function contactAdmin($type, Request $request, AuthorizationCheckerInterface $authChecker){

        //if (!$authChecker->isGranted('ROLE_USER')) { throw new AccessDeniedException(); }

        $contact = new Contact($type);
        $formContact = $this->createForm(ContactType::class, $contact);

        $formContact->handleRequest($request); 
        
        if ($formContact->isSubmitted() && $formContact->isValid()) {

            if ($authChecker->isGranted('ROLE_USER'))
                $contact->setAuthor($this->getUser()->getMyUserPage());
           
            // Enregistre le message dans la bdd
            $em = $this->get('doctrine_mongodb')->getManager();
            $em->persist($contact);
            $em->flush();

            return $this->render('theme/contact-confirm.html.twig');
        }
          
        return $this->render('theme/contact.html.twig', [
                                'form' => $formContact->createView(),
                                'type' => $type
        ]);
    }


    /**
     * Route : /map-embed/{search}", name="map-embed"
     * @Route("/map-embed/{search}", name="map-embed")
     */
    public function mapEmbed($search="", Request $request, AuthorizationCheckerInterface $authChecker)
    {

        //if (!$authChecker->isGranted('ROLE_USER')) { throw new AccessDeniedException(); }

        //if($search == "-") $search = "";
        //$search = preg_replace("/></", "#", $search);
        
        //$myUser = $this->getUser();
        //$myPosition = $myUser->getMyPosition();
        
        /*$aroundCoordinates = null;
        if($request->query->get('lat') != null && $request->query->get('lon') != null)
        $aroundCoordinates = array(floatval($request->query->get('lat')), 
                                    floatval($request->query->get('lon')));
        */
        
        /*$myAgenda = $request->query->get('myagenda') != null ? $request->query->get('myagenda') : false;
        $myEventsLinks = array();
        if($myAgenda == true){
            $myEventsLinks = array();
            $participates = $this->getUser()->getMyUserPage()->getRelations()->getParticipateTo()->toArray();
            $myEventsLinks = array_merge($myEventsLinks, $participates);
            $memberOf = $this->getUser()->getMyUserPage()->getRelations()->getFriends()->toArray();
            $myEventsLinks = array_merge($myEventsLinks, $memberOf);
            $adminOf = $this->getUser()->getMyUserPage()->getRelations()->getIsAdminOf()->toArray();
            $myEventsLinks = array_merge($myEventsLinks, $adminOf);
            $myEventsLinks = $this->objsToId($myEventsLinks);
        }*/

        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);

        $allRes = $pageRepo->getGlobalSearch($search,  
                                          $request->query->get('types'), 
                                          null, //$aroundCoordinates,
                                          null//floatval($request->query->get('radius')),
                                          //$startDate, 
                                          //$endDate,
                                          //$myAgenda,
                                          //$myEventsLinks
                                        );
        $res = $allRes["res"];
        $totalCount = $allRes["totalCount"];

        $emptyPage = new Page();
        $emptyPage->setIsActive(true);
        //if($aroundCoordinates != null && $renderPartial!="json"){
            foreach ($res as $key => $page) {
                if(!$page->isConfAuth("SHOW_GEOPOS", $emptyPage))
                    unset($res[$key]);
            }
        //}
        
        //dd($res);
        $jsonRes = $this->getJsonRes($res, $emptyPage);
        //dd($jsonRes);
        $params = array(
                "results" => $res,
                "totalCount" => $totalCount,
                'jsonRes' => $jsonRes,
                "yourSearch" => $search,
                //'myPosition' => $myPosition,
                'typeSearch' => Page::getTypeMainSearch()
            );

        if(!empty($request->query->get('types')))   $params["typesSearch"] = $request->query->get('types');
        //if($aroundCoordinates != null)              $params["coordinatesSearch"] = $aroundCoordinates;
        //if(!empty($request->query->get('radius')))  $params["radiusSearch"] = floatval($request->query->get('radius'));
       
        return $this->render('page/map-embed.html.twig', $params);

        //dd($jsonRes);
        /*
        if($renderPartial=="false"){
            return $this->render('page/search.html.twig', $params);
        }elseif($renderPartial=="json"){
            return $this->json(array('jsonRes' => $jsonRes));
        }else{
            return $this->render('page/blocks/search-res-list.html.twig', $params);
        }*/
    }
    /**
     * Convert search result objects to Json<br>
     * (to use datas in javascript, mainly for SIG to display datas on map)<br>
     * if you want your page appear in result, set excludeMe to false
     * @return array
     */
    private function getJsonRes($res, $userPage, $excludeMe=true){ 
        $jsonRes = array();
        //$myPageId = $this->getUser()->getMyUserPage()->getId();
        foreach ($res as $key => $value) {
            //if($excludeMe == false)// || $value->getId() != $myPageId)
            $jsonRes[] = $value->getJson($userPage);        
        }
        return $jsonRes;
    }


    /**
     * @Route("/setlocale/{_locale}", name="setlocale")
     */
    public function setLocaleAction(Request $request, $_locale = null)
    {
        if($_locale != null){
            $this->get('session')->set('_locale', $_locale);
        }
     
        $url = $request->headers->get('referer');
        if(empty($url)){
            $url = $this->container->get('router')->generate('index');
        }
        return $this->redirect($url);
    }

}
