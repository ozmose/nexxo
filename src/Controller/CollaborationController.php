<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Translation\TranslatorInterface;

use App\Form\CollaborationType;
use App\Entity\User;
use App\Entity\Page;
use App\Entity\Group;
use App\Entity\Collaboration;

class CollaborationController extends Controller
{
    /**
     * Route : /create-collaboration/{type}/{slug}, name="create-collaboration"
     * @Route("/create-collaboration/{type}/{slug}", name="create-collaboration")
     */
    public function createCollaboration($type="", $slug="", Request $request, TranslatorInterface $translator,
    									AuthorizationCheckerInterface $authChecker)
    {
        if (!$authChecker->isGranted('ROLE_USER')) {
        	//if user not connected
            $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
            $page = $pageRepo->findOneBy(array("slug" => $slug));
            //and page is private, deny access
            if($page->isPrivate() == true)
                throw new AccessDeniedException();
            else //or redirect to offline page version
                return $this->render('page/page-offline.html.twig', array("page" => $page));
        }

        //get the page
        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
        $page = $pageRepo->findOneBy(array("slug" => $slug));
        //if the slug page is not found
        if($page == null){
            return $this->render('page/page-not-found.html.twig');
        }
        //check report about this page
        $report = $page->getReport();
        if($report != null && $report->getStatus() == Report::STATUS_DISABLED){
            return $this->render('page/page-disabled.html.twig', array("page"=>$page));
        }
        //if the page is not an agora, you cant create RIC
        if($page->getType() != "agora" && $type == "ric"){
            return $this->redirectToRoute('page', array("slug"=>$page->getSlug()));
        }

        $myUser = $this->getUser();

        $newCollab = new Collaboration();

        $initAnswers = array($translator->trans("yes", array(), "collaboration"), 
                             $translator->trans("no", array(), "collaboration"), 
                             $translator->trans("white", array(), "collaboration"));
        for($i=0; $i<47; $i++) $initAnswers[] = "";
        $newCollab->setAnswers($initAnswers);

        $newCollab->setType($type);
        $isAdminOf = $this->getUser()->getMyUserPage()->getRelations()->getIsAdminOf()->toArray();
       
       	$form = null;
       	$form = $this->createForm(CollaborationType::class, $newCollab, 
                                    array("adminPage"=>array($page), "type"=>$type));
        
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $newCollab->setOwner($myUser);
            $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
            
            if($newCollab->getLongitude() != null && $newCollab->getLatitude() != null)
            $newCollab->setCoordinates([(float)$newCollab->getLongitude(), (float)$newCollab->getLatitude()]);

            $newCollab->setCreated(new \Datetime());
            $newCollab->setParentPage($page);
            $newCollab->setIsActive(true);
            $newCollab->clearAnswers();

            //add the current user page in admins
            //$newPage->getRelations()->addAdmin($myUser->getMyUserPage());
            //$userPage = $myUser->getMyUserPage();
            //$userPage->getRelations()->addIsAdminOf($newPage);

            // Enregistre la page dans la bdd
            $em = $this->get('doctrine_mongodb')->getManager();
            $em->persist($newCollab);
            $em->flush();

            //if the page is not private, send a notification to all my realFriends
            // if($newPage->isPrivate() == false){
            //     /* SEND NOTIFICATION */
            //     $notifObj = Notification::sendNotification(
            //                     $this->getUser()->getMyUserPage(), Notification::VERB_CREATE_PAGE,
            //                     $newPage->getId(), "page", null, null, $em, $this);
            // }

            return $this->redirectToRoute('page', array("slug"=>$slug, "view" => "collab-index"));
        }else{
           //dd($form->getErrors(true));
        }

        return $this->render("page/page.html.twig", 
            array("slug"=>$slug, 
            	  "page" => $page,
            	  "type" => $type,
            	  "view" => "collab-form",
            	  "subview" => null,
            	  'form' => $form->createView(),
                  'action' => 'create-collaboration'));
    }


    /**
     * Route : /edit-collaboration/{collabId}, name="edit-collaboration"
     * @Route("/edit-collaboration/{collabId}", name="edit-collaboration")
     */
    public function editCollaboration($collabId="", Request $request, TranslatorInterface $translator,
                                        AuthorizationCheckerInterface $authChecker)
    {
        if (!$authChecker->isGranted('ROLE_USER')) {
            //if user not connected
            $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
            $page = $pageRepo->findOneBy(array("slug" => $slug));
            //and page is private, deny access
            if($page->isPrivate() == true)
                throw new AccessDeniedException();
            else //or redirect to offline page version
                return $this->render('page/page-offline.html.twig', array("page" => $page));
        }

        $myUser = $this->getUser();

        
        $em = $this->get('doctrine_mongodb')->getManager();
        $collabRepo = $em->getRepository(Collaboration::class);
        $newCollab = $collabRepo->findOneBy(array("id" => $collabId));

        $initAnswers = $newCollab->getAnswers();
        for($i=0; $i<(50-sizeof($initAnswers)); $i++) $initAnswers[] = "";
        $newCollab->setAnswers($initAnswers);

        $isAdminOf = $this->getUser()->getMyUserPage()->getRelations()->getIsAdminOf()->toArray();
       
        $parentPage = $newCollab->getParentPage();

        //dd($newCollab);
        $form = null;
        $form = $this->createForm(CollaborationType::class, $newCollab);
        
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $newCollab->setOwner($myUser);
            $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
            
            if($newCollab->getLongitude() != null && $newCollab->getLatitude() != null)
            $newCollab->setCoordinates([(float)$newCollab->getLongitude(), (float)$newCollab->getLatitude()]);

            $newCollab->setCreated(new \Datetime());
            $newCollab->setIsActive(true);
            $newCollab->clearAnswers();

            //add the current user page in admins
            //$newPage->getRelations()->addAdmin($myUser->getMyUserPage());
            //$userPage = $myUser->getMyUserPage();
            //$userPage->getRelations()->addIsAdminOf($newPage);

            // Enregistre la collaboration dans la bdd
            $em = $this->get('doctrine_mongodb')->getManager();
            $em->persist($newCollab);
            $em->flush();

            //if the page is not private, send a notification to all my realFriends
            // if($newPage->isPrivate() == false){
            //     /* SEND NOTIFICATION */
            //     $notifObj = Notification::sendNotification(
            //                     $this->getUser()->getMyUserPage(), Notification::VERB_CREATE_PAGE,
            //                     $newPage->getId(), "page", null, null, $em, $this);
            // }

            return $this->redirectToRoute('page', array("slug"=>$parentPage->getSlug(), "view" => "collab-index"));
        }else{
          // dd($form->getErrors(true));
        }

        return $this->render("page/page.html.twig", 
            array("slug"=>$parentPage->getSlug(), 
                  "page" => $parentPage,
                  "type" => $parentPage->getType(),
                  "view" => "collab-form",
                  "subview" => null,
                  'form' => $form->createView(),
                  'action' => 'edit-collaboration',
                  'collabId' => $collabId));
    }

    /**
     * Route : /collab-single/{id}/{renderPartial}, name="collab-single"
     * @Route("/collab-single/{id}/{renderPartial}", name="collab-single")
     */
    public function collabSingle($id="", $renderPartial=false, Request $request, TranslatorInterface $translator,
                                        AuthorizationCheckerInterface $authChecker)
    {
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $em = $this->get('doctrine_mongodb')->getManager();
        $collabRepo = $em->getRepository(Collaboration::class);
        $collab = $collabRepo->findOneBy(array("id" => $id));

        return $this->render("collaboration/collab-single.html.twig", 
            array("collab"=>$collab, 
                  ));
    }


    
    /**
     * Route : /ric, name="ric"
     * @Route("/ric", name="ric")
     */
    public function collabGlobal(Request $request, TranslatorInterface $translator,
                                        AuthorizationCheckerInterface $authChecker)
    {
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $em = $this->get('doctrine_mongodb')->getManager();
        $collabRepo = $em->getRepository(Collaboration::class);
        $collabs = $collabRepo->findBy(array("type" => "ric"));

        return $this->render("collaboration/collab-global.html.twig", 
            array("collabs"=>$collabs, 
                  ));
    }



}