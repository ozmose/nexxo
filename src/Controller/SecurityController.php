<?php
 
namespace App\Controller;
 
use App\Entity\User;
use App\Entity\Page;
use App\Services\Mailer;
use App\Form\UserType;

use Symfony\Component\Routing\Annotation\Route;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Translation\TranslatorInterface;

use App\Form\ResettingType;
use App\Services\Helper;
use App\Entity\Relation;


class SecurityController extends Controller
{
    /**
     * @Route("/login", name="login")
     */
    public function login(Request $request,TranslatorInterface $translator,  AuthenticationUtils $authUtils)
    {
        //dd($this->getUser());
        //dd($this->getUser());
       
        // get the login error if there is one
        $error = $authUtils->getLastAuthenticationError();
        if($error != null)
            $request->getSession()->getFlashBag()
                            ->add('error', $translator->trans("E-mail or password incorrect", array(), "page"));
        //dd($error->getMessageKey());
        // last username entered by the user
        $lastUsername = $authUtils->getLastUsername();
        return $this->render('theme/account/login.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
        ));
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout(Request $request, AuthenticationUtils $authUtils)
    {
        //$locale = $request->getLocale();
        //$request->getSession()->set('_locale', $locale);
        //dd($locale);
        //dd("logout");
        //dd($this->getUser());
       
    }

     /**
     * @Route("/register", name="user_registration")
     */
    public function registerAction(Request $request, Mailer $mailer, 
                                    TranslatorInterface $translator, UserPasswordEncoderInterface $passwordEncoder)
    {
        // création du formulaire
        $user = new User();
        // instancie le formulaire avec les contraintes par défaut, 
        //+ la contrainte registration pour que la saisie du mot de passe soit obligatoire
        $form = $this->createForm(UserType::class, $user,[
           'validation_groups' => array('User', 'registration'),
        ]);
         
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
 
            $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
            $userRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(User::class);
            $userExists = $userRepo->findOneBy(array("email" => $user->getEmail()));
            
            $error = false;
            if($userExists != null){ $error = true;
                $request->getSession()->getFlashBag()->add('error', 
                    $translator->trans("This e-mail address is already in use : ", array(), "page").$user->getEmail());
            }

            $userExists2 = $userRepo->findOneBy(array("username" => $user->getUsername()));
            if($userExists2 != null){ $error = true;
                $request->getSession()->getFlashBag()->add('error', 
                    $translator->trans("This user name is already in use : ", array(), "page").$user->getUsername());          
            }

            if($error==true) return $this->redirectToRoute('user_registration');

            // Encode le mot de passe
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            $user->setPlainPassword("");
            
            $slug = Helper::getUniqueSlug($user->getRealUserName(), $pageRepo);

            //dd($slugExists);

            $myPage = new Page();
            $myPage->setName($user->getRealUserName());
            $myPage->setSlug($slug);
            $myPage->setOwner($user);
            $myPage->setType("user");
            $myPage->setIsPrivate(false);
            $myPage->getRelations()->addAdmin($myPage);
            $myPage->getRelations()->addIsAdminOf($myPage);
            $myPage->setCreated(new \Datetime());

            $user->addPage($myPage);

            $em = $this->get('doctrine_mongodb')->getManager();
            $em->persist($myPage);
            $em->persist($user);
            $em->flush();

            $msg =  $translator->trans("Your account is registred ! You can log in right now as a visitor.", array(), "page") ."<br>".
                    $translator->trans( "Activate your account by clicking on the validation link that we sent you by e-mail to start publishing on Nexxo.", array(), "page") ."<br>".
                    $translator->trans("If you do not validate your account within 7 days, it will be automatically deleted.", array(), "page");

            $request->getSession()->getFlashBag()->add('success', $msg);
 
            // on utilise le service Mailer créé précédemment
            $bodyMail = $mailer->createBodyMail('theme/account/mail/confirm-register.html.twig', [
                'page' => $myPage
            ]);
            

            $mailer->sendMessage('postmaster@nexxociety.co', $user->getEmail(), 
                                 $translator->trans('Nexxo : account created', array(), "mail"), $bodyMail);

            return $this->redirectToRoute('login', array('status' => 'register-success', 'email' => $user->getEmail()));
        }
 
        return $this->render(
            'theme/account/register.html.twig',
            array('form' => $form->createView())
        );
    }




     /**
     * @Route("/user/confirm-email/{slug}/{token}", name="user-confirm-email")
     */
    public function confirmEmailAction($slug, $token, Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $em = $this->get('doctrine_mongodb')->getManager();
        $pageRepo = $em->getRepository(Page::class);
        $page = $pageRepo->findOneBy(array("slug" => $slug));

        if($page != null && $page->getOwner()->getToken() == $token && $token != ""){
            
            $page->getOwner()->setToken("");
            $page->setIsActive(true);
            $page->getOwner()->setIsActive(true);
            $em->persist($page);
            $em->flush();

            $request->getSession()->getFlashBag()->add('success', "Validation e-mail ok, votre compte est activé.");
 
            return $this->redirectToRoute('login');
        }
 
        $request->getSession()->getFlashBag()->add('error', "Validation e-mail : erreur.");
        return $this->redirectToRoute('login');
    }


}
