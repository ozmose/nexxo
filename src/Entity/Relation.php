<?php 
namespace App\Entity;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use App\Entity\User;
use App\Entity\Page;

/** @MongoDB\EmbeddedDocument */
class Relation
{

    /** 
     * Reference the Page wich own (publicate) the News
     * @MongoDB\ReferenceOne(targetDocument="Page") 
     */
    private $parent;


    /** 
     * Reference the admins of the Page
     * @MongoDB\ReferenceMany(targetDocument="Page") 
     */
    private $admins;
    /**
     * @MongoDB\ReferenceMany(targetDocument="Page")
     */
    private $isAdminOf;
    /** 
     * Reference the admins of the Page
     * @MongoDB\ReferenceMany(targetDocument="Page") 
     */
    private $adminsRequests;


    /** 
     * Reference the friends of the Page
     * @MongoDB\ReferenceMany(targetDocument="Page") 
     */
    private $friends;
    /** 
     * Reference the friendsRequests of the Page
     * @MongoDB\ReferenceMany(targetDocument="Page") 
     */
    private $friendsRequests;


    /** 
     * Reference the followers of the Page
     * @MongoDB\ReferenceMany(targetDocument="Page") 
     */
    private $followers;
    /** 
     * Reference the follows of the Page
     * @MongoDB\ReferenceMany(targetDocument="Page") 
     */
    private $follows;


    /** 
     * Reference the participants of the Page (event)
     * @MongoDB\ReferenceMany(targetDocument="Page") 
     */
    private $participants;
    /** 
     * Reference the event Page where this Page participage
     * @MongoDB\ReferenceMany(targetDocument="Page") 
     */
    private $participateTo;


    
    /** 
     * Reference the likers of the Page
     * @MongoDB\ReferenceMany(targetDocument="Page") 
     */
    private $likers;
    /** 
     * Reference the page liked by the Page
     * @MongoDB\ReferenceMany(targetDocument="Page") 
     */
    private $likes;

    /** 
     * Reference the Pages i am able to chat with
     * @MongoDB\ReferenceMany(targetDocument="Page") 
     */
    private $chatContacts;


    public function __construct($parent)
    {
        //save the parent Page to enable access to all attributes
        //exemple : $this->parent->getOwner();
        $this->parent = $parent;

        $this->friends = array();
        $this->friendsRequests = array();
        $this->admins = array();
        $this->adminsRequests = array();
        $this->isAdminOf = array();
        $this->followers = array();
        $this->follows = array();
        $this->participants = array();
        $this->participateTo = array();
        $this->likers = array();
        $this->likes = array();
        $this->chatContacts = array();
    }

    /****************************** ADMINS ******************************************/

    /**
     * get admin
     */
    public function getAdmins()
    {   
        return $this->admins;
    }

    /**
     * add admin
     */
    public function addAdmin(Page $page)
    {
        
        if(!$this->getIsAdmin($page->getSlug())){
            $this->admins[] = $page;
            //all admins are friends
            $this->addFriend($page);
            $page->getRelations()->addFriend($this->parent);
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * add admin
     */
    public function addIsAdminOf(Page $page)
    {
        if(!$this->getImAdminOf($page->getSlug())){
            $this->isAdminOf[] = $page;
            //dd("true addIsAdminOf");
            return true;
        }
        else{
            //dd("false addIsAdminOf");
            return false;
        }
    }

    public function getIsAdminOf()
    {
        return $this->isAdminOf;
    }

    public function getImAdminOf($slug)
    {
        foreach ($this->isAdminOf as $key => $page) {
            if($page->getSlug() == $slug) return true;
        }
        return false;
    }
    public function getImAdminOfId($id)
    {
        foreach ($this->isAdminOf as $key => $page) {
            if($page->getId() == $id) return true;
        }
        return false;
    }
    public function getIsAdmin($slug)
    {
        foreach ($this->admins as $key => $page) {
            if($page->getSlug() == $slug) return true;
        }
        return false;
    }
    /* ############# REQUESTS ############# */

        /**
         * get adminsRequests
         */
        public function getAdminsRequests()
        {   
            return $this->adminsRequests;
        }

        /**
         * add AdminsRequests
         */
        public function addAdminRequest(Page $page)
        {
            //add admin if the Page is not already a admin
            //and if it is not the parent (you cant be admin with yourself)
            if(empty($this->adminsRequests) || !$this->adminsRequests->contains($page) && $page != $this->parent){
                $this->adminsRequests[] = $page;
                return true;
            }
            else{
                return false;
            }
        }

        /**
         * remove adminsRequests
         */
        public function removeAdminRequest(Page $page)
        {   
            $this->adminsRequests->removeElement($page);
        }


    /**
     * remove admins
     */
    public function removeAdmin(Page $page)
    {   
        $this->admins->removeElement($page);
    }

    /**
     * remove admins
     */
    public function removeIsAdminOf(Page $page)
    {   
        $this->isAdminOf->removeElement($page);
    }


    /****************************** FRIENDS ******************************************/


    /**
     * get friends : return all pages, no matter of his type
     */
    public function getFriends()
    {   
        return $this->friends;
    }


    /**
     * get friends : return only page with type Page::TYPE_USER
     */
    public function getRealFriends()
    {
        $friends = array();
        foreach ($this->friends as $key => $page) {
            if($page->getType() == Page::TYPE_USER) $friends[] = $page;
        }
        return $friends;
    }

    /**
     * add friends
     */
    public function addFriend(Page $page)
    {
        //add friend if the Page is not already a friend
        //and if it is not the parent (you cant be friend with yourself)
        //dump($page->getId()); dump($this->parent->getId());
        if(!$this->getImFriend($page->getSlug()) && $page != $this->parent){
            $this->friends[] = $page;
            $this->addChatContacts($page);
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * remove friends
     */
    public function removeFriend(Page $page)
    {   
        $this->friends->removeElement($page);
    }

    public function getIsMemberOf()
    {
        $members = array();
        foreach ($this->friends as $key => $page) {
            if($page->getType() != Page::TYPE_USER) $members[] = $page;
        }
        return $members;
        //return $this->isMemberOf;
    }

    public function getImFriend($slug)
    {
        foreach ($this->friends as $key => $page) {
            if($page->getSlug() == $slug) return true;
        }
        return false;
    }

    public function getImMember($slug)
    {
        foreach ($this->friends as $key => $page) {
            if($page->getSlug() == $slug && $page->getType != Page::TYPE_USER) 
                return true;
        }
        return false;
    }
    
    public function getImFriendId($id)
    {
        foreach ($this->friends as $key => $page) {
            if($page->getId() == $id) return true;
        }
        return false;
    }

    /* ############# REQUESTS ############# */

        /**
         * get friendsRequests
         */
        public function getFriendsRequests()
        {   
            return $this->friendsRequests;
        }

        /**
         * add friendsRequest
         */
        public function addFriendRequest(Page $page)
        {
            //add friend if the Page is not already a friend
            //and if it is not the parent (you cant be friend with yourself)
            if(empty($this->friendsRequests) || !$this->friendsRequests->contains($page) && $page != $this->parent){
                $this->friendsRequests[] = $page;
                return true;
            }
            else{
                return false;
            }
        }

        /**
         * remove friendsRequests
         */
        public function removeFriendRequest(Page $page)
        {   
            $this->friendsRequests->removeElement($page);
        }


    /****************************** FOLLOWERS ****************************************/


    /**
     * get followers
     */
    public function getFollowers()
    {   
        return $this->followers;
    }

    /**
     * add followers
     */
    public function addFollower(Page $page)
    {
        //add follower if the Page is not already a follower
        //and if it is not the parent (you cant be follower of yourself)
        //dd($this->inFollowers($page->getSlug()));
        if(!$this->inFollowers($page->getSlug())){
            $this->followers[] = $page;
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * remove followers
     */
    public function removeFollower(Page $page)
    {   
        $this->followers->removeElement($page);
    }

    /**
     * remove followers
     */
    public function removeFollow(Page $page)
    {   
        $this->follows->removeElement($page);
        return true;
    }

    /**
     * add follows
     */
    public function addFollows(Page $page)
    {
        if(!$this->inFollows($page->getSlug())){
            $this->follows[] = $page;
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * get follows
     */
    public function getFollows()
    {   
        return $this->follows;
    }

    public function inFollows($slug)
    {
        foreach ($this->follows as $key => $page) {
            if($page->getSlug() == $slug) return true;
        }
        return false;
    }

    public function inFollowers($slug)
    {
        foreach ($this->followers as $key => $page) {
            if($page->getSlug() == $slug) return true;
        }
        return false;
    }

    public function inFollowsId($id)
    {
        foreach ($this->follows as $key => $page) {
            if($page->getId() == $id) return true;
        }
        return false;
    }

    public function inFollowersId($id)
    {
        foreach ($this->followers as $key => $page) {
            if($page->getId() == $id) return true;
        }
        return false;
    }


    /****************************** LIKES ***************************************/


    /**
     * get likes
     */
    public function getLikers()
    {   
        return $this->likers;
    }

    /**
     * add likes
     */
    public function addLiker(Page $page)
    {
        if(!$this->getImLiker($page->getSlug())){
            $this->likers[] = $page;
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * remove likers
     */
    public function removeLiker(Page $page)
    {   
        //dump("remove liker");dump($page->getSlug());
        return $this->likers->removeElement($page);
    }

    /**
     * remove likes
     */
    public function removeLike(Page $page)
    {   
        //dump("removeLike");dump($page->getSlug());
        return $this->likes->removeElement($page);
    }



    /**
     * add likes
     */
    public function addLike(Page $page)
    {
        //dump("ILIKE?"); dump($this->getILike($page->getSlug()));dump("???"); 
        if(!$this->getILike($page->getSlug())){
            $this->likes[] = $page;
            return true;
        }
        else{
            return false;
        }
    }

    public function getLikes()
    {
        return $this->likes;
    }

    public function getILike($slug)
    {
        foreach ($this->likes as $key => $page) {
            //dump($page->getSlug()); dump($slug); dump("---");
            if($page->getSlug() == $slug) return true;
        }
        return false;
    }
    public function getImLiker($slug)
    {
        foreach ($this->likers as $key => $page) {
            if($page->getSlug() == $slug) return true;
        }
        return false;
    }

    /****************************** PARTICIPANTS *************************************/


    /**
     * get participants
     */
    public function getParticipants()
    {   
        return $this->participants;
    }

    /**
     * add participants
     */
    public function addParticipant(Page $page)
    {
        //dd($this->getInParticipants($page->getSlug()));
        //add participant if the Page is not already a participant
        //and if it is not the parent (you cant be participant of yourself)
        if(!$this->getInParticipants($page->getSlug()) &&
            ($this->parent->getType() == Page::TYPE_EVENT || $this->parent->getType() == Page::TYPE_PROJECT)){
            $this->participants[] = $page;
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * remove participants
     */
    public function removeParticipant(Page $page)
    {   
        $this->participants->removeElement($page);
    }
    /**
     * remove participateTo
     */
    public function removeParticipateTo(Page $page)
    {   
        $this->participateTo->removeElement($page);
    }


    public function getParticipateTo()
    {
        return $this->participateTo;
    }

    public function getInParticipateTo($slug)
    {
        foreach ($this->participateTo as $key => $page) {
            if($page->getSlug() == $slug) return true;
        }
        return false;
    }
    public function getInParticipants($slug)
    {
        foreach ($this->participants as $key => $page) {
            if($page->getSlug() == $slug) return true;
        }
        return false;
    }


    /**
     * add participateTo
     */
    public function addParticipateTo(Page $page)
    {
        if( !$this->getInParticipateTo($page->getSlug()) &&
            ($page->getType() == Page::TYPE_EVENT || $page->getType() == Page::TYPE_PROJECT)){
            $this->participateTo[] = $page;
            return true;
        }
        else{
            return false;
        }
    }



    /**
     * get chatContacts
     */
    public function getChatContacts()
    {   
        foreach ($this->chatContacts as $key => $page) {
           if($page->getIsActive() == false) 
                unset($this->chatContacts[$key]);
        }
        return $this->chatContacts;
    }

    /**
     * init chatContacts
     */
    public function initChatContacts()
    {   
        $this->chatContacts = array();
        return $this->chatContacts;
    }

    /**
     * add chatContacts
     */
    public function addChatContacts(Page $page)
    {
        //add chatContact if the Page is not already in the list
        //and if it is not the parent (you cant chat with yourself)
        //echo "addChatContacts ".$page->getSlug()." to ".$this->parent->getRelations()->getSlug();
        if(!$this->getInChatContacts($page->getSlug()) && 
            $page != $this->parent){
            $this->chatContacts[] = $page;

            if($page->getType() == Page::TYPE_USER)
                $page->getRelations()->addChatContacts($this->parent);

            return true;
        }
        else{
            return false;
        }
    }

    /**
     * remove chatContacts
     */
    public function removeChatContacts(Page $page)
    {   
        $this->chatContacts->removeElement($page);
    }

    public function getInChatContacts($slug)
    {
        foreach ($this->chatContacts as $key => $page) {
            if($page->getSlug() == $slug) return true;
        }
        return false;
    }
    


    /****************************** TEST *************************************/




    public function test(){
        //dump($this->removeFriends($this->parent));
        //dump($this->addFriends($this->parent));
    }

}
