<?php

namespace App\Entity;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use App\Services\Helper;

/**
 * @MongoDB\Document(repositoryClass="App\Repository\GroupRepository")
 */
class ChatMessage
{

/*var clientInformation = {
        senderUsername: APP_USERNAME,
        senderSlug: APP_USER_SLUG,
        senderToken: APP_USER_CHATTOKEN,
        receiverType: "",
        receiverSlug: "",
        senderThumb: APP_USER_THUMB
        // You can add more information in a static object
    };*/

	/**
     * @MongoDB\Id(strategy="auto")
     */
	private $id;

    /**
     * The name of the group
     * @MongoDB\Field(type="string")
     */
    private $message;

    /**
     * The name of the group
     * @MongoDB\Field(type="string")
     */
    private $senderUsername;
    
    /**
     * The name of the group
     * @MongoDB\Field(type="string")
     */
    private $senderThumb;

    /**
     * The name of the group
     * @MongoDB\Field(type="string")
     */
    private $senderSlug;

    /**
     * The name of the group
     * @MongoDB\Field(type="string")
     */
    private $receiverSlug;

    /**
     * The name of the group
     * @MongoDB\Field(type="boolean")
     */
    private $toRead;

    /**
     * The name of the group
     * @MongoDB\Field(type="date")
     */
    private $created;


    public function __construct($chatMessage)
    {   
        if(is_array($chatMessage)){
            //dd($chatMessage);
            $this->message = Helper::encrypt($chatMessage["message"]);
            $this->senderUsername = $chatMessage["senderUsername"];
            $this->senderThumb = $chatMessage["senderThumb"];
            $this->senderSlug = $chatMessage["senderSlug"];
            $this->receiverSlug = $chatMessage["receiverSlug"];
            $this->toRead = isset($chatMessage["toRead"]) ? $chatMessage["toRead"] : true;
            $this->created = new \Datetime();
        }else{
            $this->message = Helper::encrypt($chatMessage->message);
            $this->senderUsername = $chatMessage->senderUsername;
            $this->senderThumb = $chatMessage->senderThumb;
            $this->senderSlug = $chatMessage->senderSlug;
            $this->receiverSlug = $chatMessage->receiverSlug;
            $this->toRead = isset($chatMessage->toRead) ? $chatMessage->toRead : true;
            $this->created = new \Datetime();
            //dd($this->message);
        }
    }



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        $message = Helper::decrypt($this->message);
        return Helper::linkToHtml($message);
    }

    /**
     * @param mixed $message
     *
     * @return self
     */
    public function setMessage($message)
    {
        $this->message = Helper::encrypt($message);
        //$this->message = $message;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSenderUsername()
    {
        return $this->senderUsername;
    }

    /**
     * @param mixed $senderUsername
     *
     * @return self
     */
    public function setSenderUsername($senderUsername)
    {
        $this->senderUsername = $senderUsername;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSenderThumb()
    {
        return $this->senderThumb;
    }

    /**
     * @param mixed $senderThumb
     *
     * @return self
     */
    public function setSenderThumb($senderThumb)
    {
        $this->senderThumb = $senderThumb;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSenderSlug()
    {
        return $this->senderSlug;
    }

    /**
     * @param mixed $senderSlug
     *
     * @return self
     */
    public function setSenderSlug($senderSlug)
    {
        $this->senderSlug = $senderSlug;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getReceiverSlug()
    {
        return $this->receiverSlug;
    }

    /**
     * @param mixed $receiverSlug
     *
     * @return self
     */
    public function setReceiverSlug($receiverSlug)
    {
        $this->receiverSlug = $receiverSlug;

        return $this;
    }


    /**
     * @return mixed
     */
    public function getToRead()
    {
        return $this->toRead;
    }

    /**
     * @param mixed $toRead
     *
     * @return self
     */
    public function setToRead($toRead)
    {
        $this->toRead = $toRead;
        return $this;
    }



    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     *
     * @return self
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }



    /**
    * Return a clean array with data to be used in view by Javascript<br>
    * Currently it is used for SIG, to display data on the map 
    */
    public function getJson(): array
    {
        $now = new \DateTime();
        return array(
           "id" => $this->id,
           "message" => $this->getMessage(),
           "senderUsername" => $this->senderUsername,
           "senderSlug" => $this->senderSlug,
           "receiverSlug" => $this->receiverSlug,
           "senderThumb" => $this->senderThumb,
           "toread" => $this->toRead,
           "created" => $this->created,//."h".$this->created->format('i'),
           "createdDisplay" => $this->created->format('d/m · H')."h".$this->created->format('i'),
           //"created" => date_diff($now, $this->created)->format('%R%a days')
        );
    }
}
