<?php

namespace App\Entity;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * @MongoDB\Document(repositoryClass="App\Repository\GroupRepository")
 */
class Group
{

	/**
     * @MongoDB\Id(strategy="auto")
     */
	private $id;

	/**
     * The name of the group
     * @MongoDB\Field(type="string")
     */
	private $name;

    /** 
     * Reference the Page wich own this groupe (the creator)
     * @MongoDB\ReferenceOne(targetDocument="Page", inversedBy="groups") 
     */
    private $owner;


    /** 
     * References all member Pages of the group
     * @MongoDB\ReferenceMany(targetDocument="Page", inversedBy="inGroups") 
     */
    private $members;


    public function __construct()
    {
        
    }

    
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = strip_tags($name);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $owner
     *
     * @return self
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
        return $this;
    }


    /**
     * get members
     */
    public function getMembers()
    {   
        return $this->members;
    }

    /**
     * add member
     */
    public function addMember(Page $page)
    {   
        $isNotYet = !isset($this->members)  || !$this->members->contains($page);
        if( ($isNotYet)){
            $this->members[] = $page;
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * remove member
     */
    public function removeMember(Page $page)
    {   
        $this->members->removeElement($page);
    }

    public function __toString()
    {
        return strval($this->name);
    }


}