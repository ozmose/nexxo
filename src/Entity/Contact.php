<?php

namespace App\Entity;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


use App\Entity\Group;
use App\Entity\CommentStream;
use App\Services\Helper;
use App\Entity\Report;


/**
 * @MongoDB\Document(repositoryClass="App\Repository\ContactRepository")
 */
class Contact
{

	/**
     * @MongoDB\Id(strategy="auto")
     */
	private $id;

    /**
     * Main text (message)
     * @MongoDB\Field(type="string")
     */
    private $object;

    /**
     * Main text (message)
     * @MongoDB\Field(type="string")
     */
    private $text;
    
    /**
     * Main text (message)
     * @MongoDB\Field(type="string")
     */
    private $type;
    
    /** 
     * Reference the Page wich own (publicate) the News
     * @MongoDB\ReferenceOne(targetDocument="Page", inversedBy="news") 
     */
    private $author;

    /**
     * isSharable = false : cant share this news
     * otherwise is false
     * @MongoDB\Field(type="boolean")
     *
     */
    private $toRead;

    /**
     * Date initialized when the News is created
     * Set one time, never modified after initialization
     * @MongoDB\Field(type="date")
     */
    private $created;


    public function __construct($type)
    {
        $this->type = $type;
        $this->toRead = true;
        $this->created = new \Datetime();
    }



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @param mixed $object
     *
     * @return self
     */
    public function setObject($object)
    {
        $this->object = $object;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     *
     * @return self
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     *
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     *
     * @return self
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getToRead()
    {
        return $this->toRead;
    }

    /**
     * @param mixed $toRead
     *
     * @return self
     */
    public function setToRead($toRead)
    {
        $this->toRead = $toRead;

        return $this;
    }


    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     *
     * @return self
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     *
     * @return self
     */
    public function getTypeIcon()
    {
        if($this->type == "bug") return "bug";
        if($this->type == "suggestion") return "comment";
        if($this->type == "message") return "envelope";
    }

    /**
     *
     * @return self
     */
    public function getToReadColor()
    {
        return $this->toRead ? "green" : "blue";
    }

    /**
     * transform hashtag string in text to html (<span>)
     * you can define css classes to be added in the result
     * @return mixed
     */
    public function getTextHtml($class="")
    {
        $text = Helper::hashtagToHtml($this->text, $class);
        $text = Helper::linkToHtml($text);
        return $text;
    }
    
}
