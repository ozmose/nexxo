<?php

namespace App\Entity;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * @MongoDB\Document(repositoryClass="App\Repository\GroupRepository")
 */
class Media
{

/*var clientInformation = {
        senderUsername: APP_USERNAME,
        senderSlug: APP_USER_SLUG,
        senderToken: APP_USER_CHATTOKEN,
        receiverType: "",
        receiverSlug: "",
        senderThumb: APP_USER_THUMB
        // You can add more information in a static object
    };*/

	/**
     * @MongoDB\Id(strategy="auto")
     */
	private $id;

    /**
     * The name of the group
     * @MongoDB\Field(type="string")
     */
    private $title;

    /**
     * The name of the group
     * @MongoDB\Field(type="string")
     */
    private $href;

    /**
     * The name of the group
     * @MongoDB\Field(type="string")
     */
    private $imgSrc;

    /**
     * The name of the group
     * @MongoDB\Field(type="string")
     */
    private $channelDescription;

    /**
     * The name of the group
     * @MongoDB\Field(type="string")
     */
    private $channelName;

    /**
     * The name of the group
     * @MongoDB\Field(type="string")
     */
    private $channelThumb;
    /**
     * The name of the group
     * @MongoDB\Field(type="string")
     */
    private $channelUrl;
    
    /**
     * Date initialized when the News is created
     * Set one time, never modified after initialization
     * @MongoDB\Field(type="date")
     */
    private $created;


    public function __construct($media){
        $this->title        = $media["title"];
        $this->href         = $media["href"];
        $this->imgSrc       = $media["imgSrc"];
        $this->channelName  = $media["channelName"];
        $this->channelThumb = $media["channelThumb"];
        $this->channelUrl   = $media["channelUrl"];
        $this->channelDescription = $media["channelDescription"];
        $this->created      = $media["created"];
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     *
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getHref()
    {
        return $this->href;
    }

    /**
     * @param mixed $href
     *
     * @return self
     */
    public function setHref($href)
    {
        $this->href = $href;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getImgSrc()
    {
        return $this->imgSrc;
    }

    /**
     * @param mixed $imgSrc
     *
     * @return self
     */
    public function setImgSrc($imgSrc)
    {
        $this->imgSrc = $imgSrc;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getChannelDescription()
    {
        return $this->channelDescription;
    }

    /**
     * @param mixed $channelDescription
     *
     * @return self
     */
    public function setChannelDescription($channelDescription)
    {
        $this->channelDescription = $channelDescription;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getChannelName()
    {
        return $this->channelName;
    }

    /**
     * @param mixed $channelName
     *
     * @return self
     */
    public function setChannelName($channelName)
    {
        $this->channelName = $channelName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getChannelThumb()
    {
        return $this->channelThumb;
    }

    /**
     * @param mixed $channelThumb
     *
     * @return self
     */
    public function setChannelThumb($channelThumb)
    {
        $this->channelThumb = $channelThumb;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getChannelUrl()
    {
        return $this->channelUrl;
    }

    /**
     * @param mixed $channelUrl
     *
     * @return self
     */
    public function setChannelUrl($channelUrl)
    {
        $this->channelUrl = $channelUrl;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     *
     * @return self
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }


    /**
     * return the list of all channels
     */
    public static function getChannels()
    {
        return array(   "https://www.youtube.com/user/elfuegoo/videos",                    //partager c sympa
                        "https://www.youtube.com/channel/UCGl2QLR344ry4Y20RV9dM3g/videos", //professeur feuillage
                        "https://www.youtube.com/user/NicolasMeyrieux/videos",             //la barbe
                        "https://www.youtube.com/channel/UCVeMw72tepFl1Zt5fvf9QKQ/videos", //osons causer
                        "https://www.youtube.com/channel/UC9hHeywcPBnLglqnQRaNShQ/videos", //fil d'actu
                        "https://www.youtube.com/user/thinkerview/videos",                 //thinkerview
                        "https://www.youtube.com/channel/UC0i7t1CC7T0xheeBahaWZYQ/videos", //next
                        "https://www.youtube.com/channel/UCnIGrvSrQF7uCovnIm8LCSQ/videos", //DemosKratos
                        "https://www.youtube.com/channel/UCLHlsGMNqgfZbVFjRoy8_iA/videos", //Le poste
                        "https://www.youtube.com/channel/UC-2EkisRV8h9KsHpslQ1gXA/videos", //Et tout le monde s'en fou
                        "https://www.youtube.com/channel/UC9NB2nXjNtRabu3YLPB16Hg/videos", //jsuis pas content

                    );
    }
}