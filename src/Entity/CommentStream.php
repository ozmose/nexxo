<?php

namespace App\Entity;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


use App\Entity\Comment;
use App\Entity\Report;
use App\Services\Helper;


/**
 * @MongoDB\Document(repositoryClass="App\Repository\NewsRepository")
 */
class CommentStream
{

    /**
     * @MongoDB\Id(strategy="auto")
     */
    private $id;

    /**
     * @MongoDB\Field(type="string")
     */
    private $parentId;

    /**
     * @MongoDB\Field(type="string")
     */
    private $parentType;

    /** 
     * Reference a list of Page author of comments
     * @MongoDB\ReferenceMany(targetDocument="Page", inversedBy="comments") 
     */
    private $authors;

    /**
     * Reference all Comments
     * @MongoDB\EmbedMany(targetDocument="Comment") 
     */
    private $comments;


    public function __construct()
    {
        /*$this->TYPES_AVAILABLES = array(Page::TYPE_USER,      Page::TYPE_ASSOCIATION, Page::TYPE_BUSINESS, 
                                        Page::TYPE_FREEGROUP, Page::TYPE_INSTITUTION, Page::TYPE_PROJECT, 
                                        Page::TYPE_EVENT);*/
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getParentType()
    {
        return $this->parentType;
    }


    /**
     * @param mixed $parentType
     * @return self
     */
    public function setParentType($parentType)
    {
        $this->parentType = $parentType;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getParentId()
    {
        return $this->parentType;
    }


    /**
     * @param mixed $parentId
     * @return self
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;
        return $this;
    }


    /**
     * get comments
     */
    public function getComments()
    {   //dd($this->comments);
        return $this->comments != null ? $this->comments : array();
    }

    /**
     * set comments
     */
    public function setComments($comments)
    {   
        $this->comments = $comments;
        return $this;
    }

    /**
     * add comment to the stream
     */
    public function addComment(Comment $comment)
    {   
        $comment->setParentStream($this);
        $this->comments[] = $comment;
        $this->addAuthor($comment->getAuthor());
    }

    /**
     * remove comment from the stream
     */
    public function removeComment(Comment $comment)
    {   
        foreach ($this->getComments() as $key => $com) {
            if($comment->getId() == $com->getId()){
                $this->comments->removeElement($com);
                return true;
            }
            $answer = $com->removeComment($comment);
            
            if($answer != false){ return true; }
        }
        return false;
    }


    /**
     * add author to the authors list
     */
    public function addAuthor(Page $author)
    {   
        $this->authors[] = $author;
    }

    /**
     * get authors
     */
    public function getAuthors()
    {   
        return $this->authors;
    }

    /**
     * remove author
     */
    public function removeAuthor(Page $author)
    {   
        $this->authors->removeElement($author);
    }

    /**
     * @return mixed
     */
    public function getCommentById($id)
    {
        foreach ($this->getComments() as $key => $comment) {
             //dump($comment->getId()); dump($id);
             if($comment->getId() == $id) return $comment;
             $answer = $comment->getCommentById($id);
             if($answer != false) return $answer;
         }
         return false;
    }

    /**
     * @return mixed
     */
    public function setCommentById($id, Comment $com)
    {   
        $comments = $this->getComments();
        foreach ($comments as $key => $comment) {
            //dump($comment->getId()); dump($id);
            //si c'est le bon commentaire, on le remplace
            if($comment->getId() == $id){
                $comments[$key] = $com;
                return true;
            }else{ //sinon on regarde dans les réponses de ce commentaire
                $answer = $comment->setCommentById($id, $com);
                if($answer != false){
                   return true;
                }
            }
         }
         return false;
    }

    public function cleanRemove($em){
        //dump("CS: start remove all comments");
        foreach ($this->getComments() as $key => $comment) {
            $comment->cleanRemove($em);
        }
        $em->remove($this);
        //$em->flush($this);
    }
}



