<?php

namespace App\Entity;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


use App\Entity\Comment;
use App\Entity\Report;
use App\Services\Helper;


/** 
 * @MongoDB\EmbeddedDocument 
 */
class Comment
{
    /**
     * @MongoDB\Id
     */
    private $id;

    /** 
     * Reference the CommentStream wich own the Comment
     * @MongoDB\ReferenceOne(targetDocument="CommentStream") 
     */
    private $parentStream;

    /**
     * Main text (message)
     * @MongoDB\Field(type="string")
     */
    private $text;

    /**
     * List of tag detected in the text (not in form / auto-generated before save)
     * @MongoDB\Field(type="collection")
     */
    private $tags;

    
    /** 
     * Reference the Page wich own (publicate) the Comment
     * @MongoDB\ReferenceOne(targetDocument="Page", inversedBy="comments") 
     */
    private $author;


    /** 
     * Reference the Page wich signed the Comment
     * @MongoDB\ReferenceOne(targetDocument="Page", inversedBy="commentsSigned") 
     */
    private $signed;


    /** 
     * Reference the Pages (user) who like the Comment
     * @MongoDB\ReferenceMany(targetDocument="Page", inversedBy="iLikeIt") 
     */
    private $likes;

    /** 
     * Reference the Pages (user) who dislikes the Comment
     * @MongoDB\ReferenceMany(targetDocument="Page", inversedBy="iDislikeIt") 
     */
    private $dislikes;

    /**
     * Reference all Comments answers
     * @MongoDB\EmbedMany(targetDocument="Comment") 
     */
    private $answers;

    /**
     * When isActive == false : the Comment is not displayed<br>
     * @MongoDB\Field(type="boolean")
     *
     */
    private $isActive;

    /**
     * Owner is the User who created the page
     * @MongoDB\ReferenceOne(targetDocument="Report") 
     */
    private $report;
    
    /**
     * Date initialized when the Comment is created
     * Set one time, never modified after initialization
     * @MongoDB\Field(type="date")
     */
    private $created;


    public function __construct()
    {
        $this->isActive = true;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getParentStream()
    {
        return $this->parentStream;
    }

    /**
     * @param mixed $parentStream
     * @return self
     */
    public function setParentStream(CommentStream $parentStream)
    {
        $this->parentStream = $parentStream;
        return $this;
    }

    

    /**
     * @return mixed
     */
    public function getText()
    {
        return Helper::decrypt($this->text);
    }

    /**
     * transform hashtag string in text to html (<span>)
     * you can define css classes to be added in the result
     * @return mixed
     */
    public function getTextHtml($class="")
    {
        $text = Helper::decrypt($this->text);
        $text = Helper::hashtagToHtml($text, $class);
        $text = Helper::linkToHtml($text);
        return $text;
    }

    /**
     * @param mixed $name
     * @return self
     */
    public function setText($text)
    {
        $stripedText = strip_tags($text);
        $this->text = Helper::encrypt($stripedText);

        return $this;
    }


    /**
     * @return mixed
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param mixed $tags
     * @return self
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     * @return self
     */
    public function setAuthor($author)
    {
        $this->author = $author;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getSigned()
    {
        return $this->signed;
    }

    /**
     * @param mixed $signed
     * @return self
     */
    public function setSigned($signed)
    {
        $this->signed = $signed;
        return $this;
    }


    /**
     * get likes
     */
    public function getLikes()
    {   
        return $this->likes;
    }

    /**
     * add likes
     */
    public function addLike(Page $page)
    {
        //add follower if the Page is not already a follower
        //and if it is not the parent (you cant be follower of yourself)
        if(empty($this->likes) || !$this->likes->contains($page)){
            $this->likes[] = $page;
            $this->removeDislike($page);
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * remove likes
     */
    public function removeLike(Page $page)
    {   
        $this->likes->removeElement($page);
    }

    /**
     * clean remove likes (++remove likes on answers)
     */
    public function cleanRemoveLike(Page $page)
    {   
        $this->likes->removeElement($page);
        foreach ($this->answers as $key => $answer) {
            $answer->cleanRemoveLike($page);
        }
    }


    /**
     * get dislikes
     */
    public function getDislikes()
    {   
        return $this->dislikes;
    }

    /**
     * add dislikes
     */
    public function addDislike(Page $page)
    {
        //add follower if the Page is not already a follower
        //and if it is not the parent (you cant be follower of yourself)
        if(empty($this->dislikes) || !$this->dislikes->contains($page)){
            $this->dislikes[] = $page;
            $this->removeLike($page);
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * remove dislikes
     */
    public function removeDislike(Page $page)
    {   
        $this->dislikes->removeElement($page);
    }

    /**
     * clean remove likes (++remove likes on answers)
     */
    public function cleanRemoveDislike(Page $page)
    {   
        $this->dislikes->removeElement($page);
        foreach ($this->answers as $key => $answer) {
            $answer->cleanRemoveLike($page);
        }
    }


    /**
     * get answers
     */
    public function getAnswers()
    {   
        return $this->answers;
    }

    /**
     * set answers
     */
    public function setAnswers($answers)
    {   
        $this->answers = $answers;
        return $this;
    }

    /**
     * add comment to the stream
     */
    public function addAnswer($answer)
    {   
        $this->parentStream->addAuthor($answer->getAuthor());
        $answer->setParentStream($this->parentStream);
        $this->answers[] = $answer;
    }

    /**
     * remove comment from the stream
     */
    public function removeAnswer(Comment $answer)
    {   
        $this->answers->removeElement($answer);
    }


    /**
     * @return mixed
     */
    public function getCommentById($id)
    {
        foreach ($this->getAnswers() as $key => $comment) {
             //dump($comment->getId()); dump($id);
             if($comment->getId() == $id) return $comment;
             $answer = $comment->getCommentById($id);
             if($answer != false) return $answer;
         }
         return false;
    }
    
    /**
     * @return mixed
     */
    public function setCommentById($id, Comment $com)
    {   
        $comments = $this->getAnswers();
        foreach ($comments as $key => $comment) {
            //dump($comment->getId()); dump($id);
            //si c'est le bon commentaire, on le remplace
            if($comment->getId() == $id){
                $comments[$key] = $com;
                return true;
            }else{ //sinon on regarde dans les réponses de ce commentaire
                $answer = $comment->setCommentById($id, $com);
                if($answer != false){
                   return true;
                }
            }
         }
         return false;
    }


    /**
     * remove comment from the stream
     */
    public function removeComment(Comment $comment)
    {   
        foreach ($this->answers as $key => $answer) {
            if($comment->getId() == $answer->getId()){
                unset($this->answers[$key]);
                return true;
            }
            $res = $answer->removeComment($comment);
            if($res != false){ return true; }
        }
        return false;
    }

    /**
     * Get report
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * Set report
     */
    public function setReport(Report $report)
    {
        $this->report = $report;
        return $this;
    }

    /**
     * Remove report
     */
    public function removeReport()
    {
        $this->report = null;
        return $this;
    }

    /**
     * Get isActive
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
        return $this;
    }

    /*
     * Get created
     */
    public function getCreated()
    {
        return $this->created;
    }

    /*
     * Set created
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    public function getPastTime(){
        return Helper::pastTime($this->created);
    }

    public function cleanRemove($em, $signedSlug=null){

        //signedSlug mean we want to delete only if this comment as the same signed->slug than $signedSlug
        if($signedSlug != null && $this->getSigned()->getSlug() != $signedSlug){
            foreach ($this->getAnswers() as $key => $answer) {
                $answer->cleanRemove($em, $signedSlug);
            }
        }//
        elseif($signedSlug != null && $this->getSigned()->getSlug() == $signedSlug){
            $this->getParentStream()->removeComment($this);        
        }
        //if signedSlug == null > no check for slug
        elseif($signedSlug == null || $this->getSigned()->getSlug() == $signedSlug){
            //dump("Comment: start remove all notifications about this comment");
            //supprime les notifications liées à ce commentaire
            $notifRepo = $em->getRepository(Notification::class);
            $notifs = $notifRepo->findBy(array('whatType'=>"comment",
                                               'whatId' => $this->getId())); 
            foreach ($notifs as $n => $notif) {
                $em->remove($notif);
                $em->flush();
            }
            //dump("Comment: start remove all reports about this comment");
            //supprime les reports liées à ce commentaire
            $reportRepo = $em->getRepository(Report::class);
            $reports = $reportRepo->findBy(array('commentId' => $this->getId())); 
            foreach ($reports as $n => $report) {
                $em->remove($report);
                $em->flush();
            }
        }
    }




    /**
    * Return a clean array with data to be used in view by Javascript<br>
    * Currently it is used for SIG, to display data on the map 
    */
    public function getJson(): array
    {
        return array(
           "id" => $this->id,
           "text" => $this->text,
           "tags" => $this->tags,
           "nbLikes" => count($this->likes),
           "NbDislikes" => count($this->dislikes),
        );
    }

}
