<?php

namespace App\Entity;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


use App\Services\Helper;


/**
 * @MongoDB\Document(repositoryClass="App\Repository\TagRepository")
 */
class Tag
{

	/**
     * @MongoDB\Id(strategy="auto")
     */
	private $id;

    /**
     * The tag value, ex : #bonjour
     * @MongoDB\Field(type="string")
     */
    private $tag;

    /**
     * Number of occurence in News
     * @MongoDB\Field(type="integer")
     */
    private $count;

    /**
     * Date initialized when the Tag is created
     * Set one time, never modified after initialization
     * @MongoDB\Field(type="date")
     */
    private $created;

    /**
     * Date after every incrementation, or decrementation
     * @MongoDB\Field(type="date")
     */
    private $updated;

    /**
     * The string value of the date
     * @MongoDB\Field(type="string")
     */
    private $date;

    /**
     * The string value of the date
     * @MongoDB\Field(type="string")
     */
    private $scope;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param mixed $tag
     *
     * @return self
     */
    public function setTag($tag)
    {
        $this->tag = $tag;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param mixed $count
     *
     * @return self
     */
    public function setCount($count)
    {
        $this->count = $count;
        return $this;
    }

    /**
     * @param mixed $count
     *
     * @return self
     */
    public function incCount()
    {
        $this->count = $this->count + 1;
        $now = new \Datetime();
    	$this->updated = $now->format('y-m-d');
        return $this;
    }

    /**
     * @param mixed $count
     *
     * @return self
     */
    public function deccCount()
    {
        $this->count = $this->count - 1;
        $now = new \Datetime();
    	$this->updated = $now->format('y-m-d');
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     *
     * @return self
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }



    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     *
     * @return self
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }


    /**
     * @return mixed
     * values : "day", "7day" //represent the periode of count calculation
     */
    public function getScope()
    {
        return $this->scope;
    }

    /**
     * @param mixed $scope
     *
     * @return self
     */
    public function setScope($scope)
    {
        $this->scope = $scope;
        return $this;
    }

    public function __construct($tag, $scope="day")
    {
        $this->tag = $tag;
        $this->scope = $scope;
        $this->count = 1;
        $now = new \Datetime();
        //dd($now->format('Y-m-d'));
    	$this->created = $now->format('Y-m-d');
        $this->updated = $now->format('Y-m-d');
        $this->date = $now->format('Y-m-d');
    }


    /**
     * Static function, scan a tags list, increment count if exist, create new document if not exist
     * If $full == false : only register new tag, but not increment if exist
     * @return bool
     */
    static public function registerTags(Array $tags, $em, $full){
    	$now = new \Datetime();
    	$date = new \Datetime();
    	
    	$tagRepo = $em->getRepository(Tag::class);
    	foreach ($tags as $key => $tag) {
        	$tagBd = $tagRepo->findOneBy(
        						array("tag" => $tag, 
        							  "scope" => "day", 
        							  "date"=>$now->format('Y-m-d')));

        	if($tagBd != null){
        		if($full==true){
	        		$tagBd->incCount();
	            	$em->flush($tagBd);
	            }
        	}else{
        		$tagBd = new Tag($tag, "day");
	        	$em->persist($tagBd);
	            $em->flush();
        	}

        	//calculate count for the 7 last past days
        	$scope = "P1W";       	
        	$date1W = $date->sub(new \DateInterval($scope));
    		$allTags = $tagRepo->findBy(
        						array("tag" => $tag, 
        							  "scope" => "day", 
        							  "created"=>array('$gt'=>$date1W)));

    		$allCount = 0;
    		if($allTags != null)
    		foreach ($allTags as $k => $t) {
    			$allCount+=$t->getCount();
    		}
    		
			$tagScope = $tagRepo->findOneBy(
        						array("tag" => $tag, 
        							  "scope" => $scope, 
        							  "date"=>$now->format('Y-m-d')));

			if($tagScope != null){
        		if($full==true){
	        		$tagScope->setCount($allCount);
	        		$em->flush($tagScope);
	            }
        	}else{
        		$tagScope = new Tag($tag, $scope);
        		$tagScope->setCount($allCount);
	        	$em->persist($tagScope);
	            $em->flush();
        	}

    	}
    	return true;
    }

}