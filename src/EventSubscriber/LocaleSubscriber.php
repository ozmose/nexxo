<?php
namespace App\EventSubscriber;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class LocaleSubscriber implements EventSubscriberInterface
{
    private $defaultLocale;

    public function __construct($defaultLocale = 'en')
    {
        $this->defaultLocale = $defaultLocale;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        if (!$request->hasPreviousSession()) {
            //return;
        }
        // dump($request->hasPreviousSession());
        

        $userPrefLang = $request->getPreferredLanguage();
        $currentLocale = $request->getSession()->get('_locale');
        //$cookieLocal = $request->cookies->get('_locale');
        //dump($userPrefLang);
        //dump($currentLocale);

        //dump($cookieLocal);

        //if($cookieLocal != null) $userPrefLang = $cookieLocal;

        if($currentLocale == null && ($userPrefLang == "fr" || $userPrefLang == "fr_FR")){
            $request->setLocale('fr');
            $request->getSession()->set('_locale', "fr");
            //dump("0fr");
        }elseif($currentLocale != null){
            $request->setLocale($currentLocale);
            $request->getSession()->set('_locale', $currentLocale);
            //dump("1".$currentLocale);
        }else{
            $request->setLocale($this->defaultLocale);
            $request->getSession()->set('_locale', $this->defaultLocale);
            //dump("2".$this->defaultLocale);
        }

        //dump($userPrefLang);
        //dump($request->getSession()->get('_locale'));

        // try to see if the locale has been set as a _locale routing parameter
        //if ($locale = $request->attributes->get('_locale')) {
       /* $locale = $request->getSession()->get('_locale');
        if ($locale == null) {
            $request->getSession()->set('_locale', $this->defaultLocale);
            $request->setLocale($this->defaultLocale);
            dump("1 ".$locale." - ".$this->defaultLocale);
        } else {
            // if no explicit locale has been set on this request, use one from the session
            $lo = $request->getSession()->get('_locale');
            $request->setLocale($lo);
            dump("2 ".$lo);
        }*/
        
    }



    public static function getSubscribedEvents()
    {
        return array(
            // must be registered before (i.e. with a higher priority than) the default Locale listener
            KernelEvents::REQUEST => array(array('onKernelRequest', 20)),
        );
    }
}